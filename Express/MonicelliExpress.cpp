#include "fileEater.h"
#include "clusterizer.h"
#include "trackFitter.h"
#include "trackFinder.h"
#include "HManager.h"
#include "Geometry.h"
#include "aligner.h"
#include "threader.h"
#include "maintabs.h"

#include <TApplication.h>

#include <QDomDocument>
#include <QFile>
#include <QString>
#include <QDomNode>

#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>


// @@@ Hard coded parameters @@@
#define DUT_PLANE_ONLYXY_PARS             100011 // Define the fix [1] and free [0] parameters [z,y,x,gamma,beta,alpha]
#define DUT_STRIP_PARS                    100001 // Define the fix [1] and free [0] parameters [z,y,x,gamma,beta,alpha]
#define DUT_PIXEL_PARS                    100000 // Define the fix [1] and free [0] parameters [z,y,x,gamma,beta,alpha]
#define TELESCOPE_PIXEL_PLANE_PARS        100000 // Define the fix [1] and free [0] parameters [z,y,x,gamma,beta,alpha]
#define TELESCOPE_STRIP_PLANE_ODD_PARS    110001 // Define the fix [1] and free [0] parameters [z,y,x,gamma,beta,alpha]
#define TELESCOPE_STRIP_PLANE_EVEN_PARS   101011 // Define the fix [1] and free [0] parameters [z,y,x,gamma,beta,alpha]
#define MAX_CLUSTER_SIZE 2   			 // Max cluster size for points during an automatic alignment
#define NTELEALIGN 5         			 // Maximum telescope fine alignment iterations
#define NDUTALIGN 3         			 // Maximum DUT alignment iterations
#define DUT2STEPS true        			 // Do DUT alignment in 2 steps: (1) only translations, (2) translations + angles
#define LARGEWINDOW 1000.     			 // DUT larger window (> 1mm could cause us to misalign DUT if noisy pixels present)
#define COPYGEOFILE false    			 // Copy geo file into geometry directory
// ============================


class XmlDefaults;
class XmlFile;


//=======================================================================
class XmlParser
{
public:
  XmlParser (void);
  ~XmlParser(void);
  
  void parseDocument(std::string fileName);
  
  XmlDefaults*          getDefaults(void) {return theDefaults_;}
  std::vector<XmlFile*> getFileList(void) {return theFileList_;}
  
private:
  QDomDocument* document_;
  QDomNode      rootNode_;
  
  XmlDefaults*          theDefaults_;
  std::vector<XmlFile*> theFileList_;
  stringstream          ss_;
};


//=======================================================================
class XmlDefaults
{
public:
  XmlDefaults (QDomNode& node);
  ~XmlDefaults(void) {;}

  QDomNode&   getNode(void) {return thisNode_;}

  std::string filesPath_;
  std::string geometriesPath_;
  std::string trackFindingAlgorithm_;
  std::string trackFittingAlgorithm_;
  int         numberOfEvents_;     
  double      chi2Cut_;	       
  int         trackPoints_;     
  int         maxPlanePoints_;     
  int         xTolerance_;         
  int         yTolerance_;         
  int         xToleranceDUT_;         
  int         yToleranceDUT_;         
  bool        findDut_;         
  bool        useEtaFunction_;         
  bool        doDUTFineAlignment_;
  bool        doTelescopeFineAlignment_;
  
private:
  QDomNode thisNode_;
};


//=======================================================================
class XmlFile
{
public:
  XmlFile (QDomNode& node);
  ~XmlFile(void) {;}
  QDomNode&   getNode(void) {return thisNode_;}

  std::string fileName_;
  std::string geometryName_;
  
private:
  QDomNode thisNode_;
};

//=======================================================================
XmlParser::XmlParser(void) : document_(0)
{
}


//=======================================================================
XmlParser::~XmlParser()
{
  if (document_) delete document_;
}


//=======================================================================
void XmlParser::parseDocument(std::string xmlFileName)
{
  if (document_) delete document_;
  
  document_ = new QDomDocument("ConfigurationFile");
  QFile xmlFile(xmlFileName.c_str());

  if (!xmlFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
      STDLINE(std::string("Could not open ") + xmlFile.fileName().toStdString(),ACRed);
      return;
    }
  
  QString errMsg = "";
  int line;
  int col;

  if (!document_->setContent(&xmlFile, true , &errMsg, &line, &col))
    {
      STDLINE(std::string("Could not access ") + xmlFile.fileName().toStdString(),ACRed);
      ss_ << "Error: " << errMsg.toStdString() << " line: " << line << " col: " << col;
      STDLINE(ss_.str(),ACGreen);
      xmlFile.close();
      return;
    }
  
  STDLINE(std::string("Parsing ") + xmlFile.fileName().toStdString(),ACGreen);
  
  rootNode_ = document_->elementsByTagName("MonicelliExpressConfiguration").at(0);
  
  QDomNode defaults = document_->elementsByTagName("Defaults").at(0);
  theDefaults_ = new XmlDefaults(defaults);

  QDomNodeList fileList = document_->elementsByTagName("File");

  for (int f = 0; f < fileList.size(); f++)
    {
      QDomNode fileNode = fileList.at(f);

      if (!fileNode.isComment()) theFileList_.push_back(new XmlFile(fileNode));
    }
  
  xmlFile.close();
}


//=======================================================================
XmlDefaults::XmlDefaults(QDomNode& node)
{
  thisNode_       	 = node;
  filesPath_      	 = node.toElement().attribute("FilesPath").toStdString();
  geometriesPath_ 	 = node.toElement().attribute("GeometriesPath").toStdString();
  trackFindingAlgorithm_ = node.toElement().attribute("TrackFindingAlgorithm").toStdString();
  trackFittingAlgorithm_ = node.toElement().attribute("TrackFittingAlgorithm").toStdString();
  numberOfEvents_        = node.toElement().attribute("NumberOfEvents").toInt();
  chi2Cut_        	 = node.toElement().attribute("Chi2Cut").toInt();
  trackPoints_    	 = node.toElement().attribute("TrackPoints").toInt();
  maxPlanePoints_ 	 = node.toElement().attribute("MaxPlanePoints").toInt();
  xTolerance_     	 = node.toElement().attribute("XTolerance").toInt();
  yTolerance_     	 = node.toElement().attribute("YTolerance").toInt();
  xToleranceDUT_     	 = node.toElement().attribute("XToleranceDUT").toInt();
  yToleranceDUT_     	 = node.toElement().attribute("YToleranceDUT").toInt();

  findDut_        	    = true;
  useEtaFunction_           = true;
  doDUTFineAlignment_       = true;
  doTelescopeFineAlignment_ = true;

  if (node.toElement().attribute("FindDut") == "false" || node.toElement().attribute("FindDut") == "False")
    findDut_ = false;

  if (node.toElement().attribute("UseEtaFunction") == "false" || node.toElement().attribute("UseEtaFunction") == "False")
    useEtaFunction_ = false;

  if (node.toElement().attribute("FineAlignment") == "false" || node.toElement().attribute("FineAlignmente") == "False")
    doDUTFineAlignment_ = false;

  if (node.toElement().attribute("FineTelescopeAlignment") == "false" || node.toElement().attribute("FineTelescopeAlignmente") == "False")
    doTelescopeFineAlignment_ = false;
}


//=======================================================================
XmlFile::XmlFile(QDomNode& node)
{
  thisNode_     = node;
  fileName_     = node.toElement().attribute("Name").toStdString();
  geometryName_ = node.toElement().attribute("Geometry").toStdString();
}




//=======================================================================
int main (int argc, char** argv)
{
  if(getenv("MONICELLITMPAREA") == nullptr)
  {
      std::cout << "******************************************************" << std::endl;
      std::cout << "You must set the MONICELLITMPAREA environment variable!" << std::endl;
      std::cout << "Hint:" << std::endl;
      std::cout << "export MONICELLITMPAREA=/tmp" << std::endl;
      std::cout << "******************************************************" << std::endl;
      exit(EXIT_SUCCESS);
  }
  
  stringstream ss;

  gROOT->SetBatch(true);
  TApplication tApp("App",&argc,argv);
  STDLINE("=== Using a TApplication only ===" ,ACRed);

  XmlParser theXmlParser;
  std::string configFileName;

  //std::cout << "argc: " << argc << " -> " << *argv << " -> "<< argv[0] << " - " << argv[1] << std::endl;
  //return 1;
  if      (argc == 1) configFileName = std::string("./xml/ExpressConfiguration.xml");
  else if (argc == 2)
  { 
//  For some reasons you can't have a path as argv since the / character is interpreted as an escape one
//    if(std::string(argv[1]).find("/") == std::string::npos)
      configFileName = std::string("./xml/") + argv[1];
//    else
//      configFileName = argv[1];
  }
  else if (argc >  2)
    {
      ss.clear();
      ss.str("");
      ss << "Usage: ./MonicelliExpress optional(configuration file)";
      STDLINE(ss.str(),ACRed);

      exit(EXIT_SUCCESS);
    }

  ss.clear();
  ss.str("");
  ss << "Using: " << configFileName << " configuration.";
  STDLINE(ss.str(),ACGreen);

  theXmlParser.parseDocument(configFileName.c_str());
  
  
  const string filesPath                = theXmlParser.getDefaults()->filesPath_;
  const string geometriesPath           = theXmlParser.getDefaults()->geometriesPath_;
  std::string  trackFindingAlgorithm    = theXmlParser.getDefaults()->trackFindingAlgorithm_;
  std::string  trackFittingAlgorithm    = theXmlParser.getDefaults()->trackFittingAlgorithm_;
  int          numberOfEvents           = theXmlParser.getDefaults()->numberOfEvents_;
  double       chi2Cut                  = theXmlParser.getDefaults()->chi2Cut_;
  int          trackPoints              = theXmlParser.getDefaults()->trackPoints_;
  int          maxPlanePoints           = theXmlParser.getDefaults()->maxPlanePoints_;
  int	       xTolerance               = theXmlParser.getDefaults()->xTolerance_;
  int	       yTolerance               = theXmlParser.getDefaults()->yTolerance_;
  int	       xToleranceDUT;            
  int	       yToleranceDUT;           
  bool         findDut                  = theXmlParser.getDefaults()->findDut_;
  bool         useEtaFunction           = theXmlParser.getDefaults()->useEtaFunction_;
  bool         doDUTFineAlignment       = theXmlParser.getDefaults()->doDUTFineAlignment_;
  bool         doTelescopeFineAlignment = theXmlParser.getDefaults()->doTelescopeFineAlignment_;
  
  if((theXmlParser.getDefaults()->xToleranceDUT_) && (theXmlParser.getDefaults()->yToleranceDUT_))
  {
  	xToleranceDUT = theXmlParser.getDefaults()->xToleranceDUT_;
  	yToleranceDUT = theXmlParser.getDefaults()->yToleranceDUT_;
  }
  else
  {
  	xToleranceDUT = theXmlParser.getDefaults()->xTolerance_;
  	yToleranceDUT = theXmlParser.getDefaults()->yTolerance_;
  }
  
  for (unsigned int f = 0; f < theXmlParser.getFileList().size(); f++)
    {
      string fileName    = filesPath      + theXmlParser.getFileList()[f]->fileName_;
      string geoFileName = geometriesPath + theXmlParser.getFileList()[f]->geometryName_;

      fileEater	theFileEater;
      HManager theHManager(&theFileEater);
      theFileEater.setHManger(&theHManager);
      clusterizer theClusterizer;
      trackFitter theTrackFitter;
      trackFinder theTrackFinder(&theTrackFitter);
      aligner theTelescopeAligner(&theFileEater,&theHManager);
      aligner theDUTAligner(&theFileEater,&theHManager);

      fitter theFitter;
      calibrationLoader theCalibrationLoader(&theFileEater, 
                                             &theHManager, 
                                             &theFitter,
                                             theFileEater.getGeometryLoader(),
                                             NULL
                                            );
      // #########################
      // # Parse and make events #
      // #########################
      STDLINE("",ACBlue);
      STDLINE("Parse and Make Events",ACBlue);

      if (theFileEater.openFile(geoFileName) == "Error!")
  	{
	  STDLINE("Error in geoFileName",ACRed);
	  continue;
  	}	
	 
      theFileEater.setInputFileName(fileName);
      theFileEater.setEventsLimit(numberOfEvents);

      Geometry* theGeometry = theFileEater.getGeometry();
      theGeometry->orderPlanes();
      theGeometry->calculatePlaneMCS();
      theTrackFitter.setKalmanPlaneInfo(theGeometry->getKalmanPlaneInfo());
      
      if (!theFileEater.parse())
  	{
	  STDLINE("Error in parsing",ACRed);
	  continue;
  	}
      theHManager.setRunSubDir(theFileEater.openFile(theFileEater.getOutputTreeCompletePath()));

      // ###############
      // # Calibration #
      // ###############

      theCalibrationLoader.execute(); // loads all of the calibrations
      theFileEater.updateGeometry("geometry"); 

      // ###############
      // # Clusterizer #
      // ###############
      STDLINE("Clusterizer",ACBlue);

      if (useEtaFunction) theClusterizer.getChargeAsymmetryPlots(theGeometry);
      else                theClusterizer.setUseEtaFunction(false);
      theFileEater.setOperation(&fileEater::updateEvents2,&theClusterizer);
      theFileEater.updateEvents2();
      
      KalmanPlaneInfo theKalmanPlaneInfo = theGeometry->getKalmanPlaneInfo();

      // ################
      // # Track finder #
      // ################
      STDLINE("Track Finder",ACBlue);

      theTrackFitter.setNumberOfIterations(0);
      theTrackFitter.includeResiduals(true);
      theTrackFinder.setTrackSearchParameters(xTolerance*(1e-4)*CONVF, yTolerance*(1e-4)*CONVF, xToleranceDUT*(1e-4)*CONVF, yToleranceDUT*(1e-4)*CONVF, chi2Cut, trackPoints, maxPlanePoints);
      theTrackFinder.setTrackingOperationParameters(trackFindingAlgorithm, trackFittingAlgorithm, findDut);
      theFileEater.setOperation(&fileEater::updateEvents2,&theTrackFinder);
      theTrackFinder.setOperation(&trackFinder::findAndFitTracks);
      theFileEater.updateEvents2();

      // ############################
      // # Telescope fine alignment #
      // ############################
      if (doTelescopeFineAlignment)
      {
        STDLINE("Telescope Fine Alignment",ACBlue);

        theTelescopeAligner.setAlignmentFitMethodName(trackFittingAlgorithm);
        theTelescopeAligner.setNumberOfIterations(0);

        for (Geometry::iterator it = theGeometry->begin(); it != theGeometry->end(); it++)
        {
          if ( (*it).second->isDUT() )  theTelescopeAligner.setFixParMap((*it).first, 111111);
          else //TELESCOPE
          {
            if ( (*it).second->isStrip() )
            {
              if ( theGeometry->getDetectorModule((*it).first) %2 == 0 )  theTelescopeAligner.setFixParMap((*it).first, TELESCOPE_STRIP_PLANE_EVEN_PARS);
              else                                                        theTelescopeAligner.setFixParMap((*it).first, TELESCOPE_STRIP_PLANE_ODD_PARS);
            }   
            else  theTelescopeAligner.setFixParMap((*it).first, TELESCOPE_PIXEL_PLANE_PARS);
          }
        }

        // #############################
        // # Parameter meaning:        #
        // #############################
        // # - max trials              #
        // # - fine alignment strategy #
        // # - chi2cut                 #
        // # - max cluster size        #
        // # - min points              #
        // # - max tracks / ev         #
        // # - no diagonal clusters    #
        // # - alignment select        #
        // # - nEvents                 #
        // #############################
        theTelescopeAligner.setOperation(&aligner::align);
        theTelescopeAligner.setAlignmentPreferences(NTELEALIGN, 0, &theTrackFitter, chi2Cut, MAX_CLUSTER_SIZE, trackPoints, 1, true, "", numberOfEvents);
        theTelescopeAligner.align();

        aligner::alignmentResultsDef alignmentResultsTelescope = theTelescopeAligner.getAlignmentResults();
        for (Geometry::iterator geo = theGeometry->begin(); geo != theGeometry->end(); geo++)
        {
          Detector* theDetector = theGeometry->getDetector(geo->first);

          double xPositionCorrection = theDetector->getXPositionCorrection() + alignmentResultsTelescope[geo->first].deltaTx;
          double yPositionCorrection = theDetector->getYPositionCorrection() + alignmentResultsTelescope[geo->first].deltaTy;
          double zPositionCorrection = theDetector->getZPositionCorrection() + alignmentResultsTelescope[geo->first].deltaTz;

          //  Gonna set strip alphas to zero
          //		  double xRotationCorrection = theDetector->getXRotationCorrection() + alignmentResultsTelescope[geo->first].alpha;
          if ( (*geo).second -> isStrip() ) {
            double xRotationCorrection = theDetector->getXRotationCorrection();
            theDetector->setXRotationCorrection(xRotationCorrection);
          }
          else {
            double xRotationCorrection = theDetector->getXRotationCorrection() + alignmentResultsTelescope[geo->first].alpha;
            theDetector->setXRotationCorrection(xRotationCorrection);
          }
          double yRotationCorrection = theDetector->getYRotationCorrection() + alignmentResultsTelescope[geo->first].beta;
          double zRotationCorrection = theDetector->getZRotationCorrection() + alignmentResultsTelescope[geo->first].gamma;

          theDetector->setXPositionCorrection(xPositionCorrection);
          theDetector->setYPositionCorrection(yPositionCorrection);
          theDetector->setZPositionCorrection(zPositionCorrection);
          //		  theDetector->setXRotationCorrection(xRotationCorrection);
          theDetector->setYRotationCorrection(yRotationCorrection);
          theDetector->setZRotationCorrection(zRotationCorrection);


        }

        // ###################
        // # Update geometry #
        // ###################	
        STDLINE("Update Geometry",ACBlue);

        theFileEater.updateGeometry("geometry");	 
        theGeometry = theFileEater.getGeometry();
        theGeometry ->orderPlanes();
        theGeometry ->calculatePlaneMCS();
        theTrackFitter.setKalmanPlaneInfo(theGeometry->getKalmanPlaneInfo());        

        // ################
        // # Track finder #
        // ################
        STDLINE("Track Finder",ACBlue);
        
        theTrackFitter.setNumberOfIterations(0);
        theTrackFitter.includeResiduals(true);

        theTrackFinder.setTrackSearchParameters(xTolerance*(1e-4)*CONVF, yTolerance*(1e-4)*CONVF, xToleranceDUT*(1e-4)*CONVF, yToleranceDUT*(1e-4)*CONVF, chi2Cut, trackPoints, maxPlanePoints);
        theTrackFinder.setTrackingOperationParameters(trackFindingAlgorithm, trackFittingAlgorithm, findDut);
        theFileEater.setOperation(&fileEater::updateEvents2,&theTrackFinder);
        theTrackFinder.setOperation(&trackFinder::findAndFitTracks);
        theFileEater.updateEvents2();
      }


      // ######################
      // # Fine alignment DUT #
      // ######################
      if (doDUTFineAlignment == true)
	{
	  if (DUT2STEPS == true)
	    {
	      // ############################################
	      // # Track finder on DUT: large window search #
	      // ############################################
	      STDLINE("Track Finder on DUT: large window search",ACBlue);
	      
              theTrackFinder.setTrackSearchParameters(xTolerance*(1e-4)*CONVF, yTolerance*(1e-4)*CONVF, LARGEWINDOW*(1e-4)*CONVF, LARGEWINDOW*(1e-4)*CONVF, chi2Cut, trackPoints, maxPlanePoints);
	      theFileEater.setOperation(&fileEater::updateEvents2,&theTrackFinder);
	      theTrackFinder.setOperation(&trackFinder::findDUTCandidates);
	      theFileEater.updateEvents2();


	      // ###############################
	      // # Fine alignment DUT: only XY #
	      // ###############################
	      STDLINE("Fine Alignment DUT: only XY",ACBlue);
	      
	      for (Geometry::iterator it = theGeometry->begin(); it != theGeometry->end(); it++)
		{
		  if (!(*it).second->isDUT()) continue;
		  
		  string dut = it->first;
		  
		  theDUTAligner.setFixParMap(dut,DUT_PLANE_ONLYXY_PARS); // Here is where I choose which parameters must be kept constant
		  theDUTAligner.setAlignmentPreferences(NDUTALIGN, 0, &theTrackFitter, chi2Cut, MAX_CLUSTER_SIZE, trackPoints, 1, true, dut, numberOfEvents);
		  theDUTAligner.setOperation(&aligner::alignDUT);
	          theDUTAligner.alignDUT();

		  aligner::alignmentResultsDef alignmentResults = theDUTAligner.getAlignmentResults();
		  Detector* theDetector = theGeometry->getDetector(dut);
		  
		  double xPositionCorrection = theDetector->getXPositionCorrection() + alignmentResults[dut].deltaTx;
		  double yPositionCorrection = theDetector->getYPositionCorrection() + alignmentResults[dut].deltaTy;
		  double zPositionCorrection = theDetector->getZPositionCorrection() + alignmentResults[dut].deltaTz;
		  double xRotationCorrection = theDetector->getXRotationCorrection() + alignmentResults[dut].alpha;
		  double yRotationCorrection = theDetector->getYRotationCorrection() + alignmentResults[dut].beta;
		  double zRotationCorrection = theDetector->getZRotationCorrection() + alignmentResults[dut].gamma;
		  
		  theDetector->setXPositionCorrection(xPositionCorrection);
		  theDetector->setYPositionCorrection(yPositionCorrection);
		  theDetector->setZPositionCorrection(zPositionCorrection);
		  theDetector->setXRotationCorrection(xRotationCorrection);
		  theDetector->setYRotationCorrection(yRotationCorrection);
		  theDetector->setZRotationCorrection(zRotationCorrection);
		}


	      // ###################
	      // # Update geometry #
	      // ###################	
	      STDLINE("Update Geometry",ACBlue);

	      theFileEater.updateGeometry("geometry");
	      theGeometry = theFileEater.getGeometry();
    	      theGeometry ->orderPlanes();
    	      theGeometry ->calculatePlaneMCS();
    	      theTrackFitter.setKalmanPlaneInfo(theGeometry->getKalmanPlaneInfo());        
	    }


	  // #######################
	  // # Track finder on DUT #
	  // #######################
	  STDLINE("Track Finder on DUT",ACBlue);
	  
	  theTrackFinder.setTrackSearchParameters(xTolerance*(1e-4)*CONVF, yTolerance*(1e-4)*CONVF, xToleranceDUT*(1e-4)*CONVF, yToleranceDUT*(1e-4)*CONVF, chi2Cut, trackPoints, maxPlanePoints);
	  theFileEater.setOperation(&fileEater::updateEvents2,&theTrackFinder);
	  theTrackFinder.setOperation(&trackFinder::findDUTCandidates);
	  theFileEater.updateEvents2();
	  
	  
	  // ######################
	  // # Fine alignment DUT #
	  // ######################
	  STDLINE("Fine Alignment DUT: XY + angles",ACBlue);

	  for (Geometry::iterator it = theGeometry->begin(); it != theGeometry->end(); it++)
	    {
	      if (!(*it).second->isDUT()) continue;
	  
	      string dut = it->first;

	      if(dut == "Station: 6 - Plaq: 1")
              {
	      	theDUTAligner.setFixParMap(dut,DUT_STRIP_PARS); // Here is where I choose which parameters must be kept constant
              } 
	      else
	      {
	      	theDUTAligner.setFixParMap(dut,DUT_PIXEL_PARS); // Here is where I choose which parameters must be kept constant
              }

	      theDUTAligner.setAlignmentPreferences(NDUTALIGN, 0, &theTrackFitter, chi2Cut, MAX_CLUSTER_SIZE, trackPoints, 1, true, dut, numberOfEvents);
	      theDUTAligner.setOperation(&aligner::alignDUT);
	      theDUTAligner.alignDUT();

	      aligner::alignmentResultsDef alignmentResults = theDUTAligner.getAlignmentResults();
	      Detector* theDetector = theGeometry->getDetector(dut);

	      double xPositionCorrection = theDetector->getXPositionCorrection() + alignmentResults[dut].deltaTx;
	      double yPositionCorrection = theDetector->getYPositionCorrection() + alignmentResults[dut].deltaTy;
	      double zPositionCorrection = theDetector->getZPositionCorrection() + alignmentResults[dut].deltaTz;
	      double xRotationCorrection = theDetector->getXRotationCorrection() + alignmentResults[dut].alpha;
	      double yRotationCorrection = theDetector->getYRotationCorrection() + alignmentResults[dut].beta;
	      double zRotationCorrection = theDetector->getZRotationCorrection() + alignmentResults[dut].gamma;
	  
	      theDetector->setXPositionCorrection(xPositionCorrection);
	      theDetector->setYPositionCorrection(yPositionCorrection);
	      theDetector->setZPositionCorrection(zPositionCorrection);
	      theDetector->setXRotationCorrection(xRotationCorrection);
	      theDetector->setYRotationCorrection(yRotationCorrection);
	      theDetector->setZRotationCorrection(zRotationCorrection);
	    }



	  // ###################
	  // # Update geometry #
	  // ###################	
	  STDLINE("Update Geometry",ACBlue);
	  
	  theFileEater.updateGeometry("geometry");
	  theGeometry = theFileEater.getGeometry();				    
    	  theGeometry ->orderPlanes();						    
    	  theGeometry ->calculatePlaneMCS();					    
    	  theTrackFitter.setKalmanPlaneInfo(theGeometry->getKalmanPlaneInfo());	    



	  // #######################
	  // # Track finder on DUT #
	  // #######################
	  STDLINE("Track Finder on DUT: final",ACBlue);

	  theTrackFinder.setTrackSearchParameters(xTolerance*(1e-4)*CONVF, yTolerance*(1e-4)*CONVF, xToleranceDUT*(1e-4)*CONVF, yToleranceDUT*(1e-4)*CONVF, chi2Cut, trackPoints, maxPlanePoints);
	  theFileEater.setOperation(&fileEater::updateEvents2,&theTrackFinder);
	  theTrackFinder.setOperation(&trackFinder::findDUTCandidates);
	  theFileEater.updateEvents2();
	}



      // #############
      // # Residuals #
      // #############
      STDLINE("Residuals",ACBlue);

      theTrackFitter.clearSelectedDetectorsList();
      theTrackFitter.setFitMethodName(trackFittingAlgorithm);
      theTrackFitter.setNumberOfIterations(0);
      theTrackFitter.includeResiduals(true);
      theTrackFitter.setOperation(&trackFitter::makeFittedTracksResiduals);
      theFileEater.setOperation(&fileEater::updateEvents2,&theTrackFitter);
      theFileEater.updateEvents2();


      // ######################
      // # Copy geometry file #
      // ######################
      if (COPYGEOFILE == true)
	{
	  STDLINE("Copy Geometry File",ACBlue);
	  
	  string outputFilePath = filesPath;
          outputFilePath = outputFilePath.substr(0, outputFilePath.substr(0,outputFilePath.size()-1).find_last_of('/')).append("/MonicelliOutput/");	  
	  string newGeoName = theXmlParser.getFileList()[f]->fileName_;
	  newGeoName.erase(newGeoName.length()-4,newGeoName.length()).append(".geo");
	  
	  string copyGeometryCommand = "cp " + outputFilePath + newGeoName + " " + geometriesPath + newGeoName;
	  STDLINE(copyGeometryCommand.c_str(),ACBlue);
	  system(copyGeometryCommand.c_str());
	}
    }
  STDLINE("EXIT_SUCCESS",ACBlue);
  _exit(EXIT_SUCCESS); // exits without calling destructors; we do this because destructing the calibrations can take 30+ minutes, and has no benefit to us...
}
