#!/usr/bin/env python                                                                                 
import os,sys,argparse

# example usage: `python mkexpressscript.py 32944 --xml Run32948_50x50_DUT_MJ184_16degrees_aligned.xml` will make the script for run 32944
# or: `python mkexpressscript.py 32944 32959 --xml Run32948_50x50_DUT_MJ184_16degrees_aligned.xml` will make the script for all runs from 32944-32959
# or: `python mkexpressscript.py 32944 32959 --skip "32945,32947,32948,32952" --xml Run32948_50x50_DUT_MJ184_16degrees_aligned.xml` will make the script for all except for the four runs specified

# or change the following two lines and run via `python mkexpressscript.py` (no other cmd line args)
#xmlfile = 'Run32975_50x50_DUT_MJ184_20degrees_aligned.xml'
#runs = [32975,32976,32977,32978,32979,32980,32981,32982,32983,32984,32985]
xmlfile = 'Run45690_25x100up_DUT_MJ531_box_rotated_aligned_0degrees.xml'
runs = [45689,45690,45691,45692,45693,45694,45695,45696,45697,45698,45700]


p = argparse.ArgumentParser()
p.add_argument('startrun', type=int, nargs='?',default=None)
p.add_argument('endrun', type=int, nargs='?',default=None)
p.add_argument('--skip', default="", type=str)
p.add_argument('--xml', default="", type=str)
args = p.parse_args()

if args.startrun :
    startrun = args.startrun

    if args.endrun :
        endrun = args.endrun
    else :
        endrun = startrun
    skipstr = args.skip
    
    skipruns = []
    if skipstr != "" :
        skipruns = map(int, (skipstr).split(",")) # turn str into list of ints

    runs = []
    for run in range(startrun, endrun+1) :
        if run in skipruns : 
            print "skipping run", run
            continue
        runs.append(run)

    if args.xml == "" :
        print "When specifying runs via command line, an xml geometry file must be specified too. Exiting."
        sys.exit()
    else :
        xmlfile = args.xml

# convert ints to strs
runnumberarray =[str(run) for run in runs]

geodir = os.getenv('Monicelli_XML_Dir')
mergeddir = os.getenv('Monicelli_DataSample_Dir')

if not geodir or not mergeddir :
    print "Error: Need to source a setup script! e.g. setup2021March.sh in the Monicelli directory"
    sys.exit()

for runnumber in runnumberarray:
    
    filename = open("xml/MonicelliExpress_Run"+runnumber+"_Merged.xml","w+")

    filename.write("<?xml version='1.0' encoding='iso-8859-1'?> \n")
    filename.write('<!DOCTYPE MonicelliExpressConfiguration SYSTEM "./dtd/ExpressConfiguration.dtd">\n')
    filename.write('<MonicelliExpressConfiguration>\n')
    filename.write('  <Defaults FilesPath="'+mergeddir+'/"\n')
    filename.write('           GeometriesPath="'+geodir+'/"\n')
    filename.write('               TrackFindingAlgorithm="FirstAndLast"\n')
    filename.write('               TrackFittingAlgorithm="Kalman"\n')
    filename.write('               NumberOfEvents="-1"\n')
    filename.write('               TrackPoints="13"\n')
    filename.write('               MaxPlanePoints="-1"\n')
    filename.write('               Chi2Cut="-1"\n')
    filename.write('               XTolerance="300"\n')
    filename.write('               YTolerance="300"\n')
    filename.write('               XToleranceDUT="300"\n')
    filename.write('               YToleranceDUT="300"\n')
    filename.write('               FindDut="true"\n')
    filename.write('               FineAlignment="true"\n')
    filename.write('               FineTelescopeAlignment="true"\n')
    filename.write('               UseEtaFunction="true"/>\n')
    filename.write('   <File Name="Run'+runnumber+'_Merged.dat" Geometry="'+xmlfile+'" />\n')
    filename.write('</MonicelliExpressConfiguration>\n')

    filename.close()


scriptname = "expressscript"+runnumberarray[0]+"to"+runnumberarray[len(runnumberarray)-1]+".sh"
script = open(scriptname,"w+")

for runnumber in runnumberarray:
    script.write('nohup ./MonicelliExpress MonicelliExpress_Run'+runnumber+'_Merged.xml &> ExpressLogs/Run'+runnumber+'_Merged.log \n')
script.close()
os.chmod(scriptname, 0o755)
print "Done! Now run `./%s`" % scriptname
