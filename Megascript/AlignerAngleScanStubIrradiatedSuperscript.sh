#!/bin/bash


RunNumbers=(
	78598
78582
78585
78588
78591
78594
78598
78606
78607
78611
78615
78619
78623
78627
78631
78636
78638
78641
78643
)

for r in ${RunNumbers[@]}
do
	./MegascriptAligner.py --CopyRawFiles false --MergeRawFiles false --RunMonicelli true --RunChewie true -d -g Run${r}_2023_07_25.xml -r ${r} -e 70000 &
	sleep 1
done
