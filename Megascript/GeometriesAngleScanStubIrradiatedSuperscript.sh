#!/bin/bash


#TDC Studies
RunNumbers=(
	78598 

)

for r in ${RunNumbers[@]}
do
	./Megascript.py --CopyRawFiles false --MergeRawFiles false --RunMonicelli true -g Run78598_2023_07_25.xml --RunChewie true -d -r $r &
	sleep 1
	for t in 6
	do
		echo ============= Run number $r =====================
		./TDCMegascript.py --CopyRawFiles false --MergeRawFiles false --RunMonicelli true -g Run78598_2023_07_25.xml -d -t $t -r $r  &
	    	sleep 1
	done
	wait
	
done
