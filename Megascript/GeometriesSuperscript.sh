#!/bin/bash


#TDC Studies
RunNumbers=(
	78365 
	78364 
	78363 
	78362 
	78371 
	78372 
	78373 
	78374 
	78375 
	78376 
	78377 
	78378 
	78379 
	78380 
	78381 
	78382 
	78383
)

for r in ${RunNumbers[@]}
do
	./Megascript.py --CopyRawFiles false --MergeRawFiles false --RunMonicelli true -g 2023_06_PS_07_12_1824.xml --RunChewie true -d -r 78391 

	do
		echo ============= Run number $r =====================
		./TDCMegascript.py --CopyRawFiles false --MergeRawFiles false --RunMonicelli true -g 2023_06_PS_07_12_1824.xml -d -t $t -r $r  &
	    	sleep 1
	done
	
done
