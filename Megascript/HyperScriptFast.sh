#!/bin/bash

SourcePrefix=2023_03_March_BTL

python /home/otsdaq/TestBeamPSModule/Megascript/Megascript.py --CopyRawFiles true --MergeRawFiles true -e $2 -d -r $1 


for i in 0 1 2 3 4 5 6 7
do
	python /home/otsdaq/TestBeamPSModule/Megascript/TDCMegascript.py --CopyRawFiles false --MergeRawFiles false -e $2 -t $i -r $1 & 
done


