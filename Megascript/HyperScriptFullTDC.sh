#!/bin/bash

SourcePrefix=2023_03_March_BTL

/home/otsdaq/TestBeamPSModule/Megascript/Megascript.py --CopyRawFiles true --MergeRawFiles true -d -e $2 -r $1 


for i in 0 1 2 3 4 5 6 7
do
	/home/otsdaq/TestBeamPSModule/Megascript/TDCMegascript.py --CopyRawFiles false --MergeRawFiles false -d -t $i -e $2 -r $1  
done


