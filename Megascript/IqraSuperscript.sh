#!/bin/bash

python ./TDCKalmanMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78366-78370 -t6
python ./TDCKalmanMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78366-78370 -t7
python ./TDCKalmanMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78366-78370 -t1
python ./TDCKalmanMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78366-78370 -t0

python ./TDCKalmanMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78402-78411 -t6
python ./TDCKalmanMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78402-78411 -t7
python ./TDCKalmanMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78402-78411 -t1
python ./TDCKalmanMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78402-78411 -t0
#####################################
#python ./TDCMegascript.py -d -g 2023_06_PS_07_14_0805.xml -r 78670-78671 -t0
# Multiirradiated Scan:
#python ./TDCMegascript.py -d -g 2023_06_PS_07_14_0805.xml --CopyRawFiles true --MergeRawFiles true -r 78661-78671 -t6
# NotIrradiated Scan:
#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml --CopyRawFiles true --MergeRawFiles true -r 78402-78410 -t6
# Irradiated Scan:
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_2024.xml --CopyRawFiles true --MergeRawFiles true -r 78519,78521,78523 -t6


#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml --CopyRawFiles true --MergeRawFiles true -r 78402 -t7
#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml -r 78402 -t7


#python ./TDCMegascript.py -d -g 2023_06_PS_07_14_0805.xml -r 78661-78671 -t7
# NotIrradiated Scan:
#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml -r 78402-78410 -t7
# Irradiated Scan:
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_2024.xml -r 78519,78521,78523 -t7


#python ./TDCMegascript.py -d -g 2023_06_PS_07_14_0805.xml -r 78661-78671 -t0
#python ./TDCMegascript.py -d -g 2023_06_PS_07_14_0805.xml -r 78670-78671 -t0
# NotIrradiated Scan:
#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml -r 78402-78410 -t0
# Irradiated Scan:
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_2024.xml -r 78519,78521,78523 -t0
python ./TDCMegascript.py -d -g 2023_06_PS_07_14_0805.xml -r 78669 -t0
python ./TDCMegascript.py -d -g 2023_06_PS_07_14_0805.xml -r 78669 -t6
python ./TDCMegascript.py -d -g 2023_06_PS_07_14_0805.xml -r 78669 -t7
