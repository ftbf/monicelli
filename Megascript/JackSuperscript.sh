#!/bin/bash

#TDC 6
#Threshold scan Not Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml -r 78371-78401 -t6

#Angle scan Not Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_1405.xml -r 78412-78420 -t6
#python ./TDCMegascript.py -d -g PS_78422.xml -r 78421-78424 -t6
#python ./TDCMegascript.py -d -g PS_78428.xml -r 78425-78429 -t6
#python ./TDCMegascript.py -d -g PS_78430.xml -r 78430-78433 -t6
#python ./TDCMegascript.py -d -g PS_78437.xml -r 78434-78437 -t6
#python ./TDCMegascript.py -d -g PS_78438.xml -r 78438-78441 -t6
#python ./TDCMegascript.py -d -g PS_78446.xml -r 78442-78446 -t6
#python ./TDCMegascript.py -d -g PS_78447.xml -r 78447-78450 -t6
#python ./TDCMegascript.py -d -g PS_78454.xml -r 78451-78454 -t6
#python ./TDCMegascript.py -d -g PS_78455.xml -r 78455-78458 -t6
#python ./TDCMegascript.py -d -g PS_78462.xml -r 78459-78462 -t6
#python ./TDCMegascript.py -d -g PS_78463.xml -r 78463-78466 -t6
#python ./TDCMegascript.py -d -g PS_78468.xml -r 78467-78468 -t6
#python ./TDCMegascript.py -d -g PS_78469.xml -r 78469-78470 -t6
#python ./TDCMegascript.py -d -g PS_78473.xml -r 78471-78473 -t6
#python ./TDCMegascript.py -d -g PS_78474.xml -r 78474-78476 -t6
#python ./TDCMegascript.py -d -g PS_78479.xml -r 78477-78479 -t6
#python ./TDCMegascript.py -d -g PS_78481.xml -r 78480-78482 -t6
#python ./TDCMegascript.py -d -g PS_78485.xml -r 78483-78485 -t6
#python ./TDCMegascript.py -d -g PS_78463.xml -r 78465 -t6

#Threshold Scan Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_2024.xml -r 78544-78578 -t6

#Angle Scan Irradiated
#python ./TDCMegascript.py -d -g Run78579_2023_07_25.xml -r 78579-78581 -t6
#python ./TDCMegascript.py -d -g Run78582_2023_07_25.xml -r 78582-78584 -t6
#python ./TDCMegascript.py -d -g Run78585_2023_07_25.xml -r 78585-78587 -t6
#python ./TDCMegascript.py -d -g Run78588_2023_07_25.xml -r 78588-78590 -t6
#python ./TDCMegascript.py -d -g Run78591_2023_07_25.xml -r 78591-78593 -t6
#python ./TDCMegascript.py -d -g Run78594_2023_07_25.xml -r 78594,78596,78597 -t6
#python ./TDCMegascript.py -d -g Run78598_2023_07_25.xml -r 78598-78601 -t6
#python ./TDCMegascript.py -d -g Run78603_2023_07_25.xml -r 78603-78606 -t6
#python ./TDCMegascript.py -d -g Run78607_2023_07_25.xml -r 78607-78610 -t6
#python ./TDCMegascript.py -d -g Run78611_2023_07_25.xml -r 78611-78614 -t6
#python ./TDCMegascript.py -d -g Run78615_2023_07_25.xml -r 78615-78618 -t6
#python ./TDCMegascript.py -d -g Run78619_2023_07_25.xml -r 78619-78622 -t6
#python ./TDCMegascript.py -d -g Run78623_2023_07_25.xml -r 78623-78626 -t6
#python ./TDCMegascript.py -d -g Run78627_2023_07_25.xml -r 78627-78630 -t6
#python ./TDCMegascript.py -d -g Run78631_2023_07_25.xml -r 78631,78632,78634,78635 -t6
#python ./TDCMegascript.py -d -g Run78636_2023_07_25.xml -r 78636-78637 -t6
#python ./TDCMegascript.py -d -g Run78643_2023_07_25.xml -r 78643-78645 -t6

#TDC 7
#Threshold scan Not Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml -r 78371-78401 -t7

#Angle scan Not Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_1405.xml -r 78412-78420 -t7
#python ./TDCMegascript.py -d -g PS_78422.xml -r 78421-78424 -t7
#python ./TDCMegascript.py -d -g PS_78428.xml -r 78425-78429 -t7
#python ./TDCMegascript.py -d -g PS_78430.xml -r 78430-78433 -t7
#python ./TDCMegascript.py -d -g PS_78437.xml -r 78434-78437 -t7
#python ./TDCMegascript.py -d -g PS_78438.xml -r 78438-78441 -t7
#python ./TDCMegascript.py -d -g PS_78446.xml -r 78442-78446 -t7
#python ./TDCMegascript.py -d -g PS_78447.xml -r 78447-78450 -t7
#python ./TDCMegascript.py -d -g PS_78454.xml -r 78451-78454 -t7
#python ./TDCMegascript.py -d -g PS_78455.xml -r 78455-78458 -t7
#python ./TDCMegascript.py -d -g PS_78462.xml -r 78459-78462 -t7
#python ./TDCMegascript.py -d -g PS_78463.xml -r 78463-78466 -t7
#python ./TDCMegascript.py -d -g PS_78468.xml -r 78467-78468 -t7
#python ./TDCMegascript.py -d -g PS_78469.xml -r 78469-78470 -t7
#python ./TDCMegascript.py -d -g PS_78473.xml -r 78471-78473 -t7
#python ./TDCMegascript.py -d -g PS_78474.xml -r 78474-78476 -t7
#python ./TDCMegascript.py -d -g PS_78479.xml -r 78477-78479 -t7
#python ./TDCMegascript.py -d -g PS_78481.xml -r 78480-78482 -t7
#python ./TDCMegascript.py -d -g PS_78485.xml -r 78483-78485 -t7
#python ./TDCMegascript.py -d -g PS_78463.xml -r 78465 -t7

#Threshold Scan Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_2024.xml -r 78544-78578 -t7

#Angle Scan Irradiated
#python ./TDCMegascript.py -d -g Run78579_2023_07_25.xml -r 78579-78581 -t7
#python ./TDCMegascript.py -d -g Run78582_2023_07_25.xml -r 78582-78584 -t7
#python ./TDCMegascript.py -d -g Run78585_2023_07_25.xml -r 78585-78587 -t7
#python ./TDCMegascript.py -d -g Run78588_2023_07_25.xml -r 78588-78590 -t7
#python ./TDCMegascript.py -d -g Run78591_2023_07_25.xml -r 78591-78593 -t7
#python ./TDCMegascript.py -d -g Run78594_2023_07_25.xml -r 78594,78596,78597 -t7
#python ./TDCMegascript.py -d -g Run78598_2023_07_25.xml -r 78598-78601 -t7
#python ./TDCMegascript.py -d -g Run78603_2023_07_25.xml -r 78603-78606 -t7
#python ./TDCMegascript.py -d -g Run78607_2023_07_25.xml -r 78607-78610 -t7
#python ./TDCMegascript.py -d -g Run78611_2023_07_25.xml -r 78611-78614 -t7
#python ./TDCMegascript.py -d -g Run78615_2023_07_25.xml -r 78615-78618 -t7
#python ./TDCMegascript.py -d -g Run78619_2023_07_25.xml -r 78619-78622 -t7
#python ./TDCMegascript.py -d -g Run78623_2023_07_25.xml -r 78623-78626 -t7
#python ./TDCMegascript.py -d -g Run78627_2023_07_25.xml -r 78627-78630 -t7
#python ./TDCMegascript.py -d -g Run78631_2023_07_25.xml -r 78631,78632,78634,78635 -t7
#python ./TDCMegascript.py -d -g Run78636_2023_07_25.xml -r 78636-78637 -t7
#python ./TDCMegascript.py -d -g Run78643_2023_07_25.xml -r 78643-78645 -t7

#TDC 0
#Threshold scan Not Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml -r 78371-78401 -t0

#Angle scan Not Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_1405.xml -r 78412-78420 -t0
#python ./TDCMegascript.py -d -g PS_78422.xml -r 78421-78424 -t0
#python ./TDCMegascript.py -d -g PS_78428.xml -r 78425-78429 -t0
#python ./TDCMegascript.py -d -g PS_78430.xml -r 78430-78433 -t0
#python ./TDCMegascript.py -d -g PS_78437.xml -r 78434-78437 -t0
#python ./TDCMegascript.py -d -g PS_78438.xml -r 78438-78441 -t0
#python ./TDCMegascript.py -d -g PS_78446.xml -r 78442-78446 -t0
#python ./TDCMegascript.py -d -g PS_78447.xml -r 78447-78450 -t0
#python ./TDCMegascript.py -d -g PS_78454.xml -r 78451-78454 -t0
#python ./TDCMegascript.py -d -g PS_78455.xml -r 78455-78458 -t0
#python ./TDCMegascript.py -d -g PS_78462.xml -r 78459-78462 -t0
#python ./TDCMegascript.py -d -g PS_78463.xml -r 78463-78466 -t0
#python ./TDCMegascript.py -d -g PS_78468.xml -r 78467-78468 -t0
#python ./TDCMegascript.py -d -g PS_78469.xml -r 78469-78470 -t0
#python ./TDCMegascript.py -d -g PS_78473.xml -r 78471-78473 -t0
#python ./TDCMegascript.py -d -g PS_78474.xml -r 78474-78476 -t0
#python ./TDCMegascript.py -d -g PS_78479.xml -r 78477-78479 -t0
#python ./TDCMegascript.py -d -g PS_78481.xml -r 78480-78482 -t0
#python ./TDCMegascript.py -d -g PS_78485.xml -r 78483-78485 -t0
#python ./TDCMegascript.py -d -g PS_78463.xml -r 78465 -t0

#Threshold Scan Irradiated
#python ./TDCMegascript.py -d -g Run78402_2023_08_14.xml -r 78371-78380 -t0
#python ./TDCMegascript.py -d -g Run78402_2023_08_14.xml -r 78371-78380 -t1
#python ./TDCMegascript.py -d -g Run78402_2023_08_14.xml -r 78371-78380 -t6
#python ./TDCMegascript.py -d -g Run78402_2023_08_14.xml -r 78371-78380 -t7

#Angle Scan Irradiated
#python ./TDCMegascript.py -d -g Run78579_2023_07_25.xml -r 78579-78581 -t0
#python ./TDCMegascript.py -d -g Run78582_2023_07_25.xml -r 78582-78584 -t0
#python ./TDCMegascript.py -d -g Run78585_2023_07_25.xml -r 78585-78587 -t0
#python ./TDCMegascript.py -d -g Run78588_2023_07_25.xml -r 78588-78590 -t0
#python ./TDCMegascript.py -d -g Run78591_2023_07_25.xml -r 78591-78593 -t0
#python ./TDCMegascript.py -d -g Run78594_2023_07_25.xml -r 78594,78596,78597 -t0
#python ./TDCMegascript.py -d -g Run78598_2023_07_25.xml -r 78598-78601 -t0
#python ./TDCMegascript.py -d -g Run78603_2023_07_25.xml -r 78603-78606 -t0
#python ./TDCMegascript.py -d -g Run78607_2023_07_25.xml -r 78607-78610 -t0
#python ./TDCMegascript.py -d -g Run78611_2023_07_25.xml -r 78611-78614 -t0
#python ./TDCMegascript.py -d -g Run78615_2023_07_25.xml -r 78615-78618 -t0
#python ./TDCMegascript.py -d -g Run78619_2023_07_25.xml -r 78619-78622 -t0
#python ./TDCMegascript.py -d -g Run78623_2023_07_25.xml -r 78623-78626 -t0
#python ./TDCMegascript.py -d -g Run78627_2023_07_25.xml -r 78627-78630 -t0
#python ./TDCMegascript.py -d -g Run78631_2023_07_25.xml -r 78631,78632,78634,78635 -t0
#python ./TDCMegascript.py -d -g Run78636_2023_07_25.xml -r 78636-78637 -t0
#python ./TDCMegascript.py -d -g Run78643_2023_07_25.xml -r 78643-78645 -t0

#Voltage Scans (all tdcs, irradiated then non-irradiated)
#python ./Kalman_Megascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78366-78370 -t6
python ./Kalman_Megascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78366-78370 -t6

#python ./TDCMegascript.py -d -g 2023_06_PS_07_14_0805.xml -r 78661-78671 -t6
#python ./TDCMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78402-78410 -t6
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_2024.xml -r 78519,78521,78523 -t6

python ./Kalman_Megascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78366-78370 -t7
#python ./TDCMegascript.py -d -g 2023_06_PS_07_14_0805.xml -r 78661-78671 -t7
#python ./TDCMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78402-78410 -t7
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_2024.xml -r 78519,78521,78523 -t7

python ./Kalman_Megascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78366-78370 -t1
python ./Kalman_Megascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78366-78370 -t0
#python ./TDCMegascript.py -d -g 2023_06_PS_07_14_0805.xml -r 78661-78671 -t#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml -r 78402-78410 -t0
#python ./TDCMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78402-78410 -t0
#python ./TDCMegascript.py --RunChewie false -d -g Run78402_Merged.geo -r 78402-78410 -t1
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_2024.xml -r 78519,78521,78523 -t0


#TROUBLESHOOTING BELOW:

#Use this one when files have not been copied and merged
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_1405.xml --CopyRawFiles true --MergeRawFiles true -r 78414 -t6

#python ./TDCMegascript.py -d -g Run78615_2023_07_25.xml -r 78618 -t6
#python ./TDCMegascript.py -d -g Run78603_2023_07_25.xml -r 78603 -t6
