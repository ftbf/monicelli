#!/bin/bash


#TDC Studies
RunNumbers=(78340 78339 78338 78337 78329 78330 78331 78332 78333 78334 78335 78336 78341 78342 78343 78344 78345)

for r in ${RunNumbers[@]}
do
    echo $r
    python ./Megascript.py --CopyRawFiles false --MergeRawFiles false -g 2023_06_PS_07_12_1824.xml -d -r $r -e 15000

    for t in 0 1 2 3 4 5 6 7
    do
	    python ./TDCMegascript.py --CopyRawFiles false --MergeRawFiles false -g 2023_06_PS_07_12_1824.xml -d -t $t -r $r  &
    done
done
