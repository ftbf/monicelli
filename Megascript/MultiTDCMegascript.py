#!/usr/bin/env python
import argparse
from argparse import RawTextHelpFormatter
import xml.etree.ElementTree as ET
import sys,os,subprocess,time,datetime,string, threading, re
import distutils
from distutils import util
from email.mime.text import MIMEText

def get_env(variable_name):
    # function to get an enviromental variable, with a safety check.
    # If the variable is not defined then it prints an error and 
    # stops the script

    if variable_name in os.environ:
        return os.environ[variable_name]
    else:
        print(f"Error: Environment variable '{variable_name}' is not defined.")
        exit()

class Parameters:
    #################################################################################################
    #General parameters
    #################################################################################################
    MaxThreads  		 = 20;
    CopyRawFiles		 = False;
    MergeRawFiles		 = False;
    RunMonicelli		 = True;
    RunChewie			 = True;
    Tdc                  = 0;
    TestBeamYear		 = "2023";
    TestBeamMonth		 = "06";
    TestBeamMonthName	 = "June";
    TestBeamYearMonth	 = TestBeamYear + TestBeamMonthName;
    BaseDataDir_root     = get_env("ALLDATADIR");
    BaseDataDir 		 = BaseDataDir_root + "/" + TestBeamYear + "_" + TestBeamMonth + "_" + TestBeamMonthName + "_PSModule" + '/';
    GeometriesDataDir	 = BaseDataDir + "Geometries/"
    CopyScriptName		 = BaseDataDir + "megaCopy.csh";
    MergerInputDir		 = BaseDataDir;
    MergerOutputDir		 = BaseDataDir + "Merged/";
    # MergerName	         = "/home/otsdaq/TestBeamPSModule/Merger/build/Merger";
    AnalysisDir_root     = get_env("ANALYSISDIR") 
    MergerName	         = AnalysisDir_root + "/Merger/";
    FileNameToProcess	 = "Run{no}_Merged_tdc_{tno}.dat";#"Run{no}.dat";
    MailList			 = "";
    #MailList			 = "uplegger@cern.ch, jlawless@fnal.gov, laceyd@fnal.gov, ehagen@fnal.gov, jpeltier@fnal.gov, enibigir@fnal.gov, iqrasohail766@gmail.com, izoi@fnal.gov, fabio.ravera@cern.ch, jennetd@fnal.gov, alee43@nd.edu,gzamouranisolivia@gmail.com,";
    TemplateFilesDir	 =  get_env("MEGASCRIPTDIR") + "/templates/";

    #################################################################################################
    #Monicelli parameters
    #################################################################################################
    # MonicelliDir        = "/home/modtest/Programming/TestBeamAnalysis_PS_2023/Monicelli/";
    MonicelliDir        = get_env("MonicelliDir");
    MonicelliOutDataDir = get_env("MonicelliOutputDir")+"/"
    GeometryFile	    = "PS_78430.xml";
    #File used as a template by the megascript to substitute the run numbers and geometry in order to create the xml file that MonicelliExpress uses to run 
    MonicelliExpressTemplateFilePath = MonicelliDir + "Express/xml/";
    MonicelliExpressTemplateFileName = TestBeamYearMonth + "_MonicelliExpress_template.xml";

    #################################################################################################
    #Chewie parameters
    #################################################################################################
    ChewieDir	        = AnalysisDir_root + "/Chewie/";
    #File used as a template by the megascript to substitute the run numbers and chewie configuration in order to create the xml file that ChewieExpress uses to run 
    ChewieExpressTemplateFilePath	= ChewieDir + "Express/xml/";
    ChewieExpressTemplateFileName	= TestBeamYearMonth + "_ChewieExpress_template.xml";

    #Name of the configuration file for Chewie, written in the xml that ChewieExpress uses to run
    ChewieConfigurationFilePath 	= ChewieDir + "xml/";
    ChewieConfigurationFileName 	= TestBeamYearMonth + "_ChewieConfiguration.xml";

    #File used as a template by the megascript in case NO chewieConfigurationFileName exists!
    ChewieConfigurationTemplateFileName = TemplateFilesDir + "Chewie_TestBeamConfiguration_template.xml";

    #################################################################################################
    #MonicelliExpressTemplate parameters
    #################################################################################################
    FilesPath		      = MergerOutputDir; 
    GeometriesPath	      = GeometriesDataDir; 
    TrackFindingAlgorithm = "FirstAndLast"; 
    TrackFittingAlgorithm = "Simple";
    NumberOfEvents	      = "-1";
    TrackPoints 	      = "13"; 
    MaxPlanePoints	      = "5"; 
    MaxPlanePointsDUT	  = "50"; 
    Chi2Cut		          = "5"; 
    XTolerance  	      = "200"; 
    YTolerance  	      = "200";
    XToleranceDUT  	      = "200"; 
    YToleranceDUT  	      = "3000";
    FindDut		          = "true";
    RawAlignment	      = "false";
    FineAlignment	      = "false";
    FineTelescopeAlignment= "false";
    UseEtaFunction	      = "false";

    #################################################################################################
    #EnvironmentVariables parameters
    #################################################################################################
    #monicelliDir	     = MonicelliDir;									 
    Monicelli_RawData_Dir    = MergerOutputDir;									      
    Monicelli_DataSample_Dir = MergerOutputDir;									      
    Monicelli_CalSample_Dir  = BaseDataDir + "Calibrations";							      
    MonicelliOutputDir       = BaseDataDir + "MonicelliOutput";							      
    Monicelli_XML_Dir        = BaseDataDir + "Geometries";
    MONICELLITMPAREA         = "/tmp";
    ROOTSYS		             = get_env("ROOTSYS");
    QTDIR		             = get_env("QTDIR");									      
    LD_LIBRARY_PATH	         = ROOTSYS + "/lib:" + QTDIR + "/lib:" + get_env("XERCESCLIB")+":" + MonicelliDir + "/plugins/libs/";
    MERGER_INPUT_DIR	     = MergerInputDir;									      
    MERGER_OUTPUT_DIR        = MergerOutputDir;								      
    #CHEWIEDIR  	          = chewieDir;									      
    CHEWIEDATADIR	         = BaseDataDir + "MonicelliOutput";							      
    CHEWIEINPUTDIR	         = BaseDataDir + "ChewieInput";  							      
    CHEWIEOUTPUTDIR	         = BaseDataDir + "ChewieOutput"; 							      
    CHEWIEXMLDIR	         = ChewieDir + "xml"									      
	

###########################################################################################
def convertEnvVariables(name, parameters):
    #All Env variables preceded by $ will be substitute
    expression = "(\$(\(|\{)*(\w+)(\)|\})*)";
    regExp = re.search(expression,name);
    while( regExp ):
        variable = getattr(parameters, regExp.group(3));
        shortName = name[name.find(regExp.group(1))+len(regExp.group(1)):len(name)];
        name = name.replace(regExp.group(1),variable);
        regExp = re.search(expression,shortName);
    return name

#################################################################################################
def parseMegascriptConfigurationFile(configurationFile):
    pars = Parameters();
    tree = ET.parse(configurationFile)
    root = tree.getroot()
    #root = ET.fromstring(country_data_as_string)
    for child in root:
        #print(child.tag)
        if(child.tag == "General"):
            pars.MaxThreads	       = int(child.find("MaxThreads").text);
            pars.CopyRawFiles      = bool(distutils.util.strtobool((child.find("CopyRawFiles").text).replace(" ","").replace('\t',"")));
            pars.MergeRawFiles     = bool(distutils.util.strtobool((child.find("MergeRawFiles").text).replace(" ","").replace('\t',"")));
            pars.RunMonicelli      = bool(distutils.util.strtobool((child.find("RunMonicelli").text).replace(" ","").replace('\t',"")));
            pars.RunChewie	       = bool(distutils.util.strtobool((child.find("RunChewie").text).replace(" ","").replace('\t',"")));
        
            pars.TestBeamYear	   = (child.find("TestBeamYear").text).replace(" ","").replace('\t',"");
            pars.TestBeamMonth     = (child.find("TestBeamMonth").text).replace(" ","").replace('\t',"");
            pars.TestBeamMonthName = (child.find("TestBeamMonthName").text).replace(" ","").replace('\t',"");
            pars.TestBeamYearMonth = convertEnvVariables((child.find("TestBeamYearMonth").text).replace(" ","").replace('\t',""), pars);
            pars.BaseDataDir	   = convertEnvVariables((child.find("BaseDataDir").text).replace(" ","").replace('\t',""), pars);
            pars.GeometriesDataDir = convertEnvVariables((child.find("GeometriesDataDir").text).replace(" ","").replace('\t',""), pars);
            pars.CopyScriptName	   = convertEnvVariables((child.find("CopyScriptName").text).replace(" ","").replace('\t',""), pars);
            pars.MergerInputDir    = convertEnvVariables((child.find("MergerInputDir").text).replace(" ","").replace('\t',""), pars);
            pars.MergerOutputDir   = convertEnvVariables((child.find("MergerOutputDir").text).replace(" ","").replace('\t',""), pars);
            pars.FileNameToProcess = (child.find("FileNameToProcess").text).replace(" ","").replace('\t',"");
            pars.MailList	       = (child.find("MailList").text).replace(" ","").replace('\t',"");
            pars.TemplateFilesDir  = (child.find("TemplateFilesDir").text).replace(" ","").replace('\t',"");

        elif(child.tag == "Monicelli"):
            pars.MonicelliDir			          = convertEnvVariables((child.find("MonicelliDir").text).replace(" ","").replace('\t',""), pars);
            pars.MonicelliOutDataDir	          = convertEnvVariables((child.find("MonicelliOutDataDir").text).replace(" ","").replace('\t',""), pars);
            pars.GeometryFile			          = (child.find("GeometryFile").text).replace(" ","").replace('\t',"");
            pars.MonicelliExpressTemplateFilePath = convertEnvVariables((child.find("MonicelliExpressTemplateFilePath").text).replace(" ","").replace('\t',""), pars);
            pars.MonicelliExpressTemplateFileName = convertEnvVariables((child.find("MonicelliExpressTemplateFileName").text).replace(" ","").replace('\t',""), pars);

        elif(child.tag == "Chewie"):
            pars.ChewieDir			                 = convertEnvVariables((child.find("ChewieDir").text).replace(" ","").replace('\t',""), pars);
            pars.ChewieExpressTemplateFilePath       = convertEnvVariables((child.find("ChewieExpressTemplateFilePath").text).replace(" ","").replace('\t',""), pars);
            pars.ChewieExpressTemplateFileName       = convertEnvVariables((child.find("ChewieExpressTemplateFileName").text).replace(" ","").replace('\t',""), pars);
            pars.ChewieConfigurationFilePath	     = convertEnvVariables((child.find("ChewieConfigurationFilePath").text).replace(" ","").replace('\t',""), pars);
            pars.ChewieConfigurationFileName	     = convertEnvVariables((child.find("ChewieConfigurationFileName").text).replace(" ","").replace('\t',""), pars);
            pars.ChewieConfigurationTemplateFileName = convertEnvVariables((child.find("ChewieConfigurationTemplateFileName").text).replace(" ","").replace('\t',""), pars);

        elif(child.tag == "MonicelliExpressTemplate"):
            pars.FilesPath	             = convertEnvVariables((child.find("FilesPath").text).replace(" ","").replace('\t',""), pars);
            pars.GeometriesPath          = convertEnvVariables((child.find("GeometriesPath").text).replace(" ","").replace('\t',""), pars);
            pars.TrackFindingAlgorithm   = (child.find("TrackFindingAlgorithm").text).replace(" ","").replace('\t',"");
            pars.TrackFittingAlgorithm   = (child.find("TrackFittingAlgorithm").text).replace(" ","").replace('\t',"");
            pars.NumberOfEvents          = (child.find("NumberOfEvents").text).replace(" ","").replace('\t',"");
            pars.TrackPoints	         = (child.find("TrackPoints").text).replace(" ","").replace('\t',"");
            pars.MaxPlanePoints          = (child.find("MaxPlanePoints").text).replace(" ","").replace('\t',"");
            pars.MaxPlanePointsDUT       = (child.find("MaxPlanePointsDUT").text).replace(" ","").replace('\t',"");
            pars.Chi2Cut	             = (child.find("Chi2Cut").text).replace(" ","").replace('\t',"");
            pars.XTolerance	             = (child.find("XTolerance").text).replace(" ","").replace('\t',"");
            pars.YTolerance	             = (child.find("YTolerance").text).replace(" ","").replace('\t',"");
            pars.XToleranceDUT	         = (child.find("XToleranceDUT").text).replace(" ","").replace('\t',"");
            pars.YToleranceDUT	         = (child.find("YToleranceDUT").text).replace(" ","").replace('\t',"");
            pars.FindDut	             = (child.find("FindDut").text).replace(" ","").replace('\t',"");
            pars.RawAlignment            = (child.find("RawAlignment").text).replace(" ","").replace('\t',"");
            pars.FineAlignment           = (child.find("FineAlignment").text).replace(" ","").replace('\t',"");
	    #print "-" + pars.FineAlignment + "-"
	    #exit();
            pars.FineTelescopeAlignment  = (child.find("FineTelescopeAlignment").text).replace(" ","").replace('\t',"");
            pars.UseEtaFunction          = (child.find("UseEtaFunction").text).replace(" ","").replace('\t',"");

        elif(child.tag == "EnvironmentVariables"):
    	    #pars.monicelliDir  	  = convertEnvVariables((child.find("MonicelliDir").text);  	 
            pars.Monicelli_RawData_Dir   = convertEnvVariables((child.find("Monicelli_RawData_Dir").text).replace(" ","").replace('\t',""), pars);   
            pars.Monicelli_DataSample_Dir= convertEnvVariables((child.find("Monicelli_DataSample_Dir").text).replace(" ","").replace('\t',""), pars);
            pars.Monicelli_CalSample_Dir = convertEnvVariables((child.find("Monicelli_CalSample_Dir").text).replace(" ","").replace('\t',""), pars); 
            pars.MonicelliOutputDir	     = convertEnvVariables((child.find("MonicelliOutputDir").text).replace(" ","").replace('\t',""), pars);	 
            pars.Monicelli_XML_Dir	     = convertEnvVariables((child.find("Monicelli_XML_Dir").text).replace(" ","").replace('\t',""), pars);	 
            pars.MONICELLITMPAREA	     = convertEnvVariables((child.find("MONICELLITMPAREA").text).replace(" ","").replace('\t',""), pars);	 
            pars.ROOTSYS		         = convertEnvVariables((child.find("ROOTSYS").text).replace(" ","").replace('\t',""), pars);		  
            pars.QTDIR  		         = convertEnvVariables((child.find("QTDIR").text).replace(" ","").replace('\t',""), pars);  		  
            pars.LD_LIBRARY_PATH	     = convertEnvVariables((child.find("LD_LIBRARY_PATH").text).replace(" ","").replace('\t',""), pars);	  
            pars.MERGER_INPUT_DIR	     = convertEnvVariables((child.find("MERGER_INPUT_DIR").text).replace(" ","").replace('\t',""), pars);	  
            pars.MERGER_OUTPUT_DIR	     = convertEnvVariables((child.find("MERGER_OUTPUT_DIR").text).replace(" ","").replace('\t',""), pars);	 
            #pars.CHEWIEDIR		          = convertEnvVariables((child.find("CHEWIEDIR").text).replace(" ","").replace('\t',""), pars);		  
            pars.CHEWIEDATADIR  	     = convertEnvVariables((child.find("CHEWIEDATADIR").text).replace(" ","").replace('\t',""), pars);  	  
            pars.CHEWIEINPUTDIR 	     = convertEnvVariables((child.find("CHEWIEINPUTDIR").text).replace(" ","").replace('\t',""), pars); 	  
            pars.CHEWIEOUTPUTDIR	     = convertEnvVariables((child.find("CHEWIEOUTPUTDIR").text).replace(" ","").replace('\t',""), pars);	  
            pars.CHEWIEXMLDIR		     = convertEnvVariables((child.find("CHEWIEXMLDIR").text).replace(" ","").replace('\t',""), pars);
    return pars;
	    	     
###########################################################################################
def exportVars(parameters):
    #Exporting variables for Merger
    os.environ['MERGER_INPUT_DIR'        ] = parameters.MergerInputDir;
    #print(parameters.MergerInputDir)
    os.environ['MERGER_OUTPUT_DIR'       ] = parameters.MergerOutputDir;       
    #print(parameters.MergerOutputDir)

    #Exporting variables for Monicelli
    os.environ['MonicelliDir'            ] = parameters.MonicelliDir;           
    #print(parameters.MonicelliDir)
    os.environ['Monicelli_RawData_Dir'   ] = parameters.Monicelli_RawData_Dir;   
    #print(parameters.Monicelli_RawData_Dir)
    os.environ['Monicelli_DataSample_Dir'] = parameters.Monicelli_DataSample_Dir;
    os.environ['Monicelli_CalSample_Dir' ] = parameters.Monicelli_CalSample_Dir; 
    os.environ['MonicelliOutputDir'      ] = parameters.MonicelliOutputDir;      
    #print(parameters.MonicelliOutputDir)
    os.environ['Monicelli_XML_Dir'       ] = parameters.Monicelli_XML_Dir;   
    os.environ['MONICELLITMPAREA'        ] = parameters.MONICELLITMPAREA;       
    os.environ['ROOTSYS'                 ] = parameters.ROOTSYS;                 
    os.environ['QTDIR'                   ] = parameters.QTDIR;                   
    #print(parameters.QTDIR)
    os.environ['LD_LIBRARY_PATH'         ] = parameters.LD_LIBRARY_PATH;         
    #print(parameters.LD_LIBRARY_PATH)

    #Exporting variables for Chewie
    os.environ['CHEWIEDIR'               ] = parameters.ChewieDir;               
    os.environ['CHEWIEDATADIR'           ] = parameters.CHEWIEDATADIR;           
    os.environ['CHEWIEINPUTDIR'          ] = parameters.CHEWIEINPUTDIR;          
    os.environ['CHEWIEOUTPUTDIR'         ] = parameters.CHEWIEOUTPUTDIR;         
    os.environ['CHEWIEXMLDIR'            ] = parameters.CHEWIEXMLDIR;            

###########################################################################################
def makeMonicelliExpressTemplate(fileOut, parameters):
    outFile = open(fileOut,"w")
    outFile.write("<?xml version='1.0' encoding='iso-8859-1'?>\n");
    outFile.write("<!DOCTYPE MonicelliExpressConfiguration SYSTEM \"./dtd/ExpressConfiguration.dtd\">\n");
    outFile.write("<MonicelliExpressConfiguration>\n");
    outFile.write("  <Defaults FilesPath=\"" + parameters.FilesPath + "\"\n");
    outFile.write("	       GeometriesPath=\"" + parameters.GeometriesPath + "\"\n");
    outFile.write("	       TrackFindingAlgorithm=\"" + parameters.TrackFindingAlgorithm + "\"\n");
    outFile.write("	       TrackFittingAlgorithm=\"" + parameters.TrackFittingAlgorithm + "\"\n");
    outFile.write("	       NumberOfEvents=\"" + parameters.NumberOfEvents + "\"\n");
    outFile.write("	       TrackPoints=\"" + parameters.TrackPoints + "\"\n");
    outFile.write("	       MaxPlanePoints=\"" + parameters.MaxPlanePoints + "\"\n");
    outFile.write("	       MaxPlanePointsDUT=\"" + parameters.MaxPlanePointsDUT + "\"\n");
    outFile.write("	       Chi2Cut=\"" + parameters.Chi2Cut + "\"\n");
    outFile.write("	       XTolerance=\"" + parameters.XTolerance + "\"\n");
    outFile.write("	       YTolerance=\"" + parameters.YTolerance + "\"\n");
    outFile.write("	       XToleranceDUT=\"" + parameters.XToleranceDUT + "\"\n");
    outFile.write("	       YToleranceDUT=\"" + parameters.YToleranceDUT + "\"\n");
    outFile.write("	       FindDut=\"" + parameters.FindDut + "\"\n");
    outFile.write("	       RawAlignment=\"" + parameters.RawAlignment + "\"\n");
    outFile.write("	       FineAlignment=\"" + parameters.FineAlignment + "\"\n");
    outFile.write("	       FineTelescopeAlignment=\"" + parameters.FineTelescopeAlignment + "\"\n");
    outFile.write("	       UseEtaFunction=\"" + parameters.UseEtaFunction + "\"/>\n");
    outFile.write("</MonicelliExpressConfiguration>\n");
    outFile.close();
    f = open(fileOut, 'r')
    print(f.read())
    f.close()

###########################################################################################
def makeChewieExpressTemplate(fileOut, parameters):
    outFile = open(fileOut,"w")
    outFile.write("<?xml version='1.0' encoding='iso-8859-1'?>\n");
    outFile.write("<!DOCTYPE ChewieExpressConfiguration SYSTEM \"./dtd/ExpressConfiguration.dtd\">\n");
    outFile.write("<ChewieExpressConfiguration>\n");
    outFile.write("  <Defaults FilesPath=\"" + parameters.MonicelliOutDataDir + "\"\n");
    outFile.write("	      Convert=\"yes\"\n");
    outFile.write("	      RunAnalysis=\"yes\"\n");
    outFile.write("	      NumberOfEvents=\"" + parameters.NumberOfEvents + "\"/>\n");
    outFile.write("</ChewieExpressConfiguration>\n");
    outFile.close();
    f = open(fileOut, 'r')
    print(f.read())
    f.close()


###########################################################################################
def sshName(name):
    name = string.replace(name,"~","\\~");
    name = string.replace(name,"!","\\!");
    name = string.replace(name,"@","\\@");
    name = string.replace(name,"#","\\#");
    name = string.replace(name,"%","\\%");
    name = string.replace(name,"^","\\^");
    name = string.replace(name,"&","\\&");
    name = string.replace(name,"*","\\*");
    name = string.replace(name,"(","\\(");
    name = string.replace(name,")","\\)");
    name = string.replace(name,"[","\\[");
    name = string.replace(name,"]","\\]");
    name = string.replace(name,"{","\\{");
    name = string.replace(name,"}","\\}");
    name = string.replace(name,";","\\;");
    name = string.replace(name,"'","\\'");
    name = string.replace(name,",","\\,");
    name = string.replace(name,"<","\\<");
    name = string.replace(name,">","\\>");
    name = string.replace(name,"?","\\?");
    name = string.replace(name," ","\ "); 
    #This must stay after the substitution of *
    name = string.replace(name,'"','*');
    
    return name
###########################################################################################
def copy(files, sourceDir, destinationDir, sourceHost, destinationHost):

    copiedFiles = [];
    for file in files:
        cmd = makeCPCommand(file, sourceDir, destinationDir, sourceHost, destinationHost);
        print(cmd);
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
        out,err = proc.communicate();
        if(err != ''):
            print(err)
        else:
            copiedFiles.append(file)
    return copiedFiles;
    
###########################################################################################
def makeSysCommand(command, arg, host=''):
    cmd = '';
    if(host == ''):
        cmd = command + " \"" + arg + "\"";
    else:
        cmd = "ssh " + host + " \"" + command + " " + sshName(arg) + "\"";
    return cmd;

###########################################################################################
def makeCPCommand(fileName, sourceHost, sourceDir, destinationHost, destinationDir):
    cmd = '';
    #print fileName
    destinationDir += fileName[:fileName.rfind('/')+1];
    #print directory
    
    #if(sourceHost == '' and destinationHost == ''):
    #	 cmd = "cp -p " + sshName(sourceDir + fileName) + " " + sshName(destinationDir + fileName);
    #elif(sourceHost == '' and destinationHost != ''):
    #	 cmd = "scp -p "  + sshName(sourceDir + fileName) + " " + destinationHost + ":\"" + sshName(destinationDir + fileName) + '\"';
    #elif(sourceHost != '' and destinationHost == ''):
    #	 cmd = "scp -p " + sourceHost + ":\"" + sshName(sourceDir + fileName) + "\" " + sshName(destinationDir + fileName);
    #elif(sourceHost != '' and destinationHost != ''):
    #	 cmd = "scp -p " + sourceHost + ":\"" + sshName(sourceDir + fileName) + "\" " + destinationHost + ":\"" + sshName(destinationDir + fileName) + '\"';
    if(sourceHost == '' and destinationHost == ''):
        cmd = "cp -p \"" + sourceDir + fileName + "\" " + '\"' + destinationDir + '\"';
    elif(sourceHost == '' and destinationHost != ''):
        cmd = "scp -p \""  + sourceDir + fileName + "\" " + destinationHost + ":\"" + sshName(destinationDir) + '\"';
    elif(sourceHost != '' and destinationHost == ''):
        cmd = "scp -p " + sourceHost + ":\"" + sshName(sourceDir + fileName) + "\" \"" + destinationDir + '\"';
    elif(sourceHost != '' and destinationHost != ''):
        cmd = "scp -p " + sourceHost + ":\"" + sshName(sourceDir + fileName) + "\" " + destinationHost + ":\"" + sshName(destinationDir) + '\"';
    return cmd;

###########################################################################################
def makeFileList(host, directory, filter=''):
    cmd = makeSysCommand("ls -la", directory, host);
    #print(cmd);
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
    out,err = tuple(element.decode() for element in proc.communicate())

    if(err.find('Permission denied') != -1):
       return ;
    fileList = out.split('\n');
    #print fileList;
    del fileList[0];
    fileList.pop(); #for some reason it also gives you an empty line at the end
    files = [];
    for file in fileList:
        parsed = parsell(file);
	    #print(parsed)
        if(parsed[8] == '.' or parsed[8] == '..'):
            continue ;
        if(parsed[8].find('"') != -1):
            print("The file or dir " + directory + parsed[8] + " contains a \" character that I dont' want to deal with!! I won't copy it");
            continue ;
        name = parsed[8];
	    #print(name);
        if('d' not in parsed[0] and name.find(filter) != -1):
            files.append(name)

    #print(files);
    return files;

###########################################################################################
def parsell(line):
    fields = line.split(' ');
    nFields = len(fields);
    i = 0;
    withoutName = '';
    for counter in range(nFields):
        #print str(i) + " " + fields[i];
        if(i>=8):
            del fields[i];
        else:
            if(fields[i] == ''):
                del fields[i];
            else:
                withoutName += fields[i];
                i += 1;
            withoutName += ' ';
    #print line
    #print withoutName
    fields.append(line[len(withoutName):]);
    return fields;

###########################################################################################
def splitHostDir(name):
    split = name.split(':');
    host = '';
    dir  = name;
    if(len(split) == 2):
        host = split[0];
        dir  = split[1];
    #if(dir[len(dir)-1] == '/'):
#	dir = dir[:len(dir)-1];
    if(dir[len(dir)-1] != '/'):
        dir += '/';

    return host,dir;

###########################################################################################
def replaceInFile(fileIn, fileOut, replaceWhat, replaceWith):
    testFile = open(fileIn,"r")
    testFileContent = testFile.readlines()
    testFile.close()
    i=0
    outFile = open(fileOut,"w")
    for line in testFileContent:
        if(line.find(replaceWhat) ):
            testFileContent[i] = line.replace(replaceWhat,replaceWith);
        outFile.write(testFileContent[i])
        i = i+1
    outFile.close()

###########################################################################################
def monicelliExpressTemplateReplace(fileIn, fileOut, runList, geometry):
    testFile = open(fileIn,"r")
    testFileContent = testFile.readlines()
    testFile.close()
    i=0
    outFile = open(fileOut,"w")
    for line in testFileContent:
        if(line.find("</MonicelliExpressConfiguration>") != -1):
            for run in runList:
                print(run)
                outFile.write("   <File Name=\"" + run + "\" Geometry=\"" + geometry + "\" />\n");
        outFile.write(line)
    outFile.close()

###########################################################################################
def chewieExpressTemplateReplace(parameters, fileIn, fileOut, runList):
    testFile = open(fileIn,"r")
    testFileContent = testFile.readlines()
    testFile.close()

    outFileName = "";
    if(len(runList) != 1):
        firstTdc = re.findall(r'\d+', runList[0])[1]
        lastTdc  = re.findall(r'\d+', runList[-1])[1]
        outFileName = runList[0].replace("tdc_"+ firstTdc +".dat","tdc_"+ firstTdc + "-" + lastTdc + "_Chewie.root")
 
    outFile = open(fileOut,"w")
    for line in testFileContent:
        if(line.find("</ChewieExpressConfiguration>") != -1):
            if(len(runList) == 1):
                outFile.write("    <Files Configuration=\"" + parameters.ChewieConfigurationFileName + "\" OutFileName=\"" + runList[0].replace(".dat","") + "_Chewie.root\">\n      <File Name=\"" + runList[0].replace(".dat",".root")  + "\"/>\n    </Files>\n");
            else:
                outFile.write("    <Files Configuration=\"" + parameters.ChewieConfigurationFileName + "\" OutFileName=\"" + outFileName + "\">\n")
                for run in runList:
                    outFile.write("      <File Name=\"" + run.replace(".dat",".root")  + "\"/>\n");
                outFile.write("    </Files>\n");
                
        outFile.write(line)
    outFile.close()

###########################################################################################
def makeFileListToProcess(parameters, run, tdc):
    runList = []    
    s = parameters.FileNameToProcess.format(no=run,tno=tdc)
    runList.append(s)
    #print("File to process: " + s)
    return runList

###########################################################################################
def sendEmail(mailList, title, body):
    if(mailList == ""): return;
    print("Sending email to " + mailList + " with body:\n" + body);
    list = mailList.split(',')
    for email in list:
        p = os.popen("mail -s \"" + title + "\" " + email ,"w")
        p.write(body)
        status = p.close() 


###########################################################################################
def rawCopy(runBegin, runEnd):
    controllersList = ["CC_DUT", "CC_DS", "CC_US", "CC_STIB_MASTER"];
    for run in range(runBegin,runEnd+1):
        for controller in controllersList:
            cmd = "scp -rp " + os.environ["LOGNAME"] + '@' + remoteComputer + ':\"' + remoteDir + "Software/" + controller + "/Burst\ Archive/Run" + str(run) + "_*\" " + baseDataDir + "Raw/" + controller + "/Burst\ Archive";
            print("Running command: " + cmd);

            proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
            out,error = proc.communicate();
            if(error != ''):
                print(error)
                subject = "FATAL: Raw copy error!"
                sendEmail(mailList, subject, error);
            break;
            print(out);
#${originComputer}:"/media/sf_C_DRIVE/${originFolder}/Software/CC_DUT/Burst\ Archive/${runName}${runNumber}_*" /data/TestBeam/${destinationFolder}/Raw/CC_DUT/Burst\ Archive/  
        #scp -rp os.environ["LOGNAME"]@${originComputer}:"/media/sf_C_DRIVE/${originFolder}/Software/CC_DUT/Burst\ Archive/${runName}${runNumber}_*" /data/TestBeam/${destinationFolder}/Raw/CC_DUT/Burst\ Archive/  
    

###########################################################################################
def is_ascii(s):
#    print s, ord(s)
    if(ord(s) > 31 and ord(s) < 128):
        return True;
    return False;


############################################################################################
def main():

    ###########################################################################################
    #Parsing Arguments 
    ###########################################################################################
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument("-r", "--RunList", required=True, type=str, help="Run list.\n"
    "Examples of run list:\n"
    "1 -> only Run1\n"
    "1-10 -> from Run1 to Run10\n"
    "1-10,20,30-35 -> from Run1 to Run10, Run20, from Run30 to Run35"
    )
    parser.add_argument("-t", "--Tdc",     required=True, type=str, help="TDC Number.\n"
     "Examples of TDC list:\n"
    "1 -> only TDC 1\n"
    "1-7 -> from TDC 1 to TDC 7\n"
    "1-3,5,7 -> from TDC 1 to TDC 3, TDC 5, TDC 7"
    )

    parser.add_argument("-c", "--Configuration",                         help="Megascript xml configuration file")
    parser.add_argument("-d", "--Delete",                                help="Removes all the template files", action='store_true')
    parser.add_argument("-e", "--Events",                      type=int, help="Number of events to process.(-1 means all)")
    parser.add_argument("-f", "--Fit",                         type=str, help="Monicelli track fit (Simple, Kalman)")
    parser.add_argument("-g", "--Geometry",                    type=str, help="Geometry file name")
    parser.add_argument("--RunMonicelli",        default=True, type=lambda x:bool(distutils.util.strtobool(x)), help="Run Monicelli (default True)")
    parser.add_argument("--RunChewie",           default=True, type=lambda x:bool(distutils.util.strtobool(x)), help="Run Chewie (default True)")
    parser.add_argument("--ChewieTemplate",                    type=str, help="Chewie template file name for ChewieExpress")
    parser.add_argument("--ChewieConfiguration",               type=str, help="Chewie analysis configuration file")
    parser.add_argument("--CopyRawFiles",        default=False,type=lambda x:bool(distutils.util.strtobool(x)), help="Copy files to be merged using another script (default False)")
    parser.add_argument("--MergeRawFiles",       default=False,type=lambda x:bool(distutils.util.strtobool(x)), help="Merge files using the merger (default False)")
    parser.add_argument("--ConvertOutput",       default=False,type=lambda x:bool(distutils.util.strtobool(x)), help="Convert monicelli output files for other experiments (default False)")
    parser.add_argument("-xyTol", "--Window",                  type=int, help="Road search window")
    parser.parse_args()
    args = parser.parse_args()
    
    ###########################################################################################
    #Creating run list
    ###########################################################################################
    rangedFileList = args.RunList.split(',');
    runList = [];
    #print("Printing Runs");
    for runs in rangedFileList:
        #print runs;
        if(runs.find('-') != -1):
            begin,end = runs.split('-');
        else:
            begin = end = runs;
	    #print "B: " + begin + " E: " + end; 
        if(int(begin) > int(end)):
            print("Range " + runs + " is not valid since the starting run is greater than the end run!");
            sys.exit();
        #print(runs);
        for run in range(int(begin),int(end)+1):
            runList.append(str(run));

    parameters = Parameters();
    rangedTdcList = args.Tdc.split(',');
    tdcList = [];
    for tdcs in rangedTdcList:

        if(tdcs.find('-') != -1):
            tbegin,tend = tdcs.split('-');
        else:
            tbegin = tend = tdcs;
	    #print "B: " + begin + " E: " + end; 
        if(int(tbegin) > int(tend)):
            print("Range " + tdcs + " is not valid since the starting tdc is greater than the end tdc!");
            sys.exit();
        #print(runs);
        for tdc in range(int(tbegin),int(tend)+1):
            tdcList.append(tdc);
    
    if(args.Configuration):
        parameters = parseMegascriptConfigurationFile(args.Configuration);

    if(args.Events):
        parameters.NumberOfEvents = str(args.Events);

    if(args.Window):
        parameters.XTolerance    = str(args.Window);
        parameters.YTolerance    = str(args.Window);
        parameters.XToleranceDUT = str(args.Window);
        parameters.YToleranceDUT = str(args.Window);
	
    if(args.Fit):
        if(args.Fit != "Simple" and args.Fit != "Kalman"):
            print("fit parameter can only be Simple or Kalman, not " + args.Fit);
            exit();
        parameters.TrackFittingAlgorithm = args.Fit;

    if(args.Geometry):
        parameters.GeometryFile = args.Geometry;

    if(args.ChewieTemplate):
        parameters.ChewieExpressTemplateName = args.ChewieTemplate;

    if(not args.RunMonicelli): #The default must be True so only when we don't want to run it then we get in here
        parameters.RunMonicelli = args.RunMonicelli;

    if(not args.RunChewie): #The default must be True so only when we don't want to run it then we get in here
        parameters.RunChewie = args.RunChewie;
    
    if(args.ChewieConfiguration):
        parameters.ChewieConfigurationFileName = args.ChewieConfiguration;

    if(args.Delete):
        cmd = "rm " + parameters.MonicelliExpressTemplateFilePath + parameters.MonicelliExpressTemplateFileName + " " + parameters.ChewieExpressTemplateFilePath + parameters.ChewieExpressTemplateFileName;
        print("Running command: " + cmd);
        cleanProcess = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
        out,err = cleanProcess.communicate();

    #Done parsing arguments
    exportVars(parameters);

    destinationHost, destinationDir = splitHostDir(os.environ['Monicelli_RawData_Dir']);
    #print(destinationHost)
    #print(destinationDir)

#    CopyScriptName		          = BaseDataDir + "megaCopy.csh";
#    MergerName	                  = "/home/otsdaq/Merger/Merger";
#    MonicelliOutputConverterName = "/home/otsdaq/Merger/Merger";
    if(args.CopyRawFiles):
        for run in runList:
            cmd = parameters.CopyScriptName + " " + run;
            print("Running command: " + cmd);
            proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
            out,error = proc.communicate();
            if(error != ''):
                print(error);
                subject = "FATAL: Raw copy error!"
                sendEmail(parameters.MailList, subject, error);
            print(out);
            print("Done Copying......");
    
    if(args.MergeRawFiles):
    	for run in runList:
            cmd = parameters.MergerName + " " + run;
            print("Running command: " + cmd);

            proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
            out,error = proc.communicate();
            if((error != '') and ('TCanvas::Print' not in error)):
                print(error);
                subject = "FATAL: Merge error!"
                sendEmail(parameters.MailList, subject, error);
            if('file list is empty' in out):
                subject = "FATAL: Run " + run + " was not copied!"
                sendEmail(parameters.MailList, subject, out);
            print(out);
            print("Done Merging......");
#    exit();

    destinationList = makeFileList(destinationHost, destinationDir);

    fileList = [];
    #print(runList);
    for run in runList:
        for tdc in tdcList:
            fileList += makeFileListToProcess(parameters, int(run), tdc);

    #print("Files to be processed:");
    #print(fileList);


    tmpFileList = fileList;

    print("List of files to process:");
    print("Destination list:\n");
    #print(destinationList);
    for file in tmpFileList:
        if (file not in destinationList):
            fileList.remove(file);
            print("Removing file: " + file)
            print("There is no file named: " + file + " ...Aborting");
            exit(0);
            
        #else:
        #    print(file);

    print("Number of files to process: " + str(len(fileList)));
    
    if(len(fileList) == 0):
        print("There are no files in that range. Are you sure you copied them with the right permissions?");
        sys.exit();
    
    print(fileList);


    ################################################################################################################
    #If Monicelli Express template does not exist then create it...
    monicelliTemplateFile = parameters.MonicelliExpressTemplateFilePath + parameters.MonicelliExpressTemplateFileName;
    if not os.path.isfile(monicelliTemplateFile):
        print("WARNING: I couldn't find the MonicelliExpress template file " + monicelliTemplateFile + ", so I am creating one using the template at the top of the megascript!");
        makeMonicelliExpressTemplate(monicelliTemplateFile, parameters);
    print(monicelliTemplateFile);
    
    ################################################################################################################
    #If Chewie Express template does not exist then create it...
    chewieTemplateFile = parameters.ChewieExpressTemplateFilePath + parameters.ChewieExpressTemplateFileName;
    if not os.path.isfile(chewieTemplateFile):
        print("WARNING: I couldn't find the ChewieExpress template file " + chewieTemplateFile + ", so I am creating one using the template at the top of the megascript!");
        makeChewieExpressTemplate(chewieTemplateFile, parameters);
    print(chewieTemplateFile);

    ################################################################################################################
    #If Chewie Configuration template does not exist then create it...
    chewieConfigurationFile = parameters.ChewieConfigurationFilePath + parameters.ChewieConfigurationFileName;
    if not os.path.isfile(chewieConfigurationFile):
        print("WARNING: I couldn't find the Chewie Configuration file " + chewieConfigurationFile + ", so I am creating one using the template at the top of the megascript!");
        cmd = "cp " + parameters.ChewieConfigurationTemplateFileName + " " + chewieConfigurationFile;
        print("Running command: " + cmd);
        copyProcess = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
        out,err = copyProcess.communicate();
        

    maxThreads = parameters.MaxThreads;
    if (maxThreads > len(fileList)):
        maxThreads = len(fileList);
    
    firstState = 0;
    if(not parameters.RunMonicelli):
        firstState = 2;
    
    monicelliEndState = 2;
    chewieEndState    = 4;

    lastState = chewieEndState;#run Monicelli and Chewie
    if(not parameters.RunChewie):
        monicelliEndState = 2;#Only Monicelli

    processes        = [None]       * len(fileList);
    output           = [""]         * len(fileList);
    error            = [""]         * len(fileList);
    status           = [firstState] * len(fileList);#0=ready, 1=Running Monicelli, 2=Done running Monicelli, 3=Running Chewie, 4=Done running Chewie
    nOfIncorrectData = [0]          * len(fileList);
    runName          = [""]         * len(fileList);
    outFiles         = [None]       * len(fileList);
    
    done = False;
    runningProcesses = 0;
    
    while(not done):
        for p in range (0, len(processes)):
            #print("Process: " +str(p) + " status: " + str(status[p]));
            runName[p] = fileList[p].replace(".dat","");
            if(status[p] == 0 and runningProcesses < maxThreads):
                status[p] = 1;
                ################################################################################################################
                #Run Monicelli
                ################################################################################################################
                print("Running Monicelli for " + runName[p]);    
                #Create Express xml files from template
                templateOutName = parameters.MonicelliExpressTemplateFileName.replace("template", runName[p]);

                monicelliExpressTemplateReplace(monicelliTemplateFile, parameters.MonicelliExpressTemplateFilePath + templateOutName, [fileList[p]], parameters.GeometryFile);
    
                #Run Monicelli express
                cmd = "cd " + parameters.MonicelliDir + "Express; ./MonicelliExpress " + templateOutName;
                print("Running command: " + cmd);
                outFiles[p] = open("Monicelli_stdout_" + fileList[p], 'w');
                processes[p] = subprocess.Popen(cmd, shell=True, stdout=outFiles[p], stderr=subprocess.PIPE, encoding='utf8');
                runningProcesses += 1;
	    
            elif(status[p] == 1):
                #print("Checking Monicelli process: " + str(p));
                if not (processes[p].poll() is None):
                    outFiles[p].close();
                    #print("Communicating...");
                    nada,error = processes[p].communicate();#The thread waits here and only when Monicelli is done it continues.
                    status[p] = 2;
                    outFiles[p] = open("Monicelli_stdout_" + fileList[p],'r');
                    out = "";
                    for line in iter(outFiles[p]):
                        out += line;
                    outFiles[p].close();
                    print(out);
                    
                    cmd = "rm Monicelli_stdout_" + fileList[p];
                    subprocess.Popen(cmd, shell=True);#Removing the stdout file because the process is done
                
                    if((error != '') and (('Error in <TCling' not in error) and ('FIXME.root has no keys' not in error))):
                        print(error);
                        error = "FATAL: There was an error executing Monicelli!";
                        print(error);
                        sendEmail(parameters.MailList, runName[p], error);
    
                    for line in out.split('\n'):
                        #print(line);
                        if(line.find("Incorrect data at block") != -1):
                            nOfIncorrectData[p] += 1;	    
 
                    runningProcesses -= 1;
                    print("Number of process running " + str(runningProcesses));

            done = True;
            for p in range (0, len(processes)):
                if(status[p] != monicelliEndState):
                    done = False;
                    break;
            if(not done):
                time.sleep(1)
		    
    done = False;
    runningProcesses = 0;
    while(not done):
        for p in range (0, len(processes), len(tdcList)):
            #print("Process: " +str(p) + " status: " + str(status[p]));
            if(len(tdcList) == 1):
                runName[p] = fileList[p].replace(".dat","");
            else:
                runName[p] = fileList[p].replace("tdc_"+ str(tdcList[0]) +".dat","tdc_"+ str(tdcList[0]) + "-" + str(tdcList[-1]));
                
            if(parameters.RunChewie and status[p] == 2 and runningProcesses < maxThreads):
                status[p] = 3;

                ################################################################################################################
                #Run Chewie
                ################################################################################################################
                print("Running Chewie for " + runName[p]);
                #Create Express xml files from template
                templateOutName = parameters.ChewieExpressTemplateFileName.replace("template",runName[p]);
                print(fileList);
                chewieFileList = [];
                for f in range (p,p+len(tdcList)):
                    chewieFileList.append(fileList[f])

                chewieExpressTemplateReplace(parameters, chewieTemplateFile, parameters.ChewieExpressTemplateFilePath + templateOutName, chewieFileList);

                #Run Chewie express
                cmd = "cd " + parameters.ChewieDir + "Express; ./ChewieExpress " + templateOutName;
                print("Running command: " + cmd);
                outFiles[p] = open("Chewie_stdout_" + fileList[p], 'w');
                processes[p] = subprocess.Popen(cmd, shell=True, stdout=outFiles[p], stderr=subprocess.PIPE, encoding='utf8');
                runningProcesses += 1;
	    
            elif(status[p] == 3):
                #print("Checking Chewie process: " + str(p));
                if not (processes[p].poll() is None):
                    outFiles[p].close();
                    nada,error = processes[p].communicate();
                    outFiles[p] = open("Chewie_stdout_" + fileList[p],'r');
                    out = "";
                    for line in iter(outFiles[p]):
                        out += line;
                    outFiles[p].close();
                    print(out);
                    #print(error);
                    
                    cmd = "rm Chewie_stdout_" + fileList[p];
                    subprocess.Popen(cmd, shell=True);
                    status[p] = 4;
                    runningProcesses -= 1;
                    print("Number of process running " + str(runningProcesses));
                    if(error != ''):
                        realError = '';
                        for line in error.split('\n'):
                            #print ">-----------------------";
                            #print "---" + line + "---";
                            #print "<-----------------------";
                            #ignore a line like this!
                            if(line.find("Warning") and line.find("no dictionary")):
                                continue;
                            if((line.find("Warning") == -1 and (error.find("Info") == -1 or error.find("Fit") or error.find("no dictionary")))):
                                print(line);
                                realError += line + '\n';
                        if(realError != '') and ("created default TCanvas with name" not in realError):
                            realError = "FATAL: There was an error executing Chewie! " + realError;
                            sendEmail(parameters.MailList, runName[p], realError);
                    print(out);
                    print(error);
                    
                    emailBody = "";
                    for line in out.split('\n'):
                        if(line.find("Detector") != -1):
                            line = line[line.find("Detector"):len(line)-3];
                            for char in line:
                                if(is_ascii(char)):
                                    emailBody += char;
                            emailBody += '\n';
                        if(line.find("Converting Run") != -1 and line.find("done") == -1):
                            emailBody += "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
                            line = line[line.find("Run"):len(line)-9];
                            for char in line:
                                if(is_ascii(char)):
                                    emailBody += char;
                            emailBody += '\n\n';
                            emailBody += "There are " + str(nOfIncorrectData[p]) + " incorrect data while decoding the Merged file.\n\n";
                    emailBody += "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
                    print(emailBody);
                    sendEmail(parameters.MailList, runName[p], emailBody);

                #sendEmail(parameters.MailList, "Megascript Done!", "");

            done = True;
            for p in range (0, len(processes), len(tdcList)):
                #print("Process: " + str(p) + " is done? " + str(status[p]))
                if(status[p] != chewieEndState):
                    done = False;
                    break;
            if(not done):
                time.sleep(1)


############################################################################################
if __name__ == "__main__":
    main()



