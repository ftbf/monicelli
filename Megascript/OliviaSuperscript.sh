#!/bin/bash

#Use this one when files have not been copied and merged
#python ./TDCMegascript.py -d -g PS_78481.xml --CopyRawFiles true --MergeRawFiles true -r 78480 -t6
#python ./TDCMegascript.py -d -g PS_78479.xml --CopyRawFiles true --MergeRawFiles true -r 78477-78478 -t6
#python ./TDCMegascript.py -d -g PS_78474.xml --CopyRawFiles true --MergeRawFiles true -r 78475-78476 -t6
#python ./TDCMegascript.py -d -g PS_78473.xml --CopyRawFiles true --MergeRawFiles true -r 78471-78472 -t6
#python ./TDCMegascript.py -d -g PS_78469.xml --CopyRawFiles true --MergeRawFiles true -r 78469-78470 -t6
#python ./TDCMegascript.py -d -g PS_78468.xml --CopyRawFiles true --MergeRawFiles true -r 78468 -t6
#python ./TDCMegascript.py -d -g PS_78463.xml --CopyRawFiles true --MergeRawFiles true -r 78464 -t6
#python ./TDCMegascript.py -d -g PS_78462.xml --CopyRawFiles true --MergeRawFiles true -r 78459,78461 -t6
#python ./TDCMegascript.py -d -g PS_78455.xml --CopyRawFiles true --MergeRawFiles true -r 78456-78458 -t6
#python ./TDCMegascript.py -d -g PS_78454.xml --CopyRawFiles true --MergeRawFiles true -r 78451-78453 -t6
#python ./TDCMegascript.py -d -g PS_78447.xml --CopyRawFiles true --MergeRawFiles true -r 78448-78450 -t6
#python ./TDCMegascript.py -d -g PS_78446.xml --CopyRawFiles true --MergeRawFiles true -r 78442-78445 -t6
#python ./TDCMegascript.py -d -g PS_78438.xml --CopyRawFiles true --MergeRawFiles true -r 78439-78441 -t6
#python ./TDCMegascript.py -d -g PS_78437.xml --CopyRawFiles true --MergeRawFiles true -r 78434-78436 -t6
#python ./TDCMegascript.py -d -g PS_78430.xml --CopyRawFiles true --MergeRawFiles true -r 78431-78433 -t6
#python ./TDCMegascript.py -d -g PS_78428.xml --CopyRawFiles true --MergeRawFiles true -r 78425-78427 -t6
#python ./TDCMegascript.py -d -g PS_78422.xml --CopyRawFiles true --MergeRawFiles true -r 78422-78424 -t6
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_1405.xml --CopyRawFiles true --MergeRawFiles true -r 78414 -t6

#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_2024.xml --CopyRawFiles true --MergeRawFiles true -r 78546-78553,78562 -t6

#Threshold scan Not Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml -r 78371-78401 -t6

#Angle scan Not Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_1405.xml -r 78412-78420 -t6
#python ./TDCMegascript.py -d -g PS_78422.xml -r 78421-78424 -t6
#python ./TDCMegascript.py -d -g PS_78428.xml -r 78425-78429 -t6
#python ./TDCMegascript.py -d -g PS_78430.xml -r 78430-78433 -t6
#python ./TDCMegascript.py -d -g PS_78437.xml -r 78434-78437 -t6
#python ./TDCMegascript.py -d -g PS_78438.xml -r 78438-78441 -t6
#python ./TDCMegascript.py -d -g PS_78446.xml -r 78442-78446 -t6
#python ./TDCMegascript.py -d -g PS_78447.xml -r 78447-78450 -t6
#python ./TDCMegascript.py -d -g PS_78454.xml -r 78451-78454 -t6
#python ./TDCMegascript.py -d -g PS_78455.xml -r 78455-78458 -t6
#python ./TDCMegascript.py -d -g PS_78462.xml -r 78459-78462 -t6
#python ./TDCMegascript.py -d -g PS_78463.xml -r 78463-78466 -t6
#python ./TDCMegascript.py -d -g PS_78468.xml -r 78467-78468 -t6
#python ./TDCMegascript.py -d -g PS_78469.xml -r 78469-78470 -t6
#python ./TDCMegascript.py -d -g PS_78473.xml -r 78471-78473 -t6
#python ./TDCMegascript.py -d -g PS_78474.xml -r 78474-78476 -t6
#python ./TDCMegascript.py -d -g PS_78479.xml -r 78477-78479 -t6
#python ./TDCMegascript.py -d -g PS_78481.xml -r 78480-78482 -t6
#python ./TDCMegascript.py -d -g PS_78485.xml -r 78483-78485 -t6
#python ./TDCMegascript.py -d -g PS_78463.xml -r 78465 -t6

#Angle Scan Irradiated
#python ./TDCMegascript.py -d -g Run78579_2023_07_25.xml -r 78579-78581 -t6
#python ./TDCMegascript.py -d -g Run78582_2023_07_25.xml -r 78582-78584 -t6
#python ./TDCMegascript.py -d -g Run78585_2023_07_25.xml -r 78585-78587 -t6
#python ./TDCMegascript.py -d -g Run78588_2023_07_25.xml -r 78588-78590 -t6
#python ./TDCMegascript.py -d -g Run78591_2023_07_25.xml -r 78591-78593 -t6
#python ./TDCMegascript.py -d -g Run78594_2023_07_25.xml -r 78594,78596,78597 -t6
#python ./TDCMegascript.py -d -g Run78598_2023_07_25.xml -r 78598-78601 -t6
#python ./TDCMegascript.py -d -g Run78603_2023_07_25.xml -r 78603-78606 -t6
#python ./TDCMegascript.py -d -g Run78607_2023_07_25.xml -r 78607-78610 -t6
#python ./TDCMegascript.py -d -g Run78611_2023_07_25.xml -r 78611-78614 -t6
#python ./TDCMegascript.py -d -g Run78615_2023_07_25.xml -r 78615-78618 -t6
#python ./TDCMegascript.py -d -g Run78619_2023_07_25.xml -r 78619-78622 -t6
#python ./TDCMegascript.py -d -g Run78623_2023_07_25.xml -r 78623-78626 -t6
#python ./TDCMegascript.py -d -g Run78627_2023_07_25.xml -r 78627-78630 -t6
#python ./TDCMegascript.py -d -g Run78631_2023_07_25.xml -r 78631,78632,78634,78655 -t6
#python ./TDCMegascript.py -d -g Run78636_2023_07_25.xml -r 78636-78637 -t6
#python ./TDCMegascript.py -d -g Run78643_2023_07_25.xml -r 78643-78645 -t6
#python ./TDCMegascript.py -d -g Run78643_2023_07_25.xml -r 78646-78659 -t6
#python ./TDCMegascript.py -d -g Run78643_2023_07_25.xml -r 78698-78702 -t6
#python ./TDCMegascript.py -d -g Run78643_2023_07_25.xml -r 78746-78846 -t6

#python ./TDCMegascript.py -d -g Run78643_2023_07_25.xml -r 78659 -t6
#python ./TDCMegascript.py -d -g Run78643_2023_07_25.xml --CopyRawFiles true --MergeRawFiles true -r 78646-78659 -t6

#python ./TDCMegascript.py -d -g Run78746_2023_08_14.xml -r 78746 -t6
#python ./TDCMegascript.py -d -g Run78747_2023_08_14.xml -r 78747-78756 -t6

python ./TDCMegascript.py -d -g Run78638_2023_08_14.xml -r 78638,78639 -t6
python ./TDCMegascript.py -d -g Run78641_2023_08_14.xml -r 78640-78642 -t6










#python ./TDCMegascript.py -d -g Run78615_2023_07_25.xml -r 78618 -t6 #6
#python ./TDCMegascript.py -d -g Run78606_2023_07_25.xml -r 78603 -t6 #9

