#!/bin/bash
#Threshold scan Not Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml -r 78371-78401 -t6

#High Voltage scan Not Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_12_1824.xml -r 78402-78410 -t6

#High Voltage scan Irradiated
#python ./TDCMegascript.py -d -g 2023_06_PS_07_13_2024.xml -r 78519,78521,78523 -t6

#Angle scan Irradiated
#python ./TDCMegascript.py -d -g Run78579_2023_07_25.xml -r 78579-78581 -t6
#python ./TDCMegascript.py -d -g Run78582_2023_07_25.xml -r 78582-78584 -t6
#python ./TDCMegascript.py -d -g Run78585_2023_07_25.xml -r 78585-78587 -t6
#python ./TDCMegascript.py -d -g Run78588_2023_07_25.xml -r 78588-78590 -t6
#python ./TDCMegascript.py -d -g Run78591_2023_07_25.xml -r 78591-78593 -t6
#python ./TDCMegascript.py -d -g Run78594_2023_07_25.xml -r 78594,78596, 78597 -t6
#python ./TDCMegascript.py -d -g Run78598_2023_07_25.xml -r 78598-78601 -t6
#python ./TDCMegascript.py -d -g Run78606_2023_07_25.xml -r 78603-78606 -t6
#python ./TDCMegascript.py -d -g Run78607_2023_07_25.xml -r 78607-78610 -t6
#python ./TDCMegascript.py -d -g Run78611_2023_07_25.xml -r 78611-78614 -t6
#python ./TDCMegascript.py -d -g Run78615_2023_07_25.xml -r 78615-78618 -t6
#python ./TDCMegascript.py -d -g Run78619_2023_07_25.xml -r 78619-78622 -t6
#python ./TDCMegascript.py -d -g Run78623_2023_07_25.xml -r 78623-78626 -t6
#python ./TDCMegascript.py -d -g Run78627_2023_07_25.xml -r 78627-78630 -t6
#python ./TDCMegascript.py -d -g Run78631_2023_07_25.xml -r 78631,78632,78634,78635 -t6
#python ./TDCMegascript.py -d -g Run78636_2023_07_26.xml -r 78636-78637 -t6
#python ./TDCMegascript.py -d -g Run78643_2023_07_26.xml -r 78643-78647,78649-78659,78698-78702,78746-78846 -t6


## room temp run: 
##Weird RUNS to check 71,72
# ANGLE = 0
#./nabinMegascript.py -d -g Run39_Merged.geo -r 24,25,27-54,56-64,70#GOOD
