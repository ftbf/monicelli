#!/bin/bash


#TDC Studies
RunNumbers=(
	78558
)

COUNTER=0
for t in 6
do
    for r in "${RunNumbers[@]}"
    do
        echo "============= Run number $r ====================="
        let COUNTER++
        ./TDCMegascript.py --CopyRawFiles false --MergeRawFiles false --RunMonicelli true -g 2023_06_PS_07_13_2024.xml -d -t "$t" -r "$r" &
        sleep 1
        if ((COUNTER % 10 == 0 )); then
            wait
        fi
    done
done
