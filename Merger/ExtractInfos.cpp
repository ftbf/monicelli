// g++ -o ExtractInfos ExtractInfos.cpp -I${BOOSTINC} -L${BOOSTLIB} -lboost_regex -lboost_system -lboost_filesystem

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <map>
#include <sstream>
#include <stdint.h>
#include <vector>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

using namespace std ;
using boost::lexical_cast;
  
int main()
{ 	
   ifstream file("inefficientTracks_AllPlanes_New.txt");
   int triggerNumber,station,row,col;
   map<int, map< int, vector<pair<int,int> > > > hitMap; //Map of trigger number and hit station
   string line;
   stringstream ss;
   ofstream inefficientTracks, inefficientTracks_DUT_Hits;
   inefficientTracks.open("inefficientTracks_Info_New.txt", std::ios_base::app);
   inefficientTracks_DUT_Hits.open("inefficientTracks_DUT_Hits_Info_New.txt", std::ios_base::app);
   boost::smatch match;
   boost::regex exp ("\\s+Trigger\\s+Number\\:\\s+(\\d+)\\s+detId\\:\\s+\\d+\\s+station\\:\\s+(\\d+)\\s+plaq\\:\\s+\\d+\\s+cid\\:\\s+\\d+\\s+row\\:\\s+(\\d+)\\s+col\\:\\s+(\\d+)\\s+");

   while(getline(file,line))
   {
   	//cout<<__LINE__<<line<<endl;	
	regex_search (line,match,exp);
	triggerNumber = boost::lexical_cast<int>(match[1]);		
        station       = boost::lexical_cast<int>(match[2]);
	row           = boost::lexical_cast<int>(match[3]);
	col           = boost::lexical_cast<int>(match[4]);	
	//cout<<__LINE__<<" "<<match[1]<<" "<<match[2]<<" "<<match[3]<<" "<<match[4]<<endl;		
	hitMap[triggerNumber][station].push_back(make_pair(row,col));
	match.str("");
   }

   for (map<int,map<int, vector<pair<int,int> > > >::iterator it=hitMap.begin(); it!=hitMap.end(); ++it)
   {
	
     ss<<"Trigger number: "<<it->first;  
     if(it->second.find(0)!=it->second.end()) ss<<" Hits on pixel telescope downstream: yes";
     if(it->second.find(2)!=it->second.end()) ss<<" Hits on pixel telescope upstream: yes";
     if((it->second.find(5)!=it->second.end())||(it->second.find(6)!=it->second.end())) ss<<" Hits on strip telescope upstream: yes";
     if(it->second.find(7)!=it->second.end()) ss<<" Hits on strip telescope downstream: yes";
     map<int, vector<pair<int,int> > >::iterator found;
     found = it->second.find(4);
     if(found!=it->second.end())
     {
	 ss<<" Hits on DUT: yes";
	 for(vector<pair<int,int> > ::iterator jt = found->second.begin(); jt!= found->second.end(); ++jt)
         { 
				ss<<" row: "<<(*jt).first<<" col: "<<(*jt).second;
         }

         inefficientTracks_DUT_Hits<<ss.str()<<std::endl;							 	  
         ss.str("");
	 continue;		
	     	
     }

     inefficientTracks<<ss.str()<<std::endl;							 	  
     ss.str("");												 	  
	 	
   }	

   file.close();
   inefficientTracks.close();
   inefficientTracks_DUT_Hits.close();  
	
   return 0;

}
