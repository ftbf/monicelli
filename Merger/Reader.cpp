#include "FilesMerger.h"
#include <map>
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

const bool writeOutFile = true;

const string dutName        = "dut";
const string digitalName    = "digital";
const string telescope1Name = "downstream";
const string telescope2Name = "upstream";
const string fed0Name       = "FER0";
const string fed1Name       = "FER1";
const string fed2Name       = "FER2";
const string fed3Name       = "FER3";
const string OTEastName     = "fed000";
const string OTWestName     = "fed001";


//map<string,string> mergeStationFiles(multimap<string,string> files);



int main(int argc, char **argv)
{
  if(getenv("MERGER_INPUT_DIR") == 0 || getenv("MERGER_OUTPUT_DIR") == 0)
  {
     cout << __PRETTY_FUNCTION__ << "You need to source setupIrene.sh" << endl;
     exit(EXIT_FAILURE);
  }
  const string inputFilesDir  = getenv("MERGER_INPUT_DIR");
  const string filesDirectory = getenv("MERGER_OUTPUT_DIR");
  string runNumber = "451";
  FilesMerger theFilesMerger;
  theFilesMerger.readMergedFiles(filesDirectory+"Run"+runNumber+"_Merged.dat");
//  theFilesMerger.writeMergedFiles("Run"+runNumber+"_Read.dat");
  return EXIT_SUCCESS;
  vector<string> stationNames;
  stationNames.push_back(telescope1Name);
  stationNames.push_back(telescope2Name);
  stationNames.push_back(digitalName);//THIS IS THE DUT!
// stationNames.push_back(dutName);//DO NOT USE
  stationNames.push_back(fed0Name);
  stationNames.push_back(fed1Name);
  stationNames.push_back(fed2Name);
  stationNames.push_back(OTEastName);
//  stationNames.push_back(OTWestName);
  theFilesMerger.setStationNames(stationNames);
  theFilesMerger.makeFilesList(runNumber,filesDirectory);
  theFilesMerger.makeStationBuffers();
  theFilesMerger.readStationBuffers();
  return 0;
}
