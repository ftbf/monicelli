#include "FilesMerger.h"
#include <map>
#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <stdint.h>
#include "Run.h"
#include "DataDecoder.h"
#include "TH2F.h"

using namespace std;

//const bool writeOutFile = true;  

//const string filesDirectory = "../../2012_04_April/UTCA/";
const string fileDirectory = "../MergerIreneOutput/";
const string dutName        = "dut";
const string telescope1Name = "downstream";
const string telescope2Name = "upstream";
const string digitalName    = "digital";
const string fed0Name       = "FED0";
const string fed1Name       = "FED1";
const string fed2Name       = "FED2";


//map<string,string> mergeStationFiles(multimap<string,string> files);



int main(int argc, char **argv)
{  

 if(getenv("MERGER_INPUT_DIR") == 0)
  {
     cout << __PRETTY_FUNCTION__ << "You need to source setupIrene.sh" << endl;
     exit(EXIT_FAILURE);
  }
  
  const string inputFilesDir  = getenv("MERGER_OUTPUT_DIR");
  string fileName = "";
  string runNumber;

  if(argc != 2 )
  {
     cout << __PRETTY_FUNCTION__ << "\tUsage: ./ReaderIrene runNumber " << endl;
     exit(EXIT_FAILURE);
  }
  else if( argc == 2 )
     runNumber = argv[1];  

  cout <<   runNumber << endl;
    
  if(fileName == "")
   fileName = "Run"+runNumber+"_Merged_OnlyStrips.dat";

   string fileName2 = "Run"+runNumber+"_Merged_OnlyStrips_Ineff_DUT.dat";

  FilesMerger theFilesMerger;
  vector<string> stationNames;
  stationNames.push_back(telescope1Name);
  stationNames.push_back(telescope2Name);
  stationNames.push_back(digitalName);
  //stationNames.push_back(dutName);
  stationNames.push_back(fed0Name);
  stationNames.push_back(fed1Name);
  stationNames.push_back(fed2Name);
  theFilesMerger.setStationNames(stationNames);

  string ineffFile("Inefficient_TriggerNumber_OnlyStrip_DUT.txt");
//  string ineffFile("Inefficient_TriggerNumber_DUT0_OnlyStrips_3points.txt");
  string ineffFile2("Inefficient_TriggerNumber_DUT0_OnlyPixels.txt");

  ifstream file(ineffFile.c_str());
  ifstream file2(ineffFile2.c_str());

  cout << "Using file: " << ineffFile.c_str() << endl;
  short planeID;
  uint32_t triggerNumber;
  uint32_t triggerPx;
//  string line;
  int counterIneffStrips = 0;
  map<uint32_t, map<short, bool> > triggerMap;
  map<uint32_t, map<short, bool> > triggerPxMap;

/*  while(!file.eof())
    {
      getline (file, line);
      file >> triggerNumber >> planeID;
      triggerMap[triggerNumber][planeID] = true;
    }
*/

      while(file >> triggerNumber >> planeID)
	{
          counterIneffStrips++;
          triggerMap[triggerNumber][planeID] = true;
          cout << " evento ineff strips n " << counterIneffStrips << " trigger " << triggerNumber << endl;
        }
      while(file2 >> triggerPx >> planeID)
          triggerPxMap[triggerPx][planeID] = true;
      
      

 cout << "Closing file: " << ineffFile.c_str() << endl;
  file.close();
  file2.close();
  cout << " file closed " << endl;


   cout << " File: " << fileName << "In directory: " << inputFilesDir << endl;

  fileName = inputFilesDir + fileName;

  int maxNumberOfEvents = 65500; //Irene, before it was 30
  cout << __PRETTY_FUNCTION__ << "\tReading file: " << fileName << endl;
  //Reading the file
  // 8 Bytes: all 1's
  // 8 Bytes: time stamp in computer seconds
  // All hits in event
  // the high byte reserved bits (63:56) indicate station number
  // the low 7 bytes of hit quad word are saved (55:0)
  //Quad Word: 
  //reserved(63:52) | trigger number(51:32) | plaquette(31:29) | data0/debug1(28) | chipid(27:23) | row(22:16) | col(15:10) | pulseheight(9:0)
  bool writeOutFile = true; 
  ofstream outputFile;
  ofstream outputIneffFile;
  if(writeOutFile)
    {
      string outFileName  = fileName;
      string InefFileName = fileName2;
      outputFile.open(outFileName.replace(outFileName.find(".dat"),4,".txt").c_str());
      outputIneffFile.open(InefFileName.replace(InefFileName.find(".dat"),4,".txt").c_str());
    }

  bool timeStamp = false;
  unsigned long long ones = -1;
  unsigned long long data;
  unsigned int       uniqueTrigger = 0;
  int ineffDUT = 0;
  Run theRun;
  theRun.reset();


  ifstream inFile(fileName.c_str(),ios::binary);
  if(!inFile.is_open())
    {
      cout << "Can't open file: " << fileName << endl;
      return -1;
    }
  Event anEvent;
  DataDecoder theDataDecoder;

  while(!inFile.eof())
    {
      inFile.read((char*)(&data),sizeof(unsigned long long));
//      cout << "Data: " << (data>>32) << (data & 0xffffffff) << endl;
      if(data == ones)
	{
	  outputFile << "fffffffffffffffffffffffffffffffffffff" << endl;
//	  outputIneffFile << "fffffffffffffffffffffffffffffffffffff" << endl;
	  timeStamp = true;
	  continue;
	}
      if(timeStamp || inFile.eof())
	{
//	  ++uniqueTrigger;
	  theDataDecoder.setTimestamp(data);
	  anEvent.setTimestamp(theDataDecoder.getTimestamp());
	  outputFile << dec << "Time: " << (data>>32) << (data & 0xffffffff) << endl;
//	  outputIneffFile << dec << "Time: " << (data>>32) << (data & 0xffffffff) << endl;
	  timeStamp = false;
	  if(anEvent.getNumberOfHits() != 0)
	    {
/*	              cout << __PRETTY_FUNCTION__ << "New event " << uniqueTrigger 
                                                  << "hardware trigger " << anEvent.getHardwareTriggerNumber() 
                                                  << " nHits =  " << anEvent.getNumberOfHits() << endl;
	      
              if(uniqueTrigger != anEvent.getHardwareTriggerNumber()) cout << __PRETTY_FUNCTION__ << " DIFFERENT TRIGGERS! " << endl;*/
              theRun.addEvent(anEvent);
	      anEvent.reset();
	      if(theRun.getNumberOfEvents() >= (unsigned int)maxNumberOfEvents)
		break;
	    }
	}
      else
	{
	  theDataDecoder.setData(data);
	  if( theDataDecoder.isData() )
	    {
	      anEvent.setTriggerNumber(theDataDecoder.decodeTrigger());
//                   cout << __PRETTY_FUNCTION__ << "Before" << endl;
//	           cout << __PRETTY_FUNCTION__ << "Trigger number: " << theDataDecoder.decodeTrigger() << " unique trigger " << uniqueTrigger << "anEvent.getTriggerNumber" << anEvent.getTriggerNumber() << endl;
//	      anEvent.setTriggerNumber(uniqueTrigger);
//	      anEvent.setHardwareTriggerNumber(theDataDecoder.decodeTrigger());
//                   cout << __PRETTY_FUNCTION__ << "After" << endl;
//	           cout << __PRETTY_FUNCTION__ << "Trigger number: " << theDataDecoder.decodeTrigger() << " unique trigger " << uniqueTrigger << "anEvent.getTriggerNumber" << anEvent.getTriggerNumber() << endl;
//	           cout << __PRETTY_FUNCTION__ << "Trigger number: " << theDataDecoder.decodeTrigger() << endl;
//	           cout << __PRETTY_FUNCTION__ 
//	                 << "data " << setw(8) << setfill('0') << hex
//	                 << ((data>>32) & 0xffffffff)
//	                 << setw(8) << setfill('0')
//	                 << (data & 0xffffffff) << dec
//	                << endl;
	      anEvent.addHit(theDataDecoder.decodeHit());
	      if(writeOutFile)
		{
		  Hit tmpHit = theDataDecoder.decodeHit();
		  outputFile << hex
			     << "data: "
			     << setw(8) << setfill('0')
			     << ((data>>32) & 0xffffffff)
			     << setw(8) << setfill('0')
			     << (data & 0xffffffff)
//			     << " trig: "      << anEvent.getHardwareTriggerNumber()
			     << " trig: "      << anEvent.getTriggerNumber()
                             << " bco: "       << tmpHit.bco
			     << " row: "       << tmpHit.row
			     << " col: "       << tmpHit.col
			     << " chip: "      << tmpHit.cid
			     << " plaq: "      << tmpHit.plaq
			     << " adc: "       << tmpHit.adc
			     << " station: "   << tmpHit.station
                             << endl;

//                  cout << " trigger number " << anEvent.getTriggerNumber() << endl;
                  if(triggerMap.find(anEvent.getTriggerNumber()) == triggerMap.end())
                   {
//                    cout << " not writing inef output file " << endl; 
                    continue;
                   }
                  else
                  { 
                   
                   if(uniqueTrigger != anEvent.getTriggerNumber())
                     {
                      ineffDUT++;
                      outputIneffFile << "ineff event for DUT number " << ineffDUT << endl; 
                     } 
		   outputIneffFile << hex
		    	           << "data: "
			           << setw(8) << setfill('0')
			           << ((data>>32) & 0xffffffff)
			           << setw(8) << setfill('0')
		      	           << (data & 0xffffffff)
//		      	                 << " trig: "      << anEvent.getHardwareTriggerNumber()
			           << dec << " trig: "      << anEvent.getTriggerNumber()
                                   << " bco: "       << tmpHit.bco
			           << " row: "       << tmpHit.row
			           << " col: "       << tmpHit.col
			           << " chip: "      << tmpHit.cid
			           << " plaq: "      << tmpHit.plaq
			           << " adc: "       << tmpHit.adc
			           << " station: "   << tmpHit.station
                                   << endl;

                 }
                   uniqueTrigger = anEvent.getTriggerNumber();
		}
	    }
	}
    }
//  inFile.close();


//Analysis on inefficient events
  TFile *outFile  = new TFile("InefficiencyPlotsAllPlanesPx.root","RECREATE");

  // ineff on all DUTs
  TH1F * hIneffDutHitOnOtherPlanes       = new TH1F("hIneffDutHitOnOtherPlanes","hIneffDutHitOnOtherPlanes", 100, 0., 100.);
  TH1F * hIneffDutHitOnOtherPlanesPixels = new TH1F("hIneffDutHitOnOtherPlanesPixels","hIneffDutHitOnOtherPlanesPixels", 100, 0., 100.);
  TH1F * hIneffDutHitOnStation0          = new TH1F("hIneffDutHitOnStation0","hIneffDutHitOnStation0", 100, 0., 100.);
  TH1F * hIneffDutHitOnStation2          = new TH1F("hIneffDutHitOnStation2","hIneffDutHitOnStation2", 100, 0., 100.);
  TH1F * hIneffDutHitOnStation5          = new TH1F("hIneffDutHitOnStation5","hIneffDutHitOnStation5", 100, 0., 100.);
  TH1F * hIneffDutHitOnStation6          = new TH1F("hIneffDutHitOnStation6","hIneffDutHitOnStation6", 100, 0., 100.);
  TH1F * hIneffDutHitOnStation7          = new TH1F("hIneffDutHitOnStation7","hIneffDutHitOnStation7", 100, 0., 100.);
  TH1F * hIneffDutHitOnStation0And2      = new TH1F("hIneffDutHitOnStation0And2","hIneffDutHitOnStation0And2", 100, 0., 100.);

  // ineff on DUT0 whitout pixel telescope
  TH1F * hIneffDut0HitOnOtherPlanesWOpixTel       = new TH1F("hIneffDut0HitOnOtherPlanesWOpixTel","hIneffDut0HitOnOtherPlanesWOpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation5WOpixTel          = new TH1F("hIneffDut0HitOnStation5WOpixTel","hIneffDut0HitOnStation5WOpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation6WOpixTel          = new TH1F("hIneffDut0HitOnStation6WOpixTel","hIneffDut0HitOnStation6WOpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation7WOpixTel          = new TH1F("hIneffDut0HitOnStation7WOpixTel","hIneffDut0HitOnStation7WOpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation4plaq1And2WOpixTel = new TH1F("hIneffDut0HitOnStation4plaq1And2WOpixTel","hIneffDut0HitOnStation4plaq1And2WOpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation4plaq1WOpixTel     = new TH1F("hIneffDut0HitOnStation4plaq1WOpixTel","hIneffDut0HitOnStation4plaq1WOpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation4plaq2WOpixTel     = new TH1F("hIneffDut0HitOnStation4plaq2WOpixTel","hIneffDut0HitOnStation4plaq2WOpixTel", 100, 0., 100.);

  // ineff on DUT0 whit pixel telescope
  TH1F * hIneffDut0HitOnOtherPlanesWpixTel       = new TH1F("hIneffDut0HitOnOtherPlanesWpixTel","hIneffDut0HitOnOtherPlanesWpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation0WpixTel          = new TH1F("hIneffDut0HitOnStation0WpixTel","hIneffDut0HitOnStation0WpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation2WpixTel          = new TH1F("hIneffDut0HitOnStation2WpixTel","hIneffDut0HitOnStation2WpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation5WpixTel          = new TH1F("hIneffDut0HitOnStation5WpixTel","hIneffDut0HitOnStation5WpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation6WpixTel          = new TH1F("hIneffDut0HitOnStation6WpixTel","hIneffDut0HitOnStation6WpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation7WpixTel          = new TH1F("hIneffDut0HitOnStation7WpixTel","hIneffDut0HitOnStation7WpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation0And2WpixTel      = new TH1F("hIneffDut0HitOnStation0And2WpixTel","hIneffDut0HitOnStation0And2WpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation4plaq1And2WpixTel = new TH1F("hIneffDut0HitOnStation4plaq1And2WpixTel","hIneffDut0HitOnStation4plaq1And2WpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation4plaq1WpixTel     = new TH1F("hIneffDut0HitOnStation4plaq1WpixTel","hIneffDut0HitOnStation4plaq1WpixTel", 100, 0., 100.);
  TH1F * hIneffDut0HitOnStation4plaq2WpixTel     = new TH1F("hIneffDut0HitOnStation4plaq2WpixTel","hIneffDut0HitOnStation4plaq2WpixTel", 100, 0., 100.);

  // eff on DUT0 Whitout pixel telescope
  TH1F * hEffDut0HitOnOtherPlanesWOpixTel       = new TH1F("hEffDut0HitOnOtherPlaneWOpixTels","hEffDut0HitOnOtherPlanesWOpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation5WOpixTel          = new TH1F("hEffDut0HitOnStation5WOpixTel","hEffDut0HitOnStation5WOpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation6WOpixTel          = new TH1F("hEffDut0HitOnStation6WOpixTel","hEffDut0HitOnStation6WOpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation7WOpixTel          = new TH1F("hEffDut0HitOnStation7WOpixTel","hEffDut0HitOnStation7WOpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation4plaq1And2WOpixTel = new TH1F("hEffDut0HitOnStation4plaq1And2WOpixTel","hEffDut0HitOnStation4plaq1And2WOpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation4plaq1WOpixTel     = new TH1F("hEffDut0HitOnStation4plaq1WOpixTel","hEffDut0HitOnStation4plaq1WOpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation4plaq2WOpixTel     = new TH1F("hEffDut0HitOnStation4plaq2WOpixTel","hEffDut0HitOnStation4plaq2WOpixTel", 100, 0., 100.);

  // eff on DUT0 Whit pixel telescope
  TH1F * hEffDut0HitOnOtherPlanesWpixTel       = new TH1F("hEffDut0HitOnOtherPlanesWpixTel","hEffDut0HitOnOtherPlanesWpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation0WpixTel          = new TH1F("hEffDut0HitOnStation0WpixTel","hEffDut0HitOnStation0WpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation2WpixTel          = new TH1F("hEffDut0HitOnStation2WpixTel","hEffDut0HitOnStation2WpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation5WpixTel          = new TH1F("hEffDut0HitOnStation5WpixTel","hEffDut0HitOnStation5WpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation6WpixTel          = new TH1F("hEffDut0HitOnStation6WpixTel","hEffDut0HitOnStation6WpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation7WpixTel          = new TH1F("hEffDut0HitOnStation7WpixTel","hEffDut0HitOnStation7WpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation0And2WpixTel      = new TH1F("hEffDut0HitOnStation0And2WpixTel","hEffDut0HitOnStation0And2WpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation4plaq1And2WpixTel = new TH1F("hEffDut0HitOnStation4plaq1And2WpixTel","hEffDut0HitOnStation4plaq1And2WpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation4plaq1WpixTel     = new TH1F("hEffDut0HitOnStation4plaq1WpixTel","hEffDut0HitOnStation4plaq1WpixTel", 100, 0., 100.);
  TH1F * hEffDut0HitOnStation4plaq2WpixTel     = new TH1F("hEffDut0HitOnStation4plaq2WpixTel","hEffDut0HitOnStation4plaq2WpixTel", 100, 0., 100.);

   
  short station;
  short plaque;

  std::stringstream ss;
  std::ofstream outfile;
  outfile.open("Inefficient_TriggerNumber_OnlyStrip_DUT_New.txt", std::ios_base::app);
  int counterIneffPixels = 0;

 for(map<unsigned int, Event>::iterator eventsIt=theRun.getEvents().begin(); eventsIt!=theRun.getEvents().end(); eventsIt++)
     {
   
//    cout << "Event: " << eventsIt->second.getTriggerNumber() << endl;
    triggerNumber = eventsIt->second.getTriggerNumber();   
//    triggerNumber = eventsIt->second.getHardwareTriggerNumber();   
    triggerPx = eventsIt->second.getTriggerNumber();   

//    if(triggerMap.find(triggerNumber) == triggerMap.end()) 
//       continue;

    bool found = false;
    bool stationFound = false;
    bool pixelTelescope = false;
        
    int counterStation0      = 0;
    int counterStation2      = 0;
    int counterStation4plaq1 = 0;
    int counterStation4plaq2 = 0;
    int counterStation5      = 0;
    int counterStation6      = 0;
    int counterStation7      = 0;
    //    int triggerCounter       = 0;

   for(unsigned int hit=0; hit<eventsIt->second.getNumberOfHits(); hit++)
      {
   	station = eventsIt->second.getHit(hit).station;
        plaque    = eventsIt->second.getHit(hit).plaq;	
     
//       cout << "trigger number: " << triggerNumber << " hit: " << hit << " station: " << station << " plaq: " << plaque << endl;

       if(station == 4)
        stationFound = true;

        if(station == 4 && plaque == 1 ) counterStation4plaq1++;
        if(station == 4 && plaque == 2 ) counterStation4plaq2++;
      
       if(station == 0 || station == 2) pixelTelescope = true;

      
       if(station == 0) counterStation0++;
       if(station == 2) counterStation2++;
       if(station == 5) counterStation5++;
       if(station == 6) counterStation6++;  
       if(station == 7) counterStation7++;  
      }

    if(triggerMap.find(triggerNumber) == triggerMap.end())
      {
	if(pixelTelescope)
	  {
	   hEffDut0HitOnOtherPlanesWpixTel      ->Fill(eventsIt->second.getNumberOfHits());
	   hEffDut0HitOnStation0WpixTel         ->Fill(counterStation0);
	   hEffDut0HitOnStation2WpixTel         ->Fill(counterStation2);
	   hEffDut0HitOnStation5WpixTel         ->Fill(counterStation5);
	   hEffDut0HitOnStation6WpixTel         ->Fill(counterStation6);
	   hEffDut0HitOnStation7WpixTel         ->Fill(counterStation7);
	   hEffDut0HitOnStation0And2WpixTel     ->Fill(counterStation0+counterStation2);
	   hEffDut0HitOnStation4plaq1And2WpixTel->Fill(counterStation4plaq1+counterStation4plaq2);
	   hEffDut0HitOnStation4plaq1WpixTel    ->Fill(counterStation4plaq1);
	   hEffDut0HitOnStation4plaq2WpixTel    ->Fill(counterStation4plaq2);
          }
        else
          {
	   hEffDut0HitOnOtherPlanesWOpixTel      ->Fill(eventsIt->second.getNumberOfHits());
	   hEffDut0HitOnStation5WOpixTel         ->Fill(counterStation5);
	   hEffDut0HitOnStation6WOpixTel         ->Fill(counterStation6);
	   hEffDut0HitOnStation7WOpixTel         ->Fill(counterStation7);
	   hEffDut0HitOnStation4plaq1And2WOpixTel->Fill(counterStation4plaq1+counterStation4plaq2);
	   hEffDut0HitOnStation4plaq1WOpixTel    ->Fill(counterStation4plaq1);
	   hEffDut0HitOnStation4plaq2WOpixTel    ->Fill(counterStation4plaq2);
          } 
      }
    else
      {
	//	cout << " ineff trigger number " << triggerNumber << endl;
	//	if(pixelTelescope)
        if(triggerPxMap.find(triggerPx) == triggerPxMap.end())
          {
  	    hIneffDut0HitOnOtherPlanesWOpixTel      ->Fill(eventsIt->second.getNumberOfHits());
	    hIneffDut0HitOnStation5WOpixTel         ->Fill(counterStation5);
	    hIneffDut0HitOnStation6WOpixTel         ->Fill(counterStation6);
	    hIneffDut0HitOnStation7WOpixTel         ->Fill(counterStation7);
	    hIneffDut0HitOnStation4plaq1And2WOpixTel->Fill(counterStation4plaq1+counterStation4plaq2);
	    hIneffDut0HitOnStation4plaq1WOpixTel    ->Fill(counterStation4plaq1);
	    hIneffDut0HitOnStation4plaq2WOpixTel    ->Fill(counterStation4plaq2);
          }
        else
	  {
	    hIneffDut0HitOnOtherPlanesWpixTel      ->Fill(eventsIt->second.getNumberOfHits());
 	    hIneffDut0HitOnStation0WpixTel         ->Fill(counterStation0);
	    hIneffDut0HitOnStation2WpixTel         ->Fill(counterStation2);
	    hIneffDut0HitOnStation5WpixTel         ->Fill(counterStation5);
	    hIneffDut0HitOnStation6WpixTel         ->Fill(counterStation6);
	    hIneffDut0HitOnStation7WpixTel         ->Fill(counterStation7);
	    hIneffDut0HitOnStation0And2WpixTel     ->Fill(counterStation0+counterStation2);
	    hIneffDut0HitOnStation4plaq1And2WpixTel->Fill(counterStation4plaq1+counterStation4plaq2);
	    hIneffDut0HitOnStation4plaq1WpixTel    ->Fill(counterStation4plaq1);
	    hIneffDut0HitOnStation4plaq2WpixTel    ->Fill(counterStation4plaq2);
          }
        
         
      }  
  if(triggerMap.find(triggerNumber) == triggerMap.end())
    continue;
  else
 {
   if(!stationFound)
   {
//    cout << "Filling Histogram because stationFound = " << stationFound << endl;
    hIneffDutHitOnOtherPlanes->Fill(eventsIt->second.getNumberOfHits()); 
    hIneffDutHitOnStation0   ->Fill(counterStation0);
    hIneffDutHitOnStation2   ->Fill(counterStation2);
    hIneffDutHitOnStation5   ->Fill(counterStation5);
    hIneffDutHitOnStation6   ->Fill(counterStation6);
    hIneffDutHitOnStation7   ->Fill(counterStation7);
             ss.str("");
             ss << eventsIt->second.getTriggerNumber() << " " << 22;
             outfile << ss.str() <<endl;

   if(pixelTelescope) 
     {    
     hIneffDutHitOnOtherPlanesPixels->Fill(eventsIt->second.getNumberOfHits()); 
     hIneffDutHitOnStation0And2     ->Fill(counterStation0+counterStation2);
     counterIneffPixels++;
//             ss.str("");
//             ss << eventsIt->second.getTriggerNumber() << " " << 22;
//             outfile << ss.str() <<endl;
	//        cout << " evento ineff for pixel also  "  << counterIneffPixels << " trigger number "<< triggerNumber << std::endl;

     }
   }
  }
   /*   else
   {
//    cout << "Filling Histogram because stationFound = " << stationFound << endl;
    hIneffDut0HitOnOtherPlanes->Fill(eventsIt->second.getNumberOfHits()); 
    hIneffDut0HitOnStation0   ->Fill(counterStation0);
    hIneffDut0HitOnStation2   ->Fill(counterStation2);
    hIneffDut0HitOnStation5   ->Fill(counterStation5);
    hIneffDut0HitOnStation6   ->Fill(counterStation6);
    hIneffDut0HitOnStation7   ->Fill(counterStation7);

   if(pixelTelescope)
      {     
       hIneffDut0HitOnOtherPlanesPixels->Fill(eventsIt->second.getNumberOfHits()); 
       hIneffDut0HitOnStation0And2     ->Fill(counterStation0+counterStation2);
      }

     hIneffDut0HitOnStation4plaq1    ->Fill(counterStation4plaq1);
     hIneffDut0HitOnStation4plaq2    ->Fill(counterStation4plaq2); 
     hIneffDut0HitOnStation4plaq1And2->Fill(counterStation4plaq1+counterStation4plaq2); 

     }*/

//     cout << __PRETTY_FUNCTION__ << "Final number of saved ineff trigger: " << triggerCounter << endl;
     if(!found) continue;

 }

  outFile->Write();
  outFile->Close();

  inFile.close();
  if(writeOutFile)
    {
      outputFile.close();
      outputIneffFile.close();
    }

  outfile.close();

  cout << __PRETTY_FUNCTION__ << "Final number of events: " << theRun.getNumberOfEvents() << endl;

  return EXIT_SUCCESS;

}
