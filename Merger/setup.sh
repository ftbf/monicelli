##############
# The Merger #
##############
export MERGER_DIR=`pwd`

export MERGER_INPUT_DIR=/data/TestBeam/2024_06_June_T992/
export MERGER_OUTPUT_DIR=${MERGER_INPUT_DIR}/Merged/

export PH2ACF_BASE_DIR=/home/otsdaq/CMSPixel/Ph2_ACF
export CACTUSROOT=/opt/cactus
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:$CACTUSROOT/lib
