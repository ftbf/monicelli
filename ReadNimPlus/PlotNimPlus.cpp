#include <iostream>
#include <fstream>
#include <vector>
#include <cstdint>
#include <sstream>
#include <map>
#include <TFile.h>
#include <TH1F.h>
#include <TApplication.h>

using namespace std;

int main(int argc, char **argv)
{

  if(argc != 2)
    {
    std::cout << "Usage: PlotNimPlus RunNumber" << std::endl; 
    return 1;
    }
  std::string runNumber_ = argv[1];
  int debugLevel = 99;//101
  //**********************************
  //First read in the NimPlus timestamps --> We don't use this anymore because it was giving errors
  //**********************************
  //long triggerNumber=-1;
  vector<long long> NimPlusTimestamp;
  vector<long long> NimPlusTimestampDelay;
  stringstream fileName;
  //fileName << mainPath_ << "NimPlus/NIMPlus_RawData_Run" << runNumber_ << "_Raw.dat";
  std::string mainPath_ = getenv("NIM_INPUT_DIR");
  fileName << mainPath_ << "NIMPlus_RawData_Run" << runNumber_ << "_Raw.dat";
  ifstream nimPlusInputFile( fileName.str().c_str(), std::ifstream::binary);
  if(!nimPlusInputFile.is_open())
    std::cout << "Can't open file: " << fileName.str() << std::endl;

  int eventWordIndex=0;
  //long long firstTimeEver = 0;

  //bool isNewTrigger = false;
  //int maxPackets = 10;//999999999;
  unsigned int nOfWords = 6;
  //for( int iPacket = 0; iPacket < maxPackets; iPacket++){
  int32_t   previousTimingTrigger = -1;
  int32_t   timingTrigger         = 0;
  int32_t   telescopeTrigger	  = 0;
  uint64_t  timestamp		      = 0;
  uint32_t  tmpWord               = 0;
  uint8_t   numberOfQuadWords     = 0;
  uint8_t   type                  = 0;
  uint8_t   previousSequenceId    = 0;
  uint8_t   sequenceId		      = 0;
  uint32_t  outputA;
  uint32_t  outputB;
  uint32_t  outputC;
  uint32_t  outputD;
  int	    beginPacket           = 0;
  const int groupSize             = 6;
  int       groupCounter          = 0;
  uint32_t  triggerInfo[groupSize];
  std::map<uint64_t, uint32_t> outputATime_;
  std::map<uint64_t, uint32_t> outputBTime_;
  std::map<uint64_t, uint32_t> outputCTime_;
  std::map<uint64_t, uint32_t> outputDTime_;
  
  while(nimPlusInputFile)
    {

      nimPlusInputFile.read( reinterpret_cast<char *>(&numberOfQuadWords), 1); //no. of quad words in each packet (1 quadword= 8 bytes= 64 bits)
      if (debugLevel > 100) cout << "n quad: " << (int)numberOfQuadWords << " ";
      nimPlusInputFile.read( reinterpret_cast<char *>(&type), 1); //packet type -- 1,2 or 3
      if (debugLevel > 100) cout << "type: " << (int)type << " ";
      nimPlusInputFile.read( reinterpret_cast<char *>(&sequenceId), 1); // sequence ID -- increments by 1 each time
      if (debugLevel > 100) cout << "Seq id: " << (int)sequenceId << "\n";

      if( int(previousSequenceId+1) != int(sequenceId) && (int(previousSequenceId) != 255 && int(sequenceId) != 0)&& previousTimingTrigger != -1)
	{
	  std::cout << "Missed packet!! " << int(previousSequenceId+1) << "!=" <<  int(sequenceId) << std::endl;
	  std::cout << "Realigning!" << std::endl;
	}
      previousSequenceId = sequenceId;

      for(int i=0; i<numberOfQuadWords*2; i++)
	{
	  nimPlusInputFile.read( reinterpret_cast<char *>(&tmpWord), sizeof(uint32_t));
	  if(type == 1 || type == 2)
	    {
	      triggerInfo[groupCounter++] = tmpWord;
	      if(groupCounter == 6)
		{
		  groupCounter = 0;
		  outputATime_[(uint64_t(triggerInfo[5]) << 32) + triggerInfo[4]] = triggerInfo[0];
		  //std::cout << hex << "A: " << triggerInfo[0]   << " B: " << triggerInfo[1]	   << " C: " << triggerInfo[2]  << " D: " << triggerInfo[3] << " L: " << triggerInfo[4] << " H: " << triggerInfo[5] << dec << " time: " << (uint64_t(triggerInfo[5]) << 32) + triggerInfo[4] << std::endl;
		  if(previousTimingTrigger == -1)
		    {
		      outputA = triggerInfo[0] >> 28;
		      outputB = triggerInfo[1] >> 28;
		      outputC = triggerInfo[2] >> 28;
		      outputD = triggerInfo[3] >> 28;
		    }
		  else
		    {
		      while(  i<numberOfQuadWords*2
			      &&
			      (outputA != (triggerInfo[0] >> 28)
			       || outputB != (triggerInfo[1] >> 28)
			       || outputC != (triggerInfo[2] >> 28)
			       || outputD != (triggerInfo[3] >> 28)
			       )
			      )
			{
			  std::cout << "Need to realign!" << std::endl;
			  //std::cout << outputA << (triggerInfo[0] >> 28) << std::endl;
			  //std::cout << outputB << (triggerInfo[1] >> 28) << std::endl;
			  //std::cout << outputC << (triggerInfo[2] >> 28) << std::endl;
			  //std::cout << outputD << (triggerInfo[3] >> 28) << std::endl;
			  triggerInfo[0] = triggerInfo[2];
			  triggerInfo[1] = triggerInfo[3];
			  triggerInfo[2] = triggerInfo[4];
			  triggerInfo[3] = triggerInfo[5];

			  nimPlusInputFile.read( reinterpret_cast<char *>(&triggerInfo[4]), sizeof(uint32_t));
			  nimPlusInputFile.read( reinterpret_cast<char *>(&triggerInfo[5]), sizeof(uint32_t));
			  i+=2;
			}

		    }
		  timingTrigger    = triggerInfo[3] & 0xfffffff;//D
		  telescopeTrigger = triggerInfo[1] & 0xfffffff;//B
		  timestamp	   = (uint64_t(triggerInfo[5]) << 32) + triggerInfo[4];
		  if(previousTimingTrigger != timingTrigger && timingTrigger != 0
		     )
		    {
		      timestamp += uint64_t(tmpWord) << 32;
		      //triggerConverter_  [telescopeTrigger-1] = timingTrigger-1;
		      //triggerToTimestamp_[telescopeTrigger-1] = (uint64_t(triggerInfo[5]) << 32) + triggerInfo[4];
		      // std::cout 
		      //   << "\nTelescope: " << telescopeTrigger-1 
		      //   << "-> Timing: " << timingTrigger-1  
		      //   << " PrevTiming: " << previousTimingTrigger-1
		      //   << " timestamp: " << hex << timestamp << dec << std::endl;
		      previousTimingTrigger = timingTrigger;
		    }
		}
	    }
	}
      continue;
    }

  TApplication *myapp=new TApplication("myapp",0,0);
  std::string rootFileName = "Run" + runNumber_ + "_TimingPlots.root";
  //TFile* rootFile = TFile::Open(rootFileName.c_str(), "RECREATE");
  //TH1F* hOutputA = new TH1F("hOutputA", "OutputA Time Distribution", outputATime_.size(), 0, outputATime_.size());
  float timeUnit = (1/53.)/3;
  TH1F* hOutputA = new TH1F("hOutputA", "OutputA Time Distribution", 100001, -0.5*timeUnit, 100000.5*timeUnit);
  bool first = true;
  uint64_t previousTime = outputATime_.begin()->first;
  for(auto& a: outputATime_)
    {
      //std::cout << a.first-previousTime << std::endl;
	  hOutputA->Fill((a.first-previousTime)*timeUnit);
      previousTime = a.first;
    }
  
  hOutputA->Draw();
  //rootFile->Close();
  myapp->Run();
  return 1;
}
