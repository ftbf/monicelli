#include <iostream>
#include <vector>
#include <cstdint>


using namespace std;

int main()
{

  int debugLevel = 101;
  //**********************************
  //First read in the NimPlus timestamps --> We don't use this anymore because it was giving errors
  //**********************************
  long triggerNumber=-1; 
  vector<long long> NimPlusTimestamp;
  vector<long long> NimPlusTimestampDelay;
  //FILE* NimPlusInputFile = fopen( "/data/TestBeam/2017_12_December/NimPlus/RawDataSaver0NIM0_Run1117_0_Raw.dat", "rb" );
  FILE* NimPlusInputFile = fopen( "/data/TestBeam/2018_11_November_T992/NimPlus/NIMPlus_RawData_Run1896_Raw.dat", "rb" );
  //cout << NimPlusInputFileName << endl;
  // int cnt = 0;
  // while(!feof(NimPlusInputFile) && cnt++ < 100)
  // {

  //   cout << fgetc(NimPlusInputFile) << endl;
  // }
  // return 0;
  //ifstream myfile;
  //myfile.open(NimPlusInputFileName, ios::in | ios::binary);
  int eventWordIndex=0;
  long long firstTimeEver = 0;

  // firstTimeEver = 0x89abcdef;

  // cout << "BYTE order test" << endl;
  // cout << hex << firstTimeEver << dec << endl;
  // for(int i=0;i<4;++i)
  //   {
  //     cout << hex << (unsigned int)(((unsigned char *)(&firstTimeEver))[i]) << dec << " -----\n";
  //     (((unsigned char *)(&firstTimeEver))[i]) = i;
  //   }
  // cout << hex << firstTimeEver << dec << endl;

  // for(int i=0;i<4;++i)
  //   {
  //     cout << hex << (unsigned int)(((unsigned char *)(&firstTimeEver))[i]) << dec << " -----\n";
  //     (((unsigned char *)(&firstTimeEver))[i]) = 0;
  //   }
  // cout << hex << firstTimeEver << dec << endl;

  firstTimeEver = 0;

  bool isNewTrigger = false;
  int maxPackets = 10;//999999999;
  unsigned int nOfWords = 6;
  //for( int iPacket = 0; iPacket < maxPackets; iPacket++){ 
  while(!feof(NimPlusInputFile)){
    long tmpTrigger = 0;
    long long tmpTimestampPart1 = 0;
    long long tmpTimestampPart2 = 0;
    uint32_t  tmpWord = 0;
    
    uint8_t  numberOfQuadWords = 0;
    uint8_t  type              = 0;
    uint8_t  sequenceId        = 0;
    uint8_t  extra             = 0;
    uint32_t triggerA          = 0;
    uint32_t triggerB          = 0;
    uint32_t triggerC          = 0;
    uint32_t triggerD          = 0;
    uint64_t timestamp         = 0;

    long long t_diff=0;
    //cout << "Event: " << iPacket << " : ";    

    int x;
    // int k=-4;
    //if(iPacket<5){myfile>>x;cout<<static_cast<long>(x)<<endl;}
    //if(iPacket<5){myfile>>x;cout<<static_cast<long>(x)<<endl;}
    //unsigned char tmpC; 
    fread( &numberOfQuadWords, 1, 1, NimPlusInputFile); //no. of quad words in each packet (1 quadword= 8 bytes= 64 bits)
    if (debugLevel > 100) cout << "n quad: " << (int)numberOfQuadWords << " ";
    fread( &type, 1, 1, NimPlusInputFile); //packet type -- 1,2 or 3
    if (debugLevel > 100) cout << "type: " << (int)type << " ";
    fread( &sequenceId, 1, 1, NimPlusInputFile); // sequence ID -- increments by 1 each time
    if (debugLevel > 100) cout << "Seq id: " << (int)sequenceId << "\n";
    //fread( &extra, 1, 1, NimPlusInputFile); // maybe an extra byte
    //if (debugLevel > 100) cout << "Seq id: " << (int)extra << "\n";

    for(int i=0;i<numberOfQuadWords*2;i++){
      fread( &tmpWord, sizeof(uint32_t), 1, NimPlusInputFile); 
      //std::cout << tmpWord << std::endl;      
      if(type == 1 || type == 2){
    	if(eventWordIndex%nOfWords==0)
    	  std::cout << hex << "A: " << tmpWord << " - ";
    	else if(eventWordIndex%nOfWords==1)
    	  std::cout << "B: " << tmpWord << " - ";
    	else if(eventWordIndex%nOfWords==2)
    	  std::cout << "C: " << tmpWord << " - ";
    	else if(eventWordIndex%nOfWords==3)
    	  std::cout << "D: " << tmpWord << " - ";
   	else if(eventWordIndex%nOfWords==4)
	{
    	  timestamp = tmpWord;
	  timestamp = timestamp << 32;
	  std::cout << hex << "Time Low:  " << tmpWord << " - ";
    	}
	else if(eventWordIndex%nOfWords==5)
	{
    	  timestamp += tmpWord;
	  std::cout << "Time High: " << tmpWord << dec << std::endl;
	  //std::cout << hex << "Time: " << timestamp << dec << std::endl;	  
    	}
	eventWordIndex++;
      }
    }
    continue;
    for(int i=0;i<numberOfQuadWords*2;i++){

      //read 32-bit words
      tmpWord = 0;
      fread( &tmpWord, sizeof(float), 1, NimPlusInputFile); 
      if (debugLevel > 100) cout << "\t" << (eventWordIndex%6) << "-" <<  tmpWord << " ";
      //cout << "(k=" << eventWordIndex << ") ";

      //this is the trigger number word
      if (eventWordIndex%6==2) {
  	//a new trigger
  	if (tmpWord > triggerNumber) {
  	  triggerNumber++;
  	  isNewTrigger = true;
  	  if (debugLevel > 10) cout << "Trigger Number: " << tmpWord << " : ";
  	}
      }

      //first 32-bit word-part of the timestamp
      if(eventWordIndex%6==4){
  	if (isNewTrigger) {
	  
  	  // if(firstTimeEver == 0 || 
  	  //    (NimPlusTimestamp.size() && 
  	  //     (tmpWord-NimPlusTimestamp[NimPlusTimestamp.size()-1])*3 > 1000000000))
  	  //   firstTimeEver = tmpWord;

  	  // if(NimPlusTimestamp.size() &&
  	  //     NimPlusTimestamp[NimPlusTimestamp.size()-1] > tmpWord) {
  	  //     cout << "????";
  	  //     //tmpWord += ((long long)(1)<<32);
  	  // }
  	  tmpTimestampPart1 = tmpWord;	    	   
  	}      
      }

      if(eventWordIndex%6==5){
  	if (isNewTrigger) {  	
  	  tmpTimestampPart2 = tmpWord;
	  
  	  if (debugLevel > 100) cout << "-0x" << hex << tmpTimestampPart1 << dec << "\t\t-DIFF=" << 
  				  ((tmpTimestampPart1-firstTimeEver)*3.0f)/1000000.0f << "ms ";
  	  NimPlusTimestamp.push_back(tmpTimestampPart2*4294967296 + tmpTimestampPart1);
  	  if (debugLevel > 100) cout << "\t FullTimeStamp = " << (tmpTimestampPart2*4294967296 + tmpTimestampPart1);
  	  isNewTrigger = false;

  	}
	
  	if (debugLevel > 100) cout << endl;
      }

      if(eventWordIndex%6==5){
  	
      }
      eventWordIndex++;

    }
    if (debugLevel > 100) cout << "\n--------------------------------------------------------------------------------\n";
    // // check for end of file
    if (feof(NimPlusInputFile)) break;
  }

  //timestamps are in units of clock cycles (3ns each step)
  long long tmpRunningTimestamp = 0;
  for (int i=0; i<NimPlusTimestamp.size();i++) {
    // cout << "Trigger: " << i << " " << NimPlusTimestamp[i] << "\n";
    if (i==0) {
      NimPlusTimestampDelay.push_back(0);
    } else {
      //if (NimPlusTimestamp[i] - NimPlusTimestamp[i-1] > 0) {
      NimPlusTimestampDelay.push_back( (NimPlusTimestamp[i] - NimPlusTimestamp[i-1]) * 3); //delays are in units of ns
      //} else {
      //NimPlusTimestampDelay.push_back( (NimPlusTimestamp[i] + 4294967296 - NimPlusTimestamp[i-1]) * 3);
      //}
    }
    // cout << "Trigger: " << i << " | " << NimPlusTimestamp[i] << " | " 
    // 	 << (tmpRunningTimestamp +  NimPlusTimestampDelay[i])*1e-9 << " : " 
    // 	 << NimPlusTimestampDelay[i]*1e-9 << "\n";
    if (i>0 && NimPlusTimestamp[i] - NimPlusTimestamp[i-1] < 10000) {
      cout << "Trigger: " << i << " | " << NimPlusTimestamp[i] << " | " 
	   << NimPlusTimestamp[i] - NimPlusTimestamp[i-1] << " "
	   << "\n";
    }
    //if (i>1) {
    tmpRunningTimestamp +=  NimPlusTimestampDelay[i];
    //}
  }



  return 1;
}
