export NIM_INPUT_DIR=/data/TestBeam/2018_11_November_T992/NimPlus/
export NIM_OUTPUT_DIR=`pwd`

#===== Location of the ROOT components
export ROOTSYS=/opt/local/root

#===== Final PATH definitions
export PATH=$ROOTSYS/bin:${PATH}
export LD_LIBRARY_PATH=$ROOTSYS/lib

#----------------------------------------------------------------------------------------------------

