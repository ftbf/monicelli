# clean install, only when given the optional argument
if [ $# -ge 1 ] && [ "$1" == "initial" ]; then
    make -f MakefileAll distclean
fi

# (re)compile Monicelli
make -f MakefileAll

# (re)compile MonicelliExpress
cd Express
./compile.py
cd ..

