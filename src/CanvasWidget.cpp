/*===============================================================================
 * Chewie: the FERMILAB MTEST telescope and DUT anaysis tool
 * 
 * Copyright (C) 2014 
 *
 * Authors:
 *
 * Mauro Dinardo      (Universita' Bicocca) 
 * Dario Menasce      (INFN) 
 * Jennifer Ngadiuba  (INFN)
 * Lorenzo Uplegger   (FNAL)
 * Luigi Vigani       (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/

#include "CanvasWidget.h"
#include "ui_canvaswidget.h"
#include "MessageTools.h"

#include <QResizeEvent>

#include <TList.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <iostream>

//===========================================================================
CanvasWidget::CanvasWidget(QWidget * parent) :
    QWidget(parent)
  , ui(new Ui::canvasWidget)
  , logX_(false)
  , logY_(false)
  , logZ_(false)

{
    ui->setupUi(this);
    canvas_ = new QRootCanvas(this,"") ;
}

//===========================================================================
CanvasWidget::~CanvasWidget()
{
    STDLINE("Dtor",ACRed) ;
    delete canvas_;
}

//===========================================================================
void CanvasWidget::divide( int nx, int ny, float xmargin, float ymargin )
{
    canvas_->getCanvas()->Clear() ;
    canvas_->getCanvas()->Divide(nx, ny, xmargin, ymargin) ;
    canvas_->update();
}

//===========================================================================
void CanvasWidget::cd( int pos )
{
    canvas_->getCanvas()->cd(pos) ;
}

//===========================================================================
void CanvasWidget::flush( void )
{
    TObject * obj = nullptr ;
    TIter next(canvas_->getCanvas()->GetListOfPrimitives()) ;
    int pad = 0 ;
    while((obj = next()))
    {
        if( obj->InheritsFrom(TVirtualPad::Class()))
        {
            canvas_->getCanvas()->SetClickSelectedPad((TPad*)(obj)) ;
            canvas_->getCanvas()->SetClickSelected((obj)) ;
            canvas_->getCanvas()->cd(++pad) ;
            gPad->SetLogx(logX_);
            gPad->SetLogy(logY_);
            gPad->SetLogz(logZ_);
//            ss_.str("") ;
//            ss_ << "Working on pad " << tot ;
//            STDLINE(ss_.str(),ACWhite) ;
        }
    }
    update();
}

//===========================================================================
void CanvasWidget::update( void )
{
    canvas_->update();
}

//===========================================================================
void CanvasWidget::clear( )
{
    canvas_->getCanvas()->Clear();
    canvas_->update();
}

//===========================================================================
void CanvasWidget::paintEvent ( QPaintEvent * event )
{
    //std::cout << __PRETTY_FUNCTION__ << std::endl;
    int margin = 0 ;
    QSize newSize(event->rect().width()-margin, event->rect().height()-margin); // Allow room for margins
    canvas_->resize(newSize) ;
}


//===========================================================================
void CanvasWidget::setTitle(std::string title)
{
    canvas_->setWindowTitle(QApplication::translate("CanvasWidget", title.c_str(), 0, 0));
}
