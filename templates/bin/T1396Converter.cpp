#include "EventReader.h"
#include "T1396Converter.h"

#include <iostream>
//====================================================================
int main(int argv, char **argc)
{
  
  if ( argv < 3)
    {
      std::cout << __LINE__<< " " << __FILE__ << " usage: " << argc[0] << " root-file geo-file" << std::endl;  
      return 1;
    }

  EventReader * reader = new EventReader() ; 
  //reader->setPath ("/data/TestBeam/2014_02_February_T1037/MonicelliOutput/");
  //reader->setPath ("/data/TestBeam/2015_12_December/MonicelliOutput/");
  //reader->setPath ("/data/TestBeam/2016_12_December/MonicelliOutput/");
  //reader->setPath ("/data/TestBeam/2017_01_January/MonicelliOutput/");
  //reader->setPath ("/data/TestBeam/2017_04_April/MonicelliOutput/");
  //reader->setPath ("/data/TestBeam/2017_11_November/MonicelliOutput/");
  reader->setPath ("/data/TestBeam/2017_12_December/MonicelliOutput/");
  reader->openEventsFile(argc[1]);
  reader->openGeoFile   (argc[2]);
  T1396Converter* analyzer = new T1396Converter(reader) ;
  
  // app.Run() ;
    
  return 0 ;
}
