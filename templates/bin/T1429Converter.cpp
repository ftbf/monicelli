#include "EventReader.h"
#include "T1429Converter.h"

#include <iostream>
//====================================================================
int main(int argv, char **argc)
{
  
  if ( argv < 3)
    {
      std::cout << __LINE__<< " " << __FILE__ << " usage: " << argc[0] << " root-file geo-file" << std::endl;  
      return 1;
    }

  EventReader * reader = new EventReader() ; 
  reader->setPath ("/data/TestBeam/2018_03_March_T1429/MonicelliOutput/");
  reader->openEventsFile(argc[1]);
  reader->openGeoFile   (argc[2]);
  T1429Converter* analyzer = new T1429Converter(reader) ;
  
  // app.Run() ;
    
  return 0 ;
}
