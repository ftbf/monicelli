/*===============================================================================
 * Monicelli: the FERMILAB MTEST geometry builder and track reconstruction tool
 * 
 * Copyright (C) 2014 
 *
 * Authors:
 *
 * Dario Menasce      (INFN) 
 * Luigi Moroni       (INFN)
 * Jennifer Ngadiuba  (INFN)
 * Stefano Terzo      (INFN)
 * Lorenzo Uplegger   (FNAL)
 * Luigi Vigani       (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/
 
#ifndef _OuterTrackerConverter_h_
#define _OuterTrackerConverter_h_

#include "TTree.h"

class EventReader;
class Event;
class EventHeader;
class Geometry;
class TFile;
class TTree;


typedef struct 
{
  double    dxdz;
  double    dydz;
  double    xPos;
  double    yPos;
  double    chi2ndf;
  double    xPosErr;
  double    yPosErr;
  double    xPosPrevHit;
  double    yPosPrevHit;
  double    xPosErrPrevHit;
  double    yPosErrPrevHit;
  double    xPosNextHit;
  double    yPosNextHit;
  double    xPosErrNextHit;
  double    yPosErrNextHit;
  double    xPosPrevHitGlobal;
  double    yPosPrevHitGlobal;
  double    zPosPrevHitGlobal;
  double    xPosErrPrevHitGlobal;
  double    yPosErrPrevHitGlobal;
  double    zPosErrPrevHitGlobal;
  double    xPosNextHitGlobal;
  double    yPosNextHitGlobal;
  double    zPosNextHitGlobal;
  double    xPosErrNextHitGlobal;
  double    yPosErrNextHitGlobal;
  double    zPosErrNextHitGlobal;
  int       trigger;
  int       runNumber;
  long long timestamp;
  bool      statusPrevHit;
  bool      statusNextHit;
  
  void reset()
  {
    dxdz    		 = 0;
    dydz    		 = 0;
    xPos    		 = 0;
    yPos    		 = 0;
    chi2ndf 		 = 0;
    xPosErr              = 0;
    yPosErr              = 0;
    xPosPrevHit          = 0;
    yPosPrevHit          = 0;
    xPosErrPrevHit       = 0;
    yPosErrPrevHit       = 0;
    xPosNextHit          = 0;
    yPosNextHit          = 0;
    xPosErrNextHit       = 0;
    yPosErrNextHit       = 0;
    xPosPrevHitGlobal    = 0;
    yPosPrevHitGlobal    = 0;
    zPosPrevHitGlobal    = 0;
    xPosErrPrevHitGlobal = 0;
    yPosErrPrevHitGlobal = 0;
    zPosErrPrevHitGlobal = 0;
    xPosNextHitGlobal    = 0;
    yPosNextHitGlobal    = 0;
    zPosNextHitGlobal    = 0;
    xPosErrNextHitGlobal = 0;
    yPosErrNextHitGlobal = 0;
    zPosErrNextHitGlobal = 0;
    trigger              = 0;
    runNumber            = 0;
    timestamp            = 0;
    statusPrevHit	 = false;
    statusNextHit	 = false;
  }

} OuterTrackerEvent;


class OuterTrackerConverter
{
public:
   OuterTrackerConverter(EventReader* reader);
  ~OuterTrackerConverter(void);
  
  void analyzeEvent(unsigned int event) ;

private: 
  Event       * theEvent_    ;
  EventHeader * theHeader_   ;
  Geometry    * theGeometry_ ;
   
  TFile*     theFile_;
  TTree*     theTree_;
  OuterTrackerEvent theOuterTrackerEvent_;
   
  int runNumber_;
};

#endif
