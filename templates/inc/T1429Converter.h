/****************************************************************************
** Authors: Lorenzo Uplegger, Luigi Vigani
**
** I.N.F.N. Milan-Bicocca
** Piazza  della Scienza 3, Edificio U2
** Milano, 20126
**
****************************************************************************/
#ifndef _T1429Converter_h_
#define _T1429Converter_h_

#include "TTree.h"

class EventReader;
class Event;
class EventHeader;
class Geometry;
class TFile;
class TTree;


typedef struct 
{
  double    xSlope;
  double    ySlope;
  double    xIntercept;
  double    yIntercept;
  double    cov[6];
  double    chi2;
  double    x[22];
  double    y[22];
  double    z[22];
  double    xErr[22];
  double    yErr[22];
  double    zErr[22];
  int       status[22];
  int       trigger;
  int       runNumber;
  long long timestamp;
  long long stripCounter;
  long long fastCounter;

} ConvertedEvent;

class T1429Converter
{
public:
   T1429Converter(EventReader* reader);
  ~T1429Converter(void);
  
  void analyzeEvent(unsigned int event) ;

private: 
  Event       * theEvent_    ;
  EventHeader * theHeader_   ;
  Geometry    * theGeometry_ ;
   
  TFile*         theFile_;
  TTree*         theTree_;
  ConvertedEvent theConvertedEvent_;
   
  int runNumber_;
  std::map<std::string, unsigned int> moduleToPosition_;

};

#endif
