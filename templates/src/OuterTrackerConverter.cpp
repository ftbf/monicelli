/*===============================================================================
 * Monicelli: the FERMILAB MTEST geometry builder and track reconstruction tool
 * 
 * Copyright (C) 2014 
 *
 * Authors:
 *
 * Dario Menasce      (INFN) 
 * Luigi Moroni       (INFN)
 * Jennifer Ngadiuba  (INFN)
 * Stefano Terzo      (INFN)
 * Lorenzo Uplegger   (FNAL)
 * Luigi Vigani       (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/
 
#include "OuterTrackerConverter.h"
#include "EventReader.h"
#include "Event.h"
#include "MessageTools.h"
#include "Geometry.h"


#include <TFile.h>
#include <sstream>

using namespace std;

//------------------------------------------------------------------------------------
OuterTrackerConverter::OuterTrackerConverter(EventReader* reader)
{
  theHeader_   = reader->getEventHeaderPointer() ;
  theEvent_    = reader->getEventPointer      () ;
  theGeometry_ = reader->getGeometryPointer   () ;
  
  runNumber_ = reader->getRunNumber();
  
  stringstream fileName;
  //fileName << "/data/TestBeam/2014_02_February_OuterTracker/ConvertedOutput//Run" << runNumber_ << "_Merged_OuterTracker_converted.root";
  fileName << "/data/TestBeam/2017_12_December/OuterTrackerConverted/Run" << runNumber_ << "_Merged_OuterTracker_converted.root";

  theFile_ = TFile::Open(fileName.str().c_str(),"RECREATE");
  theTree_ = new TTree ("OuterTracker", "The reconstructed telescope tracks");
  //  theTree_->Branch("event", &theOuterTrackerEvent_, "xSlope/D:ySlope/D:xIntercept/D:yIntercept/D:chi2/D:trigger/I:runNumber/I:timestamp/L");
  //theTree_->Branch("event", &theOuterTrackerEvent_, "dxdz/D:dydz/D:xPos/D:yPos/D:chi2ndf/D:xPosErr/D:yPosErr/D:statusPrevHit/O:xPosPrevHit/D:yPosPrevHit/D:xPosErrPrevHit/D:yPosErrPrevHit/D:statusNextHit/O:xPosNextHit/D:yPosNextHit/D:xPosErrNextHit/D:yPosErrNextHit/D:xPosPrevHitGlobal/D:yPosPrevHitGlobal/D:zPosPrevHitGlobal/D:xPosErrPrevHitGlobal/D:yPosErrPrevHitGlobal/D:zPosErrPrevHitGlobal/D:xPosNextHitGlobal/D:yPosNextHitGlobal/D:zPosNextHitGlobal/D:xPosErrNextHitGlobal/D:yPosErrNextHitGlobal/D:zPosErrNextHitGlobal/D:trigger/I:runNumber/I:timestamp/L");
  theTree_->Branch("event", &theOuterTrackerEvent_, "dxdz/D:dydz/D:xPos/D:yPos/D:chi2ndf/D:xPosErr/D:yPosErr/D:xPosPrevHit/D:yPosPrevHit/D:xPosErrPrevHit/D:yPosErrPrevHit/D:xPosNextHit/D:yPosNextHit/D:xPosErrNextHit/D:yPosErrNextHit/D:xPosPrevHitGlobal/D:yPosPrevHitGlobal/D:zPosPrevHitGlobal/D:xPosErrPrevHitGlobal/D:yPosErrPrevHitGlobal/D:zPosErrPrevHitGlobal/D:xPosNextHitGlobal/D:yPosNextHitGlobal/D:zPosNextHitGlobal/D:xPosErrNextHitGlobal/D:yPosErrNextHitGlobal/D:zPosErrNextHitGlobal/D:trigger/I:runNumber/I:timestamp/L:statusPrevHit/O:statusNextHit/O");


  // Retrieve from file the number of stored events  
  unsigned int numberOfEvents = reader->getNumberOfEvents() ;
  
  std::stringstream ss;
  ss.str(""); 
  ss << "Found " << numberOfEvents << " events on file" ;
  STDLINE(ss.str(),ACYellow) ;
  STDLINE("       ",ACYellow) ;
  for(unsigned int event=0; event<numberOfEvents; ++event)
  //for(unsigned int event=0; event<100; ++event)
  {
    //    cout<<__LINE__<<endl;
    reader->readEvent(event) ;
    //    cout<<__LINE__<<endl;
    this->analyzeEvent(event) ;   
    //    cout<<__LINE__<<endl;
  }
  
  STDLINE("Writing Output File", ACRed);
  theFile_->Write("",TObject::kOverwrite);
  theFile_->Close();
  STDLINE("Done!", ACGreen);
}


//------------------------------------------------------------------------------------
OuterTrackerConverter::~OuterTrackerConverter ()
{
}

//------------------------------------------------------------------------------------
// This is the method of the OuterTrackerConverter class where users are supposed 
// to implement their own private code for analysis. Here you will find 
// an exampl of how to retrieve and manipulate components of the event
// and the geometry.
//
// NOTE: for a detailed description of the Event class, its public access
//       methods and the meaning of invidual containers, please consult
//       ../include/Event.h and comments therein.
//
void OuterTrackerConverter::analyzeEvent(unsigned int event)
{
  const std::string prevModuleName = "Station: 2 - Plaq: 0";
  const std::string nextModuleName = "Station: 0 - Plaq: 3";
  
  Event::fittedTracksDef           & fittedTracks             = theEvent_->getFittedTracks          ();
  Event::chi2VectorDef             & chi2                     = theEvent_->getFittedTracksChi2      ();
  Event::fittedTracksCovarianceDef & fittedTrackCovariance    = theEvent_->getFittedTracksCovariance();
  Event::clustersMapDef            & alignedClusters          = theEvent_->getAlignedClusters       ();
  Event::trackCandidatesDef        & trackPoints              = theEvent_->getTrackCandidates       ();
  ROOT::Math::SVector<double,4>      trackParameters;
  //bool statusPrevHit;
  //bool statusNextHit;
  
  if( fittedTracks.size() == 0 ) return ;

  int clusterIDPrevHit;
  int clusterIDNextHit;
  for(unsigned int track=0; track<fittedTracks.size(); track++)
  {
    //cout<<endl;
    //cout<<"Looking at track "<<tr<<endl;
    theOuterTrackerEvent_.reset();
    
    trackParameters = fittedTracks[track] ;
    // Rename and add uncertainties to match: https://github.com/sroychow/TestBeamAnalysis/blob/Nov17/EdmToNtupleNoMask/interface/Track.h
    theOuterTrackerEvent_.dxdz       = trackParameters[0];
    theOuterTrackerEvent_.dydz       = trackParameters[2];
    theOuterTrackerEvent_.xPos       = trackParameters[1]*10;
    theOuterTrackerEvent_.yPos       = trackParameters[3]*10;
    theOuterTrackerEvent_.chi2ndf    = chi2[track];
    theOuterTrackerEvent_.xPosErr    = sqrt(fittedTrackCovariance[track](1,1))*10;
    theOuterTrackerEvent_.yPosErr    = sqrt(fittedTrackCovariance[track](3,3))*10;


//**  ----------------------------------------------------------------------------------------------
//**  The vector of fitted tracks: for each plaquette the coordinates/errors of the cluster hits 
//**  used for the final fit are provided in the laboratory reference frame. Additionally it is 
//**  possible to gain access to the cluster used to perform the fit by means of the "cluster ID"
//**  
//**  vector<map<string, map<string, double> > >                            trackCandidates_
//**    |          |           |       |
//**    |          |           |       +--------------> value
//**    |          |           +----------------------> x | y | z | xErr | yErr | errZ | xyErr | size | cluster ID | dataType | charge
//**    |          +----------------------------------> plaquette  ID
//**    +---------------------------------------------> vector of track candidates
    
    if( trackPoints[track].find(prevModuleName) != trackPoints[track].end() )
    {
      //statusPrevHit        = true;
      theOuterTrackerEvent_.statusPrevHit        = true;
      theOuterTrackerEvent_.xPosPrevHitGlobal    = trackPoints[track][prevModuleName]["x"]*10;
      theOuterTrackerEvent_.yPosPrevHitGlobal    = trackPoints[track][prevModuleName]["y"]*10;
      theOuterTrackerEvent_.zPosPrevHitGlobal    = trackPoints[track][prevModuleName]["z"]*10;
      theOuterTrackerEvent_.xPosErrPrevHitGlobal = trackPoints[track][prevModuleName]["xErr"]*10;
      theOuterTrackerEvent_.yPosErrPrevHitGlobal = trackPoints[track][prevModuleName]["yErr"]*10;
      theOuterTrackerEvent_.zPosErrPrevHitGlobal = trackPoints[track][prevModuleName]["zErr"]*10;
      clusterIDPrevHit                           = trackPoints[track][prevModuleName]["cluster ID"];                             
    }
    if( trackPoints[track].find(nextModuleName) != trackPoints[track].end() )
    {
      //statusNextHit        = true;
      theOuterTrackerEvent_.statusNextHit        = true;
      theOuterTrackerEvent_.xPosNextHitGlobal    = trackPoints[track][nextModuleName]["x"]*10;
      theOuterTrackerEvent_.yPosNextHitGlobal    = trackPoints[track][nextModuleName]["y"]*10;
      theOuterTrackerEvent_.zPosNextHitGlobal    = trackPoints[track][nextModuleName]["z"]*10;
      theOuterTrackerEvent_.xPosErrNextHitGlobal = trackPoints[track][nextModuleName]["xErr"]*10;
      theOuterTrackerEvent_.yPosErrNextHitGlobal = trackPoints[track][nextModuleName]["yErr"]*10;
      theOuterTrackerEvent_.zPosErrNextHitGlobal = trackPoints[track][nextModuleName]["zErr"]*10;
      clusterIDNextHit                           = trackPoints[track][nextModuleName]["cluster ID"];                             
    }

//**  ----------------------------------------------------------------------------------------------
//**  The cluster hit coordinates/errors computed for each plaquette by using the charge sharing
//**  wheighting. The x/y coordinates are computed in the local plaquette reference frame.
//**  dataType is 0 for pixels and 1 for strips.
//**
//**  map<string, map<int, map<string, double> > >                          clusters_  
//**         |         |         |       |
//**         |         |         |       +------------> value
//**         |         |         +--------------------> x | y | xErr | yErr | charge | dataType | size
//**         |         +------------------------------> cluster ID number
//**         +----------------------------------------> plaquette ID

    //if(statusPrevHit) 
    if(theOuterTrackerEvent_.statusPrevHit) 
    {
      theOuterTrackerEvent_.xPosPrevHit       = alignedClusters[prevModuleName][clusterIDPrevHit]["x"]*10;
      theOuterTrackerEvent_.yPosPrevHit       = alignedClusters[prevModuleName][clusterIDPrevHit]["y"]*10;
      theOuterTrackerEvent_.xPosErrPrevHit    = alignedClusters[prevModuleName][clusterIDPrevHit]["xErr"]*10;
      theOuterTrackerEvent_.yPosErrPrevHit    = alignedClusters[prevModuleName][clusterIDPrevHit]["yErr"]*10;
    }
    //if(statusNextHit) 
    if(theOuterTrackerEvent_.statusNextHit) 
    {
      theOuterTrackerEvent_.xPosNextHit       = alignedClusters[nextModuleName][clusterIDNextHit]["x"]*10;
      theOuterTrackerEvent_.yPosNextHit       = alignedClusters[nextModuleName][clusterIDNextHit]["y"]*10;
      theOuterTrackerEvent_.xPosErrNextHit    = alignedClusters[nextModuleName][clusterIDNextHit]["xErr"]*10;
      theOuterTrackerEvent_.yPosErrNextHit    = alignedClusters[nextModuleName][clusterIDNextHit]["yErr"]*10;
    }

    theOuterTrackerEvent_.trigger    = theEvent_->getTrigger();
    theOuterTrackerEvent_.runNumber  = runNumber_;
    theOuterTrackerEvent_.timestamp  = theEvent_->getUTC();
    theTree_->Fill();

//     stringstream ss;
//     ss.str("") ;
//     ss.setf(std::ios_base::right,std::ios_base::adjustfield) ;
//     ss << std::setprecision(8) << " ";
//     ss << std::setw( 4) << track 
// 	<< std::setw(15) << trackParameters[0] 
// 	<< std::setw(15) << trackParameters[2] 
// 	<< std::setw(15) << trackParameters[1] 
// 	<< std::setw(15) << trackParameters[3] 
// 	<< std::setprecision(3)
// 	<< std::setw( 6) << chi2[track]
// 	<< " : " << theOuterTrackerEvent_.statusPrevHit 
// 	<< " : " << theOuterTrackerEvent_.xPosPrevHit
// 	<< " : " << theOuterTrackerEvent_.zPosPrevHit
// 	<< " : " << theOuterTrackerEvent_.statusNextHit 
// 	<< " : " << theOuterTrackerEvent_.xPosNextHit
// 	<< " : " << theOuterTrackerEvent_.zPosNextHit; 
//     STDLINE(ss.str(),ACGreen) ;
    
    
  }
                                 
}
