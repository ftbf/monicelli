/*===============================================================================
 * Monicelli: the FERMILAB MTEST geometry builder and track reconstruction tool
 *
 * Copyright (C) 2014
 *
 * Authors:
 *
 * Dario Menasce      (INFN)
 * Luigi Moroni       (INFN)
 * Jennifer Ngadiuba  (INFN)
 * Stefano Terzo      (INFN)
 * Lorenzo Uplegger   (FNAL)
 * Luigi Vigani       (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/
#include "ROC.h"

#include <iostream>

using namespace std;

ClassImp(ROC)

//==========================================================================
ROC::ROC(unsigned int position,
         int          chipID,
         unsigned int degrees) :
    rocLengthX_         (0                              )
  ,rocLengthY_         (0                              )
  ,numberOfRows_       (0                              )
  ,numberOfCols_       (0                              )
  ,chipID_             (chipID                         )
  ,orientation_        (0                              )
  ,position_           (position                       )
  ,standardPixelPitch_ (std::pair<double,double>(-1,-1))
  ,calibrationFilePath_(""                             )
{
    this->setOrientation(degrees);
}

//===============================================================================
double ROC::linearFitFunctionROOT(double *x, double *par)
{
    return x[0]*par[0] + par[1];
}

//===============================================================================
double ROC::parabolicFitFunctionROOT(double *x, double *par)
{
    return x[0]*x[0]*par[0] + x[0]*par[1] + par[2];
}

//===============================================================================
double ROC::tanhFitFunctionROOT(double *x, double *par)
{
    return par[0] + par[1]*tanh(par[2]*x[0] + par[3]);
}

//===============================================================================
double ROC::timewalkFitFunctionROOT(double *x, double *par)
{
    // See https://cds.cern.ch/record/2649493/files/CLICdp-Note-2018-008.pdf
    return x[0]*par[0] + par[1] - par[2]/(x[0]-par[3]);
}

//===============================================================================
double ROC::noneFitFunctionROOT(double *x, double */*par*/)
{
    return x[0];
}

//===============================================================================
double ROC::calibrationFitFunction(double *x, double *par, bool isDut)
{
    if      (isDut && calibrationFunctionType_ == "linear"   ) return x[0]*par[0] + par[1];
    else if (isDut && calibrationFunctionType_ == "parabolic") return x[0]*x[0]*par[0] + x[0]*par[1] + par[2] ;
    else if (isDut && calibrationFunctionType_.find("lookup") != std::string::npos) return 14.0; //Cheating -- just returning maxToT here
    else if (isDut && calibrationFunctionType_ == "timewalk")  return x[0]*par[0] + par[1] - par[2]/(x[0]-par[3]);
    else if (isDut && calibrationFunctionType_ == "none"     ) return x[0];
    else                                                       return par[0] + par[1]*tanh(par[2]*x[0] + par[3]);
}

//===============================================================================
double ROC::calibrationFitFunctionInv(double *x, double *par)
{
    if      (calibrationFunctionType_ == "linear"   ) return (x[0] - par[1]) / par[0];
    else if (calibrationFunctionType_ == "parabolic")
    {
        if ((par[1]*par[1] - 4.*par[0]*(par[2] - x[0])) >= 0)
            return ((-par[1] + sqrt(par[1]*par[1] - 4.*par[0]*(par[2] - x[0]))) / (2.*par[0]));
        else
            return 0.;
    }
    else if (calibrationFunctionType_ == "lookup"   ) {
      if      (x[0] == 0.0)  return  par[0];
      else if (x[0] == 1.0)  return (par[0]  + par[1])  / 2.0;
      else if (x[0] == 2.0)  return (par[1]  + par[2])  / 2.0;
      else if (x[0] == 3.0)  return (par[2]  + par[3])  / 2.0;
      else if (x[0] == 4.0)  return (par[3]  + par[4])  / 2.0;
      else if (x[0] == 5.0)  return (par[4]  + par[5])  / 2.0;
      else if (x[0] == 6.0)  return (par[5]  + par[6])  / 2.0;
      else if (x[0] == 7.0)  return (par[6]  + par[7])  / 2.0;
      else if (x[0] == 8.0)  return (par[7]  + par[8])  / 2.0;
      else if (x[0] == 9.0)  return (par[8]  + par[9])  / 2.0;
      else if (x[0] == 10.0) return (par[9]  + par[10]) / 2.0;
      else if (x[0] == 11.0) return (par[10] + par[11]) / 2.0;
      else if (x[0] == 12.0) return (par[11] + par[12]) / 2.0;
      else if (x[0] == 13.0) return (par[12] + par[13]) / 2.0;
      else if (x[0] == 14.0) return  par[13];
      else {
	std::cout << "Invalid ToT! " << x[0] << std::endl;
	return par[13];
      }
    }
    else if (calibrationFunctionType_ == "lookup1"   ) {
      if      (x[0] == 0.0)  return par[0];
      else if (x[0] == 1.0)  return par[1];
      else if (x[0] == 2.0)  return par[2];
      else if (x[0] == 3.0)  return par[3];
      else if (x[0] == 4.0)  return par[4];
      else if (x[0] == 5.0)  return par[5];
      else if (x[0] == 6.0)  return par[6];
      else if (x[0] == 7.0)  return par[7];
      else if (x[0] == 8.0)  return par[8];
      else if (x[0] == 9.0)  return par[9];
      else if (x[0] == 10.0)  return par[10];
      else if (x[0] == 11.0)  return par[11];
      else if (x[0] == 12.0)  return par[12];
      else if (x[0] == 13.0)  return par[13];
      else if (x[0] == 14.0)  return par[14];
      else {
	std::cout << "Invalid ToT! " << x[0] << std::endl;
	return par[14];
      }
    }
    else if (calibrationFunctionType_ == "timewalk")
    {
        // Inverse taken from https://www.wolframalpha.com/input/?i=a*y+%2B+b+-+c%2F%28y-d%29+%3D+x+solve+for+y
        double a = par[0];
        double b = par[1];
        double c = par[2];
        double d = par[3];

        if (a != 0)
        {
            double radicand = a*a*d*d + 2*a*b*d + 4*a*c - 2*a*d*x[0] + b*b - 2*b*x[0] + x[0]*x[0];
            if (radicand >= 0) {
                // This first one always seems to be true, but keep the other solutions just in case
                if      (sqrt(radicand) !=  a*d + b - x[0]) return ( sqrt(radicand) + a*d - b + x[0]) / (2*a);
                else if (sqrt(radicand) != -a*d - b + x[0]) return (-sqrt(radicand) + a*d - b + x[0]) / (2*a);
            }
        }
        else if (a == 0 && b != x[0] && c != 0) return (b*d + c - d*x[0]) / (b - x[0]);

        return 0;
    }
    else if (calibrationFunctionType_ == "none"     ) return x[0];
    else                                              return x[0];
}

//==========================================================================
void ROC::setOrientation(unsigned int degrees)
{
    if (degrees == 0 || degrees == 180) orientation_ = degrees;
    else if(degrees == 90 || degrees == 270)
    {
        STDLINE("ERROR: Only 0 and 180 degrees rotations are supported for a ROC!", ACRed);
        exit(EXIT_FAILURE);
    }
    else
    {
        STDLINE("ERROR: Only 0 and 180 degrees rotations are supported for a ROC!", ACRed);
        exit(EXIT_FAILURE);
    }
}

//==========================================================================
bool ROC::goodRow(unsigned int row)
{
    if (row < numberOfRows_) return true;
    else
    {
        ss_.str("");
        ss_ << "WARNING: no pixel found with col number " <<  row;
        STDLINE(ss_.str(), ACRed);
        return false;
    }
}

//==========================================================================
bool ROC::goodCol(unsigned int col)
{
    if (col < numberOfCols_) return true;
    else
    {
        ss_.str("");
        ss_ << "WARNING: no pixel found with col number " <<  col;
        STDLINE(ss_.str(), ACRed);
        return false;
    }
}

//==========================================================================
void ROC::setRowPitchVector(void)
{
    if (numberOfRows_ == 0 || standardPixelPitch_.first == 0) return;
    rowPitches_.clear();
    rowLowEdge_.clear();
    rowPitches_.resize(numberOfRows_, standardPixelPitch_.first);
    rowLowEdge_.resize(numberOfRows_,0);
    for (nonStandardPitchMapDef::iterator it = nonStandardRowPitch_.begin(); it != nonStandardRowPitch_.end(); it++)
    {
        if(it->first < rowPitches_.size())
        {
            rowPitches_[it->first] = it->second;
        }
    }
    rowLowEdge_[0] = 0;
    for (unsigned int r=1; r<rowPitches_.size(); r++)
    {
        rowLowEdge_[r] = rowLowEdge_[r-1] + rowPitches_[r-1];
    }
    rocLengthY_ = rowLowEdge_[rowLowEdge_.size()-1] + rowPitches_[rowPitches_.size()-1];
}

//==========================================================================
void ROC::setCalibrationFunctionType(string type)
{
    calibrationFunctionType_ = type ;
    if (calibrationFunctionType_.find("lookup") != std::string::npos)
    {
      size_t npar = sizeof(par_) / sizeof(par_[0]);
      if (npar < 15)
      {
        ss_.str("") ; ss_ << "ABORT: Lookup calibration function is being used and requires par_ to be of size 15 but it is only of size " << npar << "! Need to modify ROC.h accordingly. Note that any existing Monicelli output files will be incompatible with Chewie once ROC.h is modified!" ;
        STDLINE(ss_.str(),ACRed) ;
        exit(-1) ;
      }
    }
}
//==========================================================================
void ROC::setColPitchVector(void)
{
    if (numberOfCols_ == 0 || standardPixelPitch_.second == 0) return;
    colPitches_.clear();
    colLowEdge_.clear();
    colPitches_.resize(numberOfCols_, standardPixelPitch_.second);
    colLowEdge_.resize(numberOfCols_,0);
    for (nonStandardPitchMapDef::iterator it = nonStandardColPitch_.begin(); it != nonStandardColPitch_.end(); it++)
    {
        if(it->first < colPitches_.size())
        {
            colPitches_[it->first] = it->second;
        }
    }
    colLowEdge_[0] = 0;
    for (unsigned int c=1; c<colPitches_.size(); c++)
    {
        colLowEdge_[c] = colLowEdge_[c-1] + colPitches_[c-1];
    }
    rocLengthX_ = colLowEdge_[colLowEdge_.size()-1] + colPitches_[colPitches_.size()-1];
}

//==========================================================================
void ROC::setNumberOfRowsCols(unsigned int numberOfRows, unsigned int numberOfCols)
{
    numberOfRows_ = numberOfRows;
    setRowPitchVector();
    numberOfCols_ = numberOfCols;
    setColPitchVector();
}

//==========================================================================
void ROC::setNumberOfRows(unsigned int numberOfRows)
{
    numberOfRows_ = numberOfRows;
    setRowPitchVector();
}

//==========================================================================
void ROC::setNumberOfCols(unsigned int numberOfCols)
{
    numberOfCols_ = numberOfCols;
    setColPitchVector();
}

//==========================================================================
void ROC::setStandardPixPitch(double row_cm, double col_cm)
{
    standardPixelPitch_ = std::make_pair(row_cm,col_cm);
    setRowPitchVector();
    setColPitchVector();
}

//==========================================================================
void ROC::setOneRowPitch(unsigned int row, double row_cm)
{
    nonStandardRowPitch_[row] = row_cm;
    setRowPitchVector();
}

//==========================================================================
void ROC::setOneColPitch(unsigned int col, double col_cm)
{
    nonStandardColPitch_[col] = col_cm;
    setColPitchVector();
}

//==========================================================================
double ROC::getPixelCenterLocalX( unsigned int pixelCol)
{
    if (!goodCol(pixelCol)) return -1;
    return (this->getPixelLowEdgeLocalX(pixelCol) + this->getPixelPitchLocalX(pixelCol)/2.);
}

//==========================================================================
double ROC::getPixelCenterLocalY(unsigned int pixelRow)
{
    if (!goodRow(pixelRow)) return -1;
    return (this->getPixelLowEdgeLocalY(pixelRow) + this->getPixelPitchLocalY(pixelRow)/2.);
}

//==========================================================================
double ROC::getPixelLowEdgeLocalX(unsigned int pixelCol)
{
    if (!goodCol(pixelCol)) return -1;

    if (orientation_ == 0)
        return colLowEdge_[pixelCol];
    else if(orientation_ == 180)
        return rocLengthX_ - colLowEdge_[pixelCol] - colPitches_[pixelCol];
    else
    {
        ss_.str("");
        ss_ << "WARNING: orientation not supported " <<  orientation_;
        STDLINE(ss_.str(), ACRed);
        return -1;
    }
}

//==========================================================================
double ROC::getPixelLowEdgeLocalY(unsigned int pixelRow)
{
    if (!goodRow(pixelRow)) return -1;

    if (orientation_ == 0)
        return rowLowEdge_[pixelRow];
    else if(orientation_ == 180)
        return rocLengthY_ - rowLowEdge_[pixelRow] - rowPitches_[pixelRow];
    else
    {
        ss_.str("");
        ss_ << "WARNING: orientation not supported " <<  orientation_;
        STDLINE(ss_.str(), ACRed);
        return -1;
    }
}

//==========================================================================
double ROC::getPixelHiEdgeLocalX(unsigned int pixelCol)
{
    if (!goodCol(pixelCol)) return -1;
    return (this->getPixelLowEdgeLocalX(pixelCol) + this->getPixelPitchLocalX(pixelCol));
}

//==========================================================================
double ROC::getPixelHiEdgeLocalY(unsigned int pixelRow)
{
    if (!goodRow(pixelRow)) return -1;
    return (this->getPixelLowEdgeLocalY(pixelRow) + this->getPixelPitchLocalY(pixelRow));
}

//==========================================================================
double ROC::getPixelPitchLocalX(unsigned int pixelCol)
{
    if (!goodCol(pixelCol)) return -1;
    return colPitches_[pixelCol];
}

//==========================================================================
double ROC::getPixelPitchLocalY(unsigned int pixelRow)
{
    if (!goodRow(pixelRow)) return -1;
    return rowPitches_[pixelRow];
}

//==========================================================================
double ROC::getLengthLocalX(void)
{
    return rocLengthX_;
}

//==========================================================================
double ROC::getLengthLocalY (void)
{
    return rocLengthY_;
}

//=============================================================================
bool ROC::calibratePixel(int row, int col, int adc, int& charge, std::string BBmap)
{
    double newAdc[1];
    newAdc[0] = adc;
    double *par = this->getCalibrationFunction(row, col, BBmap);

    if (par != 0)
    {
        charge = (int)ROC::calibrationFitFunctionInv(newAdc , par);
        //cout<<__LINE__<<__PRETTY_FUNCTION__<<" charge: "<<charge<<endl;
        return true;
    }
    else
    {
        charge = adc;
        //cout<<__LINE__<<__PRETTY_FUNCTION__<<" charge: "<<charge<<endl;
        return false;
    }
}

//=============================================================================
bool ROC::isPixelCalibrated(int row, int col, std::string BBmap)
{
    if(BBmap != "")
        this->convertSensorRowColToChip(&row,&col,BBmap);

    if (pixelCalibrationFunctionTmp_.count(row) && pixelCalibrationFunctionTmp_[row].count(col))
        return true;
    else
        return false;
}

//============================================================================
void ROC::setCalibrationFunction(int row, int col, double *par, double */*cov*/, int npar)
{
    if (pixelCalibrationFunctionTmp_.count(row) && pixelCalibrationFunctionTmp_[row].count(col))
        pixelCalibrationFunctionTmp_.clear();

    if(par != NULL)
    {
      for(int i=0; i < npar; i++) //
        {
            pixelCalibrationFunctionTmp_[row][col].push_back(par[i]);
        }
    }
}

//============================================================================
double* ROC::getCalibrationFunction(int row, int col, std::string BBmap)
{
    if(BBmap != "")
        this->convertSensorRowColToChip(&row,&col,BBmap);

    if (pixelCalibrationFunctionTmp_.count(row) && pixelCalibrationFunctionTmp_[row].count(col))
        for(unsigned int i = 0; i < pixelCalibrationFunctionTmp_[row][col].size(); i++)
            par_[i] = pixelCalibrationFunctionTmp_[row][col][i] ;
    else
        return 0;

    return par_;
}

//============================================================================
double ROC::getCalibrationError(int /*row*/, int /*col*/, int /*adc*/)
{
    return -1;
}

//=================================================================
//From row and col of the chip to row and col of the sensor (BB map) for 25x100 RD53 modules
void ROC::convertChipRowColToSensor(int *row, int *col, std::string BBmap)
{
    if (BBmap == "25x100up")
    {
      *row = *row * 2 + (*col % 2);
      *col = *col / 2;
    }
    else if (BBmap == "25x100down")
    {
      *row = *row * 2 + (1 - *col % 2);
      *col = *col / 2;
    }
    else if (BBmap == "25x100INFN")
    {
      *row = *row * 2 + (*col % 2);
      *col = *col / 2;
    }

    // The old mappings from YARR are below (no longer needed now that we use FC7)
    // RD53A read out in YARR have rows and columns starting from 1, not 0!
    /*
    if (BBmap == "25x100up")
    {
      *row = *row - 1; //Compensate for row/col starting at 1 instead of 0
      *col = *col - 1;
      *row = *row * 2 + (*col % 2);
      *col = *col / 2;
      *row = *row + 1;
      *col = *col + 1;
    }
    if (BBmap == "25x100down")
    {
      *row = *row - 1;
      *col = *col - 1;
      *row = *row * 2 + (1 - *col % 2);
      *col = *col / 2;
      *row = *row + 1;
      *col = *col + 1;
    }
    */
}
//=================================================================
//From row and col of the sensor to row and col of the chip (BB map) for 25x100 RD53 modules
void ROC::convertSensorRowColToChip(int *row, int *col, std::string BBmap)
{
    if (BBmap == "25x100up")
    {
      *col = *col * 2 + (*row % 2);
      *row = *row / 2;
    }
    else if (BBmap == "25x100down")
    {
      *col = *col * 2 + (1 - *row % 2);
      *row = *row / 2;
    }
    else if (BBmap == "25x100INFN")
    {
      *col = *col * 2 + (*row % 2);
      *row = *row / 2;
    }

    // The old mappings from YARR are below (no longer needed now that we use FC7)
    // RD53A read out in YARR have rows and columns starting from 1, not 0!
    /*
    if (BBmap == "25x100up")
    {
      *row = *row - 1;
      *col = *col - 1;
      *col = *col * 2 + (*row % 2);
      *row = *row / 2;
      *row = *row + 1;
      *col = *col + 1;
    }
    if (BBmap == "25x100down")
    {
      *row = *row - 1;
      *col = *col - 1;
      *col = *col * 2 + (1 - *row % 2);
      *row = *row / 2;
      *row = *row + 1;
      *col = *col + 1;
    }
    */
}

//=================================================================
//Get number of rows in chip for 25x100 RD53 modules
unsigned int ROC::getNumberOfRowsChip(std::string BBmap)
{
  if      (BBmap == "25x100up"  || BBmap == "25x100down") return  numberOfRows_ / 2;
  else if (BBmap == "25x100INFN"                        ) return  numberOfRows_ / 2;
  else                                                    return  numberOfRows_    ;

  // The old mappings from YARR are below (no longer needed now that we use FC7)
  // RD53A read out in YARR have rows and columns starting from 1, not 0!
  //if      (BBmap == "25x100up"  || BBmap == "25x100down") return (numberOfRows_ - 1) / 2 + 1;
}

//=================================================================
//Get number of cols in chip for 25x100 RD53 modules
unsigned int ROC::getNumberOfColsChip(std::string BBmap)
{
  if      (BBmap == "25x100up"  || BBmap == "25x100down") return  numberOfCols_ * 2;
  else if (BBmap == "25x100INFN"                        ) return  numberOfCols_ * 2;
  else                                                    return  numberOfCols_    ;

  // The old mappings from YARR are below (no longer needed now that we use FC7)
  // RD53A read out in YARR have rows and columns starting from 1, not 0!
  //if      (BBmap == "25x100up"  || BBmap == "25x100down") return (numberOfCols_ - 1) * 2 + 1;
}
