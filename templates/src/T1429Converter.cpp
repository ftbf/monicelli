/****************************************************************************
** Authors: Lorenzo Uplegger, Luigi Vigani
**
** I.N.F.N. Milan-Bicocca
** Piazza  della Scienza 3, Edificio U2
** Milano, 20126
**
****************************************************************************/
#include "T1429Converter.h"
#include "EventReader.h"
#include "Event.h"
#include "MessageTools.h"

#include <TFile.h>
#include <sstream>
#include <TH2D.h>
#include <TCanvas.h>

using namespace std;

//------------------------------------------------------------------------------------
T1429Converter::T1429Converter(EventReader* reader)
{
  moduleToPosition_["Station: 5 - Plaq: 0"]  = 0 ;     
  moduleToPosition_["Station: 5 - Plaq: 1"]  = 1 ;     
  moduleToPosition_["Station: 5 - Plaq: 2"]  = 2 ;     
  moduleToPosition_["Station: 5 - Plaq: 3"]  = 3 ;     
  moduleToPosition_["Station: 6 - Plaq: 0"]  = 4 ;     
  moduleToPosition_["Station: 6 - Plaq: 1"]  = 5 ;     
  moduleToPosition_["Station: 6 - Plaq: 2"]  = 6 ;     
  moduleToPosition_["Station: 6 - Plaq: 3"]  = 7 ;     
  moduleToPosition_["Station: 2 - Plaq: 0"]  = 8 ;     
  moduleToPosition_["Station: 2 - Plaq: 1"]  = 9 ;     
  moduleToPosition_["Station: 2 - Plaq: 2"]  = 10;     
  moduleToPosition_["Station: 2 - Plaq: 3"]  = 11;     
  moduleToPosition_["Station: 0 - Plaq: 0"]  = 12;     
  moduleToPosition_["Station: 0 - Plaq: 1"]  = 13;     
  moduleToPosition_["Station: 0 - Plaq: 2"]  = 14;     
  moduleToPosition_["Station: 0 - Plaq: 3"]  = 15;     
  moduleToPosition_["Station: 7 - Plaq: 0"]  = 16;     
  moduleToPosition_["Station: 7 - Plaq: 1"]  = 17;     
  moduleToPosition_["Station: 7 - Plaq: 2"]  = 18;     
  moduleToPosition_["Station: 7 - Plaq: 3"]  = 19;     
  moduleToPosition_["Station: 7 - Plaq: 4"]  = 20;     
  moduleToPosition_["Station: 7 - Plaq: 5"]  = 21;     

  theHeader_   = reader->getEventHeaderPointer() ;
  theEvent_    = reader->getEventPointer      () ;
  theGeometry_ = reader->getGeometryPointer   () ;
  
  runNumber_ = reader->getRunNumber();
  
  stringstream fileName;
  fileName << "/data/TestBeam/2018_03_March_T1429/T1429/Run" << runNumber_ << "_Converted.root";
  

  theFile_ = TFile::Open(fileName.str().c_str(),"RECREATE");
  theTree_ = new TTree ("ConvertedEvent", "The reconstructed telescope tracks");
  theTree_->Branch("ConvertedEvent", &theConvertedEvent_, "xSlope/D:ySlope/D:xIntercept/D:yIntercept/D:cov[6]/D:chi2/D:trigger/I:runNumber/I:timestamp/L:stripCounter/L:fastCounter/L");

  // Retrieve from file the number of stored events  
  unsigned int numberOfEvents = reader->getNumberOfEvents() ;
  
  std::stringstream ss;
  ss.str(""); 
  ss << "Found " << numberOfEvents << " events on file" ;
  STDLINE(ss.str(),ACYellow) ;
  STDLINE("       ",ACYellow) ;
  STDLINE("Reading and Analyzing events...",ACGreen) ;
  for(unsigned int event=0; event<numberOfEvents; ++event)
  {
    if(event%1000 == 0)
    {
      ss.str(""); 
      ss << "Progress " << (int)(((float)event/numberOfEvents)*100) << " %" ;
      STDSNAP(ss.str(),ACRed) ;
    }      
    reader->readEvent(event) ;
    this->analyzeEvent(event) ;   
  }
  STDSNAP("Progress 100 %\n",ACGreen) ;
  
  ss.str("");;
  ss << "Writing output file: " << fileName.str();
  STDLINE(ss.str(), ACRed);
  theFile_->Write("",TObject::kOverwrite);
  theFile_->Close();
  STDLINE("Done!", ACGreen);
}


//------------------------------------------------------------------------------------
T1429Converter::~T1429Converter ()
{
}

//------------------------------------------------------------------------------------
// This is the method of the T1429Converter class where users are supposed 
// to implement their own private code for analysis. Here you will find 
// an exampl of how to retrieve and manipulate components of the event
// and the geometry.
//
// NOTE: for a detailed description of the Event class, its public access
//       methods and the meaning of invidual containers, please consult
//       ../include/Event.h and comments therein.
//
void T1429Converter::analyzeEvent(unsigned int event)
{



  //Event::clustersMapDef            & clusters                 = theEvent_->getClusters        ()     ;
  Event::fittedTracksDef           & fittedTracks             = theEvent_->getFittedTracks    ()     ;
  Event::chi2VectorDef             & chi2                     = theEvent_->getFittedTracksChi2()     ;
  Event::fittedTracksCovarianceDef & fittedTrackCovariance    = theEvent_->getFittedTracksCovariance();
  Event::trackCandidatesDef        & trackPoints              = theEvent_->getTrackCandidates ()     ;

  if( fittedTracks.size() == 0 ) return ;

  //TH2D *h_beam_profile = new TH2D("beam_profile","",620,0.,37200.,620,0.,37200.);

  unsigned int position;
  for(unsigned int track=0; track<fittedTracks.size(); ++track)
  {
    ROOT::Math::SVector<double,4> tParameters = fittedTracks[track] ;
    theConvertedEvent_.xSlope       = tParameters[0];
    theConvertedEvent_.ySlope       = tParameters[2];
    theConvertedEvent_.xIntercept   = tParameters[1]*10;//microns
    theConvertedEvent_.yIntercept   = tParameters[3]*10;//microns
    theConvertedEvent_.cov[0]       = fittedTrackCovariance[track](1,1)*100;
    theConvertedEvent_.cov[1]       = fittedTrackCovariance[track](2,2);
    theConvertedEvent_.cov[2]       = fittedTrackCovariance[track](1,2)*10;
    theConvertedEvent_.cov[3]       = fittedTrackCovariance[track](3,3)*100;
    theConvertedEvent_.cov[4]       = fittedTrackCovariance[track](4,4);
    theConvertedEvent_.cov[5]       = fittedTrackCovariance[track](3,4)*10;
    theConvertedEvent_.chi2         = chi2[track];
    theConvertedEvent_.trigger      = theEvent_->getTrigger();
    theConvertedEvent_.runNumber    = runNumber_;
    theConvertedEvent_.timestamp    = theEvent_->getUTC();
    theConvertedEvent_.stripCounter = theEvent_->getBCO();
    //theConvertedEvent_.fastCounter  = theEvent_->getNIMCounter();

    for(unsigned int i=0; i<22; i++)
      theConvertedEvent_.status[i] = 0;

    for( map<string, map<string, double> >::iterator pIt = trackPoints[track].begin();  pIt != trackPoints[track].end(); pIt++ )
    {
      position = moduleToPosition_[pIt->first];
      if( moduleToPosition_.find(pIt->first) != moduleToPosition_.end() )
      {
	theConvertedEvent_.status[position] = 1;
        theConvertedEvent_.x     [position] = pIt->second["x"]*10;
        theConvertedEvent_.y     [position] = pIt->second["y"]*10;
        theConvertedEvent_.z     [position] = pIt->second["z"]*10;
        theConvertedEvent_.xErr  [position] = pIt->second["xErr"]*10;
        theConvertedEvent_.yErr  [position] = pIt->second["yErr"]*10;
        theConvertedEvent_.zErr  [position] = pIt->second["zErr"]*10;
      }
	
    }
    //h_beam_profile->Fill(tParameters[1]*10,tParameters[3]*10);
    
    theTree_->Fill();
//    stringstream ss;
//    ss.str("") ;
//    ss.setf(std::ios_base::right,std::ios_base::adjustfield) ;
//    ss << std::setprecision(8) << " ";
//    ss << std::setw( 4) << tr  
//        << std::setw(15) << tParameters[0] 
//        << std::setw(15) << tParameters[2] 
//        << std::setw(15) << tParameters[1] 
//        << std::setw(15) << tParameters[3] 
//        << std::setprecision(3)
//        << std::setw( 6) << chi2[tr]
//          << hex << theConvertedEvent_.bco;
//    STDLINE(ss.str(),ACGreen) ;
    
    
  }

 // TCanvas *c = new TCanvas("beam_profile","",700,700);
 // h_beam_profile->Draw("colz");
                                 
}
